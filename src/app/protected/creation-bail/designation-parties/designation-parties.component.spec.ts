import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignationPartiesComponent } from './designation-parties.component';

describe('DesignationPartiesComponent', () => {
  let component: DesignationPartiesComponent;
  let fixture: ComponentFixture<DesignationPartiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesignationPartiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignationPartiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
