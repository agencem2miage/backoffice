/**
 *
 */
export default class Utils {
  /**
   * transforme un fichier (image) en base 64
   * @param url url de l'image
   */
  static async imageUrlToBase64(
    url: string
  ): Promise<string | ArrayBuffer | null>;

  /**
   * transforme un fichier (image) en base 64
   * @param url url de l'image
   * @returns l'image en base 64
   */
  static async imageUrlToBase64(
    url: string
  ): Promise<string | ArrayBuffer | null> {
    const res = await fetch(url);
    const blob = await res.blob();

    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.addEventListener(
        'load',
        function () {
          return resolve(reader.result);
        },
        false
      );

      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    });
  }
}
