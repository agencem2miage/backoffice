import { SharedModule } from './../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChoixBailComponent } from './choix-bail/choix-bail.component';
import { PiecesComponent } from './pieces/pieces.component';
import { InventaireMeublesComponent } from './inventaire-meubles/inventaire-meubles.component';
import { SignatureEdlComponent } from './signature-edl/signature-edl.component';
import { CuisineComponent } from './types-pieces/cuisine/cuisine.component';
import { SalleDeBainComponent } from './types-pieces/salle-de-bain/salle-de-bain.component';
import { ToilettesComponent } from './types-pieces/toilettes/toilettes.component';
import { AutresComponent } from './types-pieces/autres/autres.component';
import { MeublesComponent } from './types-equipements/meubles/meubles.component';
import { ElectromenagerComponent } from './types-equipements/electromenager/electromenager.component';
import { EquipementGeneralComponent } from './types-equipements/equipement-general/equipement-general.component';

@NgModule({
  declarations: [
    ChoixBailComponent,
    PiecesComponent,
    InventaireMeublesComponent,
    SignatureEdlComponent,
    CuisineComponent,
    SalleDeBainComponent,
    ToilettesComponent,
    AutresComponent,
    MeublesComponent,
    ElectromenagerComponent,
    EquipementGeneralComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
  ],
})
export class CreationEtatDesLieuxModule {}
