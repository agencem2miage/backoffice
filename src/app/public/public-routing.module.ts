import { ConnexionComponent } from './connexion/connexion.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotFoundComponent } from './not-found/not-found.component';
import { LoggedInAdminGuard } from '../core/guards/logged-in-admin.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'connexion',
    pathMatch: 'full',
  },
  {
    path: 'connexion',
    component: ConnexionComponent,
    canActivate: [LoggedInAdminGuard],
    data: { unloggedAdmin: true },
  },
  {
    path: 'page-404',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicRoutingModule { }
