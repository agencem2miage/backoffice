import { Subscription } from 'rxjs';
import { BienService } from './../../core/services/bien.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgMeta } from 'ngmeta';

import { Constants } from '../../constants';
import { Bien } from 'src/app/shared/models/bien.model';
import { environment } from 'src/environments/environment';
import { TypeContrat } from 'src/app/shared/models/typeContrat.model';
import { TypeBien } from 'src/app/shared/models/typeBien.model';
import { Adresse } from 'src/app/shared/models/adresse.model';
import { Utilisateur } from 'src/app/shared/models/utilisateur.model';
import { Bail } from 'src/app/shared/models/bail.model';

@Component({
  selector: 'app-list-bien',
  templateUrl: './list-bien.component.html',
  styleUrls: ['./list-bien.component.scss'],
})
export class ListBienComponent implements OnInit {
  /**
   * Formulaire pour la recherche des filtres
   */
  formFilterBien!: FormGroup;

  /**
   * Permet d'attendre que l'anomalie soit chargée pour l'afficher
   */
  listeBiensLoaded!: Promise<boolean>;

  /**
   * Contient la liste des biens à afficher dans la mosaïque
   */
  listeBien: Bien[] = [];

  /**
   * Contient la liste des filtres biens à afficher
   */
  listTypeContrat: TypeContrat[] = [];
  /**
   * liste type bien
   */
  listTypeBien: TypeBien[] = [];

  /**
   * liste Localisation
   */
  listLocalisation: Adresse[] = [];

  /**
   * Liste proprietaire
   */
  listProprietaire: Utilisateur[] = [];

  /**
   * Liste localisation
   */
  listLocation: number[] = [];

  /**
   * liste numéros
   */
  listNumBail: number[] = [];

  /**
   * Le nombre de biens retournés
   */
  nbBiens!: number;

  /**
   * Subscription pour les biens
   */
  getListeBienSub!: Subscription;

  /**
   * Subscription pour les types de filtres de biens
   */
  getFilterBienSub!: Subscription;

  /**
   * Type des anomalies à afficher
   */
  displayedBiens: {
    list: Bien[];
    infiniteScroll: {
      scrollBien: Bien[];
      sum: number;
      isFull: boolean;
    };
  } = {
      list: [],
      infiniteScroll: {
        scrollBien: [],
        sum: 12,
        isFull: false,
      },
    };

  /**
   * Constructeur du composant
   */
  constructor(
    public constants: Constants,
    private ngMeta: NgMeta,
    private fb: FormBuilder,
    private bienService: BienService
  ) { }

  /**
   * Initialisation du composant
   */
  ngOnInit(): void {
    this.ngMeta.setAll({
      title: this.constants.SEO_SITE_TITLE + 'Liste des biens',
      description: this.constants.SEO_SITE_DESCRIPTION,
      canonical: this.constants.CURRENT_URL,
      image: this.constants.IMAGE_URL + 'logo.png',
    });
    this.getListeBienSub = this.bienService
      .getListeBien()
      .subscribe((result) => {
        this.listeBien = result;
        this.nbBiens = this.listeBien.length;
        this.displayedBiens.list = this.listeBien;
        this.getBiens();
        this.listeBiensLoaded = Promise.resolve(true);
      });
    this.getFilterBienSub = this.bienService
      .getListeFilterBienAdmin()
      .subscribe((result) => {
        this.listTypeContrat = result[0].listTypeContrat;
        this.listTypeBien = result[0].listTypeBien;
        this.listLocalisation = result[0].listLocalisation;
        this.listProprietaire = result[0].listProprietaire;
        this.listLocation = result[0].listLocation;
        this.listNumBail = result[0].listNumBail.sort();
      });
    this.initForm();
  }

  /**
   * initialisation du formulaire
   */
  initForm(): void {
    this.formFilterBien = this.fb.group({
      filtreContrat: [''],
      filtreBien: [''],
      filtreLocalisation: [''],
      filtrePrice: [0],
      filtreArea: [0],
      filtreRoom: [1],
      filtreProprietaire: [''],
      filtreLocation: [''],
      filtreNumBail: [''],
    });
  }

  /**
   * récupération de la premiere image
   * @param bien
   * @returns
   */
  premiereImage(bien: Bien): boolean {
    if (
      !bien.image[0] ||
      bien.image[0] === environment.IMAGE_API + 'undifined'
    ) {
      return false;
    }
    return true;
  }

  /**
   * Se déclenche au click sur le lien "voir plus"
   * Permet d'afficher 12 éléments supplémentaires à l'affichage du tableau de biens
   */
  onScrollDown(): void {
    this.displayedBiens.infiniteScroll.sum += 12;
    this.getBiens();
  }

  /**
   * Permet de retourner les controls du formulaire facilement
   */
  get f() {
    return this.formFilterBien.controls;
  }

  /**
   * Remplit les biens à afficher avec le scroll infini
   */
  getBiens() {
    const nbDisplayedBien = this.displayedBiens.list.length;
    this.displayedBiens.infiniteScroll.scrollBien = this.displayedBiens.list.slice(
      0,
      this.displayedBiens.infiniteScroll.sum
    );

    if (
      this.displayedBiens.infiniteScroll.sum >= nbDisplayedBien ||
      nbDisplayedBien <= 12
    ) {
      this.displayedBiens.infiniteScroll.isFull = true;
    }
  }

  /**
   * changer les filtres
   */
  filterChange(): void {
    let filterListBien = this.listeBien;
    if (this.f.filtreContrat.value > 0) {
      filterListBien = filterListBien.filter(
        (bien) => bien.typeContrat.id === this.f.filtreContrat.value
      );
    }

    if (this.f.filtreBien.value > 0) {
      filterListBien = filterListBien.filter(
        (bien) => bien.typeBien.id === this.f.filtreBien.value
      );
    }

    if (this.f.filtreLocalisation.value !== '') {
      filterListBien = filterListBien.filter(
        (bien) => bien.adresse.ville === this.f.filtreLocalisation.value
      );
    }

    if (this.f.filtrePrice.value > -1) {
      filterListBien = filterListBien.filter(
        (bien) => bien.prix >= this.f.filtrePrice.value
      );
    }

    if (this.f.filtreArea.value > -1) {
      filterListBien = filterListBien.filter(
        (bien) => bien.surfaceHabitable >= this.f.filtreArea.value
      );
    }

    if (this.f.filtreRoom.value > -1) {
      filterListBien = filterListBien.filter(
        (bien) => bien.nombrePiece >= this.f.filtreRoom.value
      );
    }

    if (this.f.filtreProprietaire.value !== '') {
      filterListBien = filterListBien.filter(
        (bien) => bien.proprietaire.nom === this.f.filtreProprietaire.value
      );
    }

    if (this.f.filtreLocation.value === 'oui') {
      filterListBien = filterListBien.filter((bien) =>
        this.listLocation.includes(bien.id)
      );
    } else {
      this.f.filtreNumBail.setValue('');
    }

    if (this.f.filtreLocation.value === 'non') {
      filterListBien = filterListBien.filter(
        (bien) => !this.listLocation.includes(bien.id)
      );
    }

    if (this.f.filtreNumBail.value > 0) {
      filterListBien = filterListBien.filter(
        (bien) => bien.id === this.f.filtreNumBail.value
      );
    }

    this.nbBiens = filterListBien.length;
    this.displayedBiens.list = filterListBien;
    this.displayedBiens.infiniteScroll.sum = 12;
    this.displayedBiens.infiniteScroll.isFull = false;
    this.getBiens();
  }
}
