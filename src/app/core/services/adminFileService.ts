import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from 'src/app/constants';

import { environment } from '../../../environments/environment';

/**
 * Service de téléchargement de la documentation Administrateur
 */
@Injectable({
  providedIn: 'root',
})
export class AdminFileService {
  /**
   * Point d'entrée de l'API EcoCampus pour le téléchargement des documents administrateur
   */
  file = environment.API_URL + this.constants.API_ENDPOINTS.FILE;

  /**
   * Constructeur du composant, Import des services nécessaires
   * @param http Service natif d'angular pour la création de requêtes HTTP
   * @param constants Fichier qui contient les constantes du projet
   */
  constructor(private http: HttpClient, private constants: Constants) {}

  /**
   * Appelle le service de téléchargement de fichier
   * @returns le fichier sous format pdf
   */
  getFile(nomFichier: string): Observable<any> {
    return this.http.get(this.file + '/' + nomFichier, {
      responseType: 'blob',
    });
  }
}
