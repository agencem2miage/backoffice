import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PriseEffetContratComponent } from './prise-effet-contrat.component';

describe('PriseEffetContratComponent', () => {
  let component: PriseEffetContratComponent;
  let fixture: ComponentFixture<PriseEffetContratComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PriseEffetContratComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PriseEffetContratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
