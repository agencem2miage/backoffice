import { DureesBail } from './../../../constants';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { CreationBailProvider } from 'src/app/core/providers/creation-bail.provider';

@Component({
  selector: 'app-prise-effet-contrat',
  templateUrl: './prise-effet-contrat.component.html',
  styleUrls: ['./prise-effet-contrat.component.scss'],
})
export class PriseEffetContratComponent implements OnInit {
  /**
   * Formulaire pour la désignation des parties
   */
  form!: FormGroup;

  /**
   * Nom du formulaire
   */
  formLabel = 'priseEffetContrat';

  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  dureesBail = DureesBail;

  /**
   * Constructeur du composant
   * @param creationBailProvider Provider permettant d'enregistrer les étapes de création de bail
   * @param fb FormBuilder natif angular
   */
  constructor(
    private creationBailProvider: CreationBailProvider,
    private fb: FormBuilder,
    private router: Router
  ) {}

  /**
   * Initialisation du formulaire
   */
  ngOnInit(): void {
    this.initForm();
  }

  /**
   * Initialisation du formulaire
   */
  initForm(): void {
    const formulairePrerempli = this.creationBailProvider.formGroups.find(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli) {
      this.form = formulairePrerempli.formGroup;
    } else {
      this.form = this.fb.group({
        datePriseEffet: ['', [Validators.required]],
        dureeBail: ['', [Validators.required]],
      });
    }
  }

  /**
   * Soumission du formulaire pour passer à l'étape suivante
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.form.valid) {
      const formulairePrerempli = this.creationBailProvider.formGroups.findIndex(
        (form) => form.formGroupLabel === this.formLabel
      );

      if (formulairePrerempli !== -1) {
        this.creationBailProvider.formGroups.splice(formulairePrerempli, 1, {
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      } else {
        this.creationBailProvider.formGroups.push({
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      }

      this.router.navigate([
        'admin',
        'creation-bail',
        'conditions-financieres',
      ]);
    }
  }

  /**
   * Ajoute/Supprime les champs concernant la durée du bail selon le choix de l'utilisateur
   */
  changeDureeBail(): void {
    if (this.form.controls.dureeBail.value === DureesBail.REDUITE) {
      this.form.addControl(
        'dureeBailReduite',
        new FormControl('', [Validators.required])
      );
      this.form.addControl(
        'justificationDureeBailReduite',
        new FormControl('', [Validators.required])
      );
    } else {
      if (
        this.form.contains('dureeBailReduite') &&
        this.form.contains('justificationDureeBailReduite')
      ) {
        this.form.removeControl('dureeBailReduite');
        this.form.removeControl('justificationDureeBailReduite');
      }
    }
  }
}
