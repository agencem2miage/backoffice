import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';

/**
 * Routes de app routing
 */
const routes: Routes = [
  {
    path: '**',
    redirectTo: '/page-404',
  },
];

/**
 * Options de routing
 */
const routerOptions: ExtraOptions = {
  scrollPositionRestoration: 'enabled',
  useHash: false,
  anchorScrolling: 'enabled',
};

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule],
})
/**
 * Classe de routing
 */
export class AppRoutingModule {}
