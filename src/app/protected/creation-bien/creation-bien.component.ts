import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { BienService } from 'src/app/core/services/bien.service';
import { ModalService } from 'src/app/core/services/modal.service';
import { NotificationsService } from 'src/app/core/services/notifications.service';
import { UserService } from 'src/app/core/services/user.service';
import { PiecesService } from 'src/app/core/services/pieces.service';

import { Bien } from 'src/app/shared/models/bien.model';
import { Modal, ModalType } from 'src/app/shared/models/modal.model';
import {
  Notification,
  NotificationBackground,
  NotificationIcon,
} from 'src/app/shared/models/notification.model';
import { TypeBien } from 'src/app/shared/models/typeBien.model';
import { TypeContrat } from 'src/app/shared/models/typeContrat.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-creation-bien',
  templateUrl: './creation-bien.component.html',
  styleUrls: ['./creation-bien.component.scss'],
})
export class CreationBienComponent implements OnInit {
  /**
   * Formulaire pour la creation de bien
   */
  formCreationBien!: FormGroup;
  /**
   * Souscription de création d'un bien
   */
  createBienSub!: Subscription;
  /**
   * liste des types contrat
   */
  listTypeContrat: TypeContrat[] = [];
  /**
   * Liste des types de bien
   */
  listTypeBien: TypeBien[] = [];
  /**
   * liste des proprietaires
   */
  listeProprietaire!: any;
  /**
   * Liste des types de piece
   */
  listTypePiece!: any;
  /**
   * liste des images de bail
   */
  listImgBailName: Array<string> = [];
  /**
   * liste des pieces
   */
  listPiece: Array<any> = [];

  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  /**
   * Constructeur du composant
   * @param fb FormBuilder natif angular
   * @param http Requête POST et GET
   */
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private modalService: ModalService,
    private notificationsService: NotificationsService,
    private bienService: BienService,
    private UserService: UserService,
    private cd: ChangeDetectorRef,
    private piecesService: PiecesService
  ) {}

  /**
   * Initialisation du formulaire
   */
  ngOnInit(): void {
    this.createBienSub = this.bienService
      .getListeFilterBienAdmin()
      .subscribe((result) => {
        this.listTypeContrat = result[0].listTypeContrat;
        this.listTypeBien = result[0].listTypeBien;
        this.UserService.getProprietaires().subscribe((result) => {
          this.listeProprietaire = result;
        });
      });
    this.piecesService.getTypePiece().subscribe((result) => {
      this.listTypePiece = result;
    });
    this.initForm();
  }

  /**
   * Initialisation du formulaire
   */
  initForm(): void {
    this.formCreationBien = this.fb.group({
      proprietaire: ['', [Validators.required, Validators.maxLength(55)]],
      typeBien: ['', [Validators.required]],
      ville: ['', [Validators.required]],
      numeroRue: ['', [Validators.required]],
      typeContrat: ['', [Validators.required]],
      nomRue: ['', [Validators.required, Validators.maxLength(55)]],
      complementAdresse: [''],
      codePostal: ['', [Validators.required, Validators.pattern('[0-9]{5}')]],
      surfaceHabitable: ['', [Validators.required]],
      surfaceTerrain: ['', [Validators.required]],
      surfaceBalcon: ['', [Validators.required]],
      prixVente: ['', [Validators.required]],
      anneeConstruction: ['', [Validators.required]],
      photoBien: [''],
      equipementsDivers: [''],
      descriptionBien: [''],
      detailsPieces: [''],
      meuble: ['', [Validators.required]],
      classeEnergie: ['', [Validators.required]],
      regimeJuridique: ['', [Validators.required]],
      ges: ['', [Validators.required]],
      garageVelo: [''],
      garageVoiture: [''],
      espacesVert: [''],
      localPoubelle: [''],
      ascenseur: [''],
      gardiennage: [''],
      aireJeux: [''],
      laverie: [''],
      image: [''],
      listePieces: [''],
    });
  }

  /**
   * getteur du formulaire
   */
  get f() {
    return this.formCreationBien.controls;
  }

  /**
   * suppression d'une piece
   * @param idSelect
   */
  removePiece(idSelect: any) {
    const RemoveElementFromObjectArray = (key: number) => {
      this.listPiece.forEach((value, index) => {
        if (value.id == key) this.listPiece.splice(index, 1);
      });
    };
    RemoveElementFromObjectArray(idSelect);
    const notification = new Notification({
      message: 'La pièce a été supprimée.',
      background: NotificationBackground.GREEN,
      icon: NotificationIcon.CHECK,
    });
    this.notificationsService.genericNotification(notification);
  }

  /**
   * ajout d'une piece
   * @param idSelect
   */
  addPiece(idSelect: any): void {
    const typePiece = this.listTypePiece.filter(
      (obj: any) => obj.id === idSelect
    );
    this.listPiece.push({
      id: typePiece[0].id,
      label: typePiece[0].label,
    });
    const notification = new Notification({
      message: 'Nouvelle pièce ajoutée.',
      background: NotificationBackground.GREEN,
      icon: NotificationIcon.CHECK,
    });
    this.notificationsService.genericNotification(notification);
  }

  /**
   * événement de changement de fichier
   * @param event
   */
  onFileChange(event: any): void {
    if (event.target.files && event.target.files.length) {
      const finalResult: Array<string | ArrayBuffer | null> = [];
      const file = event.target.files;
      const countFile = file.length;

      for (let i = 0; i < countFile; i++) {
        this.listImgBailName.push(file.item(i).name);
        if (file.item(i).size <= 5000000) {
          const reader = new FileReader();
          reader.readAsDataURL(file.item(i));
          reader.onload = () => {
            finalResult.push(reader.result);
          };
        } else {
          const notification = new Notification({
            message: "Votre image n'est pas valide.",
            background: NotificationBackground.RED,
            icon: NotificationIcon.CROSS,
          });
          this.notificationsService.genericNotification(notification);
        }
      }
      this.formCreationBien.patchValue({
        photoBien: finalResult,
      });
      this.cd.markForCheck();
    }
  }

  /**
   * suppression de fichier
   */
  onDeleteFile(): void {
    this.listImgBailName = [];
    this.formCreationBien.patchValue({
      photoBien: '',
    });
  }

  /**
   * méthode appelée lors de la soumission du formulaire.
   * Elle appelle l'API de connexion
   */
  onSubmit(): void {
    this.submitted = true;
    if (this.formCreationBien.valid) {
      this.formCreationBien.patchValue({
        listePieces: this.listPiece,
      });
      const confirmationModal = new Modal({
        title: 'Confirmation',
        text: 'Êtes-vous sûr de vouloir créer ce bien ?',
        type: ModalType.CONFIRMATION,
      });

      confirmationModal.confirm = () => {
        const newBien = Bien.fromJson({
          BIEN_ID: -1,
          ID_PROPRIETAIRE: this.f.proprietaire.value,

          ID_TYPE_CONTRAT: this.f.typeContrat.value,

          ID_TYPE_BIEN: this.f.typeBien.value,
          BIEN_ID_ADRESSE: -1,
          ADRESSE_BIEN_CODE_POSTAL: this.f.codePostal.value,
          ADRESSE_BIEN_VILLE: this.f.ville.value,
          ADRESSE_BIEN_NUMERO: this.f.numeroRue.value,
          ADRESSE_BIEN_RUE: this.f.nomRue.value,
          ADRESSE_BIEN_COMPLEMENT: this.f.complementAdresse.value,
          ADRESSE_BIEN_PAYS: 'FRANCE',
          ID_REGIME_JURIDIQUE: this.f.regimeJuridique.value,
          ANNEE_CONSTRUCTION: this.f.anneeConstruction.value,
          SURFACE_HABITABLE: this.f.surfaceHabitable.value,
          SURFACE_TERRAIN: this.f.surfaceTerrain.value,
          SURFACE_BALCON: this.f.surfaceBalcon.value,
          BIEN_PRIX_LOCATION: this.f.prixVente.value,
          EQUIPEMENT: this.f.equipementsDivers.value,
          DETAILS_PIECE: this.f.detailsPieces.value,
          MEUBLE: this.f.meuble.value,
          DESCRIPTION: this.f.descriptionBien.value,
          CLASSE_ENERGIE: this.f.classeEnergie.value,
          GES: this.f.ges.value,
          IMAGES: this.f.photoBien.value,
          PIECES: this.f.listePieces.value,
          GARAGE_VELO: this.f.garageVelo.value,
          GARAGE_VOITURE: this.f.garageVoiture.value,
          ESPACES_VERTS: this.f.espacesVert.value,
          LOCAL_POUBELLE: this.f.localPoubelle.value,
          ASCENSEUR: this.f.ascenseur.value,
          AIRE_JEUX: this.f.aireJeux.value,
          GARDIENNAGE: this.f.gardiennage.value,
          LAVERIE: this.f.laverie.value,
        });

        this.createBienSub = this.bienService
          .createBien(newBien)
          .subscribe(() => {
            const notification = new Notification({
              message: 'Votre bien a bien été créé.',
              background: NotificationBackground.GREEN,
              icon: NotificationIcon.CHECK,
            });
            this.notificationsService.genericNotification(notification);

            this.formCreationBien.patchValue({
              proprietaire: '',
              typeBien: '',
              ville: '',
              numeroRue: '',
              typeContrat: '',
              nomRue: '',
              complementAdresse: '',
              codePostal: '',

              surfaceHabitable: '',
              surfaceTerrain: '',
              surfaceBalcon: '',
              prixVente: '',
              anneeConstruction: '',
              photoBien: '',
              equipementsDivers: '',
              descriptionBien: '',
              detailsPieces: '',
              meuble: '',
              classeEnergie: '',
              regimeJuridique: '',
              ges: '',
              garageVelo: '',
              garageVoiture: '',
              espacesVert: '',
              localPoubelle: '',
              ascenseur: '',
              gardiennage: '',
              aireJeux: '',
              laverie: '',
              image: '',
              listePieces: '',
            });
            this.router.navigate(['/admin/liste-biens/']);
          });
      };
      this.modalService.confirmationModal(confirmationModal);
    }
  }
}
