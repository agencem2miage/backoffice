import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EtatsEquipements } from 'src/app/constants';
import { CreationEdlProvider } from 'src/app/core/providers/creation-edl.provider';

@Component({
  selector: 'app-salle-de-bain',
  templateUrl: './salle-de-bain.component.html',
  styleUrls: ['./salle-de-bain.component.scss'],
})
/**
 * classe de salle de bain
 */
export class SalleDeBainComponent implements OnInit {
  /**
   * formulaire
   */
  form!: FormGroup;
  /**
   * Soumission du formulaire
   */
  submitted = false;
  /**
   * état des équipements
   */
  etats = EtatsEquipements;
  /**
   * titre du formulaire
   */
  @Input() formLabel!: string;
  /**
   * types de pieces
   */
  @Input() typePiece!: string;

  /**
   * constructeur de salle de bain
   * @param fb formulaire
   * @param creationEdlProvider provider de création de salle de bain
   */
  constructor(
    private fb: FormBuilder,
    private creationEdlProvider: CreationEdlProvider
  ) {}

  /**
   * Initialisation du component
   */
  ngOnInit(): void {
    const formulairePrerempli = this.creationEdlProvider.piecesFormGroups.find(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli) {
      this.form = formulairePrerempli.formGroup;
    } else {
      this.form = this.fb.group({
        elements: this.fb.group({
          plafond: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          murs: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          sol: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          porte: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          plinthes: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          fenetreVolet: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          placard: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          vasque: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          meubleSousVasque: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          baignoireDouche: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          autres: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
        }),
        equipements: this.fb.group({
          robinetterieVasque: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          evacuationVasque: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          robinetterieDouche: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          evacuationDouche: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          robinetterieBaignoire: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          evacuationBaignoire: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          joints: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          rangements: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          wc: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          miroir: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          radiateur: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          eclairage: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          prisesElectriques: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          interrupteurs: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          ventilation: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
          autres: this.fb.group({
            etat: ['1', Validators.required],
            observations: [''],
          }),
        }),
      });
    }
  }

  /**
   * Méthode de soumission du formulaire
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.form.valid) {
      const formulairePrerempli = this.creationEdlProvider.piecesFormGroups.findIndex(
        (form) => form.formGroupLabel === this.formLabel
      );

      if (formulairePrerempli !== -1) {
        this.creationEdlProvider.piecesFormGroups.splice(
          formulairePrerempli,
          1,
          {
            formGroupLabel: this.formLabel,
            formGroup: this.form,
          }
        );
      } else {
        this.creationEdlProvider.piecesFormGroups.push({
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      }

      let elementStyle = document.querySelector(
        '.sdbItem' + this.formLabel
      ) as HTMLElement;
      elementStyle.style.display = 'none';

      elementStyle = document.querySelector(
        '.sdb' + this.formLabel
      ) as HTMLElement;

      elementStyle.style.backgroundColor = '#60b664';
    }
  }

  /**
   * Méthode d'édition du formulaire
   */
  editForm(): void {
    let elementStyle = document.querySelector(
      '.sdbItem' + this.formLabel
    ) as HTMLElement;
    elementStyle.style.display = 'block';

    elementStyle = document.querySelector(
      '.sdb' + this.formLabel
    ) as HTMLElement;

    elementStyle.style.backgroundColor = '#fff';
  }
}
