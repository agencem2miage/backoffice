import { CreationEdlProvider } from './../../../../core/providers/creation-edl.provider';
import { EtatsEquipements } from './../../../../constants';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-cuisine',
  templateUrl: './cuisine.component.html',
  styleUrls: ['./cuisine.component.scss'],
})
/**
 * Component de cuisine
 */
export class CuisineComponent implements OnInit {
  /**
   * formulaire du component
   */
  form!: FormGroup;
  /**
   * soumission du formulaire
   */
  submitted = false;
  /**
   * etat des équipements
   */
  etats = EtatsEquipements;
  /**
   * titre du formulaire
   */
  @Input() formLabel!: string;

  /**
   * constructeur de cuisine (cuisiniste)
   * @param fb  Formulaire
   * @param creationEdlProvider provider de cuisiniste
   */
  constructor(
    private fb: FormBuilder,
    private creationEdlProvider: CreationEdlProvider
  ) {}

  /**
   * méthode d'initialisation du component
   */
  ngOnInit(): void {
    const formulairePrerempli = this.creationEdlProvider.piecesFormGroups.find(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli) {
      this.form = formulairePrerempli.formGroup;
    } else {
      this.form = this.fb.group({
        plafond: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        plinthes: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        sol: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        murs: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        portesMenuiseries: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        fenetresVolets: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        rangementsPlacards: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        prisesInterrupteurs: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        evacuationEau: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        radiateurs: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        eclairage: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        ventilation: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        eviers: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        meublesEviers: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        hotte: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        planTravail: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        placards: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        tiroirs: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        autres: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
      });
    }
  }

  /**
   * méthode de soumission du formulaire
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.form.valid) {
      const formulairePrerempli = this.creationEdlProvider.piecesFormGroups.findIndex(
        (form) => form.formGroupLabel === this.formLabel
      );

      if (formulairePrerempli !== -1) {
        this.creationEdlProvider.piecesFormGroups.splice(
          formulairePrerempli,
          1,
          {
            formGroupLabel: this.formLabel,
            formGroup: this.form,
          }
        );
      } else {
        this.creationEdlProvider.piecesFormGroups.push({
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      }

      let elementStyle = document.querySelector(
        '.cuisineItem' + this.formLabel
      ) as HTMLElement;
      elementStyle.style.display = 'none';

      elementStyle = document.querySelector(
        '.cuisine' + this.formLabel
      ) as HTMLElement;

      elementStyle.style.backgroundColor = '#60b664';
    }
  }

  /**
   * méthode d'édition du formulaire
   */
  editForm(): void {
    let elementStyle = document.querySelector(
      '.cuisineItem' + this.formLabel
    ) as HTMLElement;
    elementStyle.style.display = 'block';

    elementStyle = document.querySelector(
      '.cuisine' + this.formLabel
    ) as HTMLElement;

    elementStyle.style.backgroundColor = '#fff';
  }
}
