import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignatureEdlComponent } from './signature-edl.component';

describe('SignatureEdlComponent', () => {
  let component: SignatureEdlComponent;
  let fixture: ComponentFixture<SignatureEdlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SignatureEdlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignatureEdlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
