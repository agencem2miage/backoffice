import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Constants } from 'src/app/constants';
import { environment } from 'src/environments/environment';
import { Piece } from 'src/app/shared/models/bien.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
/**
 * service des pièces
 */
export class PiecesService {
  /**
   * Point d'entrée de l'API pour la gestion des pièces
   */
  piecesEndpoint = environment.API_URL + this.constants.API_ENDPOINTS.PIECES;

  /**
   * constructeur des pieces
   * @param http
   * @param constants
   */
  constructor(private http: HttpClient, private constants: Constants) {}

  /**
   * récupération des pièces d'un bien
   * @param idBien
   * @returns
   */
  getPieces(idBien: number): Observable<Piece[]> {
    return this.http.get(this.piecesEndpoint + '/' + idBien).pipe(
      map((jsonResponse: any) =>
        jsonResponse.map((jsonPiece: any) => {
          return {
            id: jsonPiece.idPieceBien,
            label: '',
            typePiece: jsonPiece.idTypePiece,
          };
        })
      )
    );
  }

  /**
   * Récupération des types de piece
   * @returns
   */
  getTypePiece(): Observable<any> {
    return this.http
      .get(this.piecesEndpoint + '/type-piece')
      .pipe(
        map((jsonResponse: any) =>
          jsonResponse.map((jsonPiece: any) => jsonPiece)
        )
      );
  }
}
