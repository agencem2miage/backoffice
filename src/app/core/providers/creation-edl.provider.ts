import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Bail } from 'src/app/shared/models/bail.model';

/**
 * Provider permettant d'enregistrer les étapes de création d'un état des lieux
 */
@Injectable({
  providedIn: 'root',
})
export class CreationEdlProvider {
  /**
   * Liste des formulaires de création d'un état des lieux
   */
  formGroups: {
    formGroupLabel: string;
    formGroup: FormGroup;
  }[] = [];

  /**
   * Liste des formulaires de création d'une piece
   */
  piecesFormGroups: {
    formGroupLabel: string;
    formGroup: FormGroup;
  }[] = [];

  /**
   * formulaire d'inventaire
   */
  inventaireFormGroups: {
    formGroupLabel: string;
    formGroup: FormGroup;
  }[] = [];

  /**
   * bail
   */
  bail!: Bail;

  /**
   * formulaire rempli
   * @param formName
   * @returns
   */
  isFormFilled(formName: string): boolean {
    return (
      this.formGroups.findIndex(
        (formGroup) => formGroup.formGroupLabel === formName
      ) !== -1
    );
  }

  /**
   * vidage du provider
   */
  cleanProvider(): void {
    this.formGroups = [];
    this.piecesFormGroups = [];
    this.inventaireFormGroups = [];
  }
}
