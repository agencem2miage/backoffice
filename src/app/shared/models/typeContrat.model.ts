/**
 * Modèle représentant un utilisateur
 */
export class TypeContrat {
  /**
   * Identifiant de l'utilisateur
   */
  readonly id: number;

  /**
   * Label du role
   */
  label: string;

  /**
   * Constructeur de l'objet, tous les paramètres sont obligatoires
   * @param options
   */
  constructor(option: { id: number; label: string }) {
    this.id = option.id || 0;
    this.label = option.label || '';
  }

  /**
   * Crée un utilisateur à partir d'un flux  JSON
   * @param json Les propriétés de l'uilisateur contenues dans le flux JSON
   * @returns {TypeContrat} L'objet utilisateur créé
   */
  public static fromJson(json: any): TypeContrat {
    return new TypeContrat({
      id: json.id,
      label: json.label,
    });
  }
}
