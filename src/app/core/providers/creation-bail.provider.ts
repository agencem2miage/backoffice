import { Bien } from 'src/app/shared/models/bien.model';
import { Utilisateur } from './../../shared/models/utilisateur.model';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

/**
 * Provider permettant d'enregistrer les étapes de création de bail
 */
@Injectable({
  providedIn: 'root',
})
export class CreationBailProvider {
  /**
   * Liste des formulaires de création de bail
   */
  formGroups: {
    formGroupLabel: string;
    formGroup: FormGroup;
  }[] = [];

  /**
   * liste des parties signataires
   */
  parties!: {
    locataire: Utilisateur;
    proprietaire: Utilisateur;
  };

  /**
   * bien du bail
   */
  bien!: Bien;

  /**
   * champs remplis
   * @param formName
   * @returns
   */
  isFormFilled(formName: string): boolean {
    return (
      this.formGroups.findIndex(
        (formGroup) => formGroup.formGroupLabel === formName
      ) !== -1
    );
  }

  /**
   * vidage du provider
   */
  cleanProvider(): void {
    this.formGroups = [];
  }
}
