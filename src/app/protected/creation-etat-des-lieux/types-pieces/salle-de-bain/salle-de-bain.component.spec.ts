import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalleDeBainComponent } from './salle-de-bain.component';

describe('SalleDeBainComponent', () => {
  let component: SalleDeBainComponent;
  let fixture: ComponentFixture<SalleDeBainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalleDeBainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalleDeBainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
