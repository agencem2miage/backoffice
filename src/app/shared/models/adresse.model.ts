/**
 * Modèle représentant une adresse
 */
export class Adresse {
  /**
   * Identifiant de l'adresse
   * Possiblement inutile, à voir
   */
  readonly id?: number;

  /**
   * Numéro de la rue de l'adresse
   * String car peut être "20" comme peut également être "20bis" ou "20ter"
   */
  numero: string;

  /**
   * Nom de la rue de l'adresse
   */
  rue: string;

  /**
   * Complément d'adresse de l'adresse
   */
  complement: string;

  /**
   * Code postale de l'adresse
   */
  codePostal: string;

  /**
   * Ville de l'adresse
   */
  ville: string;

  /**
   * Pays de l'adresse
   */
  pays: string;

  /**
   * Constructeur de l'objet, tous les paramètres sont obligatoires
   * @param options
   */
  constructor(option: {
    id: number;
    numero: string;
    rue: string;
    complement: string;
    codePostal: string;
    ville: string;
    pays: string;
  }) {
    this.id = option.id || 0;
    this.numero = option.numero || '';
    this.rue = option.rue || '';
    this.complement = option.complement || '';
    this.codePostal = option.codePostal || '00000';
    this.ville = option.ville || '';
    this.pays = option.pays || '';
  }

  /**
   * Crée un utilisateur à partir d'un flux JSON
   * @param json Les propriétés de l'adresse contenues dans le flux JSON
   * @returns {Adresse} L'objet adresse créé
   */
  public static fromJson(json: any): Adresse {
    return new Adresse({
      id: json.id,
      numero: json.numero,
      rue: json.rue,
      complement: json.complement,
      codePostal: json.codePostal,
      ville: json.ville,
      pays: json.pays,
    });
  }
}
