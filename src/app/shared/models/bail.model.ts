import { Adresse } from './adresse.model';
import { Bien } from './bien.model';
import { legalRegime } from './legalRegime.model';
import { TypeBien } from './typeBien.model';
import { TypeContrat } from './typeContrat.model';
import { Utilisateur } from './utilisateur.model';
/**
 * modèle de bail
 */
export class Bail {
  /**
   * id du bail
   */
  readonly id: number;

  /**
   * Date de prise d'effet du bail
   */
  datePriseEffet: string;

  /**
   * Durée du bail
   */
  dureeBail: string;

  /**
   * Durée du bail si dureeBail réduite
   */
  dureeBailReduite: string;

  /**
   * Justification de la réduction de la durée du bail
   */
  justificationDureeBailReduite: string;

  /**
   * Loyer mensuel HT
   */
  loyerHorsCharges: number;

  /**
   * Montant des charges récupérables
   */
  chargesRecuperables: number;

  /**
   * Le loyer du logement objet du présent contrat est soumis au décret fixant annuellement le montant maximum d'évolution des loyers à la relocation
   */
  isMontantMaximumAnnuel: boolean;

  /**
   * Le loyer du logement objet du présent contrat est soumis au loyer de référence majoré fixé par arrêté préfectoral
   */
  isLoyerReference: boolean;

  /**
   * Montant du loyer de référence
   */
  loyerReference: number;

  /**
   * Périodicité de révision du loyer
   */
  periodicite: string;

  /**
   * Montant du dernier loyer de l'ancien locataire
   */
  loyerAncienLocataire: number;

  /**
   * Modalité de règlement des charges récupérables
   */
  modaliteReglementChargesRecuperables: string;

  /**
   * Montant des charges récupérables
   */
  montantChargesRecuperables: number;

  /**
   * En cas de colocation, le bailleur a-t-il souscrit une assurance pour les locataires ?
   */
  souscriptionAssuranceParBailleur: boolean;

  /**
   * Périodicité du paiement du loyer
   */
  periodicitePaiementLoyer: string;

  /**
   * Echeance du paiement
   */
  echeancePaiement: string;

  /**
   * Date ou période de paiement
   */
  datePeriodePaiement: string;

  /**
   * Montant de la première échéance
   */
  montantPremiereEcheance: number;

  /**
   * Montant total annuel récupérable au titre de l’assurance pour compte des colocataires
   */
  montantRecuperableAssurance: number;

  /**
   * Montant récupérable par douzième
   */
  montantRecuperableAssuranceDouzieme: number;

  /**
   * Liste des majorations liées à des travaux
   */
  majorations: Majoration[];

  /**
   * Montant des garanties
   */
  montantGaranties: number;

  /**
   * "habitation" ou "mixte professionnel et habitation"
   * usage du bien pour
   */
  usageLocaux: string;

  /**
   * Signature du proprietaire
   */
  signatureProprietaire: boolean;

  /**
   * Signature du locataire
   */
  signatureLocataire: boolean;

  /**
   * nom du pdf du bail
   */
  pdf: string;

  /**
   * nom du fichier d'assurance proprietaire
   */
  assuranceProprietaire: string;

  /**
   * Bien du bail
   */
  bien: Bien;
  /**
   * Locataire du bail
   */
  locataire: Utilisateur;

  /**
   * état des lieux d'entrée
   */
  edlEntree: string;

  /**
   * état des lieux de sortie
   */
  edlSortie: string;

  /**
   * Constructeur de l'objet Bail
   * @param options Propriétés de l'objet
   */
  constructor(options: {
    id: number;
    datePriseEffet: string;
    dureeBail: string;
    dureeBailReduite: string;
    justificationDureeBailReduite: string;
    loyerHorsCharges: number;
    chargesRecuperables: number;
    isMontantMaximumAnnuel: boolean;
    isLoyerReference: boolean;
    loyerReference: number;
    periodicite: string;
    loyerAncienLocataire: number;
    modaliteReglementChargesRecuperables: string;
    montantChargesRecuperables: number;
    souscriptionAssuranceParBailleur: boolean;
    periodicitePaiementLoyer: string;
    echeancePaiement: string;
    datePeriodePaiement: string;
    montantPremiereEcheance: number;
    montantRecuperableAssurance: number;
    montantRecuperableAssuranceDouzieme: number;
    majorations: Majoration[];
    montantGaranties: number;
    usageLocaux: string;
    signatureProprietaire: boolean;
    signatureLocataire: boolean;
    pdf: string;
    assuranceProprietaire: string;
    locataire: Utilisateur;
    bien: Bien;
    edlEntree: string;
    edlSortie: string;
  }) {
    this.id = options.id || -1;
    this.datePriseEffet = options.datePriseEffet || '';
    this.dureeBail = options.dureeBail || '';
    this.dureeBailReduite = options.dureeBailReduite || '';

    this.justificationDureeBailReduite =
      options.justificationDureeBailReduite || '';

    this.loyerHorsCharges = options.loyerHorsCharges || 0;
    this.chargesRecuperables = options.chargesRecuperables || 0;
    this.isMontantMaximumAnnuel = options.isMontantMaximumAnnuel || false;
    this.isLoyerReference = options.isLoyerReference || false;
    this.loyerReference = options.loyerReference || 0;
    this.periodicite = options.periodicite || '';
    this.loyerAncienLocataire = options.loyerAncienLocataire || 0;
    this.modaliteReglementChargesRecuperables =
      options.modaliteReglementChargesRecuperables || '';
    this.montantChargesRecuperables = options.montantChargesRecuperables || 0;
    this.souscriptionAssuranceParBailleur =
      options.souscriptionAssuranceParBailleur || false;
    this.periodicitePaiementLoyer = options.periodicitePaiementLoyer || '';
    this.echeancePaiement = options.echeancePaiement || '';
    this.datePeriodePaiement = options.datePeriodePaiement || '';
    this.montantPremiereEcheance = options.montantPremiereEcheance || 0;
    this.montantRecuperableAssurance = options.montantRecuperableAssurance || 0;

    this.montantRecuperableAssuranceDouzieme =
      options.montantRecuperableAssuranceDouzieme || 0;

    this.majorations = options.majorations || [];

    this.montantGaranties = options.montantGaranties || 0;
    this.usageLocaux = options.usageLocaux || '';
    this.signatureProprietaire = options.signatureProprietaire || false;
    this.signatureLocataire = options.signatureLocataire || false;
    this.pdf = options.pdf || '';
    this.locataire = options.locataire || '';
    this.bien = options.bien || '';
    this.edlEntree = options.edlEntree || '';
    this.edlSortie = options.edlSortie || '';
    this.assuranceProprietaire = options.assuranceProprietaire || '';
  }

  /**
   * Transforme un json en l'objet
   * @param json json de bail
   * @returns objet bail
   */
  public static fromJson(json: any): Bail {
    return new Bail({
      id: json.BAIL_id,
      datePriseEffet: json.BAIL_date_prise_effet,
      dureeBail: json.BAIL_duree,
      dureeBailReduite: json.BAIL_duree_reduite,
      justificationDureeBailReduite: json.BAIL_justification_duree_bail_reduite,
      loyerHorsCharges: json.BAIL_loyer_hc,
      chargesRecuperables: json.BAIL_charges_recuperables,
      isMontantMaximumAnnuel: !!json.BAIL_is_montant_maximum_annuel,
      isLoyerReference: !!json.BAIL_is_loyer_reference,
      loyerReference: json.BAIL_loyer_leference,
      periodicite: json.BAIL_periodicite,
      loyerAncienLocataire: json.BAIL_loyer_ancien_locataire,
      modaliteReglementChargesRecuperables:
        json.BAIL_modalite_reglement_charges_recuperables,
      montantChargesRecuperables: json.BAIL_montant_charges_recuperables,
      souscriptionAssuranceParBailleur: !!json.BAIL_souscription_assurance_par_bailleur,
      periodicitePaiementLoyer: json.BAIL_periodicite_paiement_poyer,
      echeancePaiement: json.BAIL_echeance_paiement,
      datePeriodePaiement: json.BAIL_date_periode_paiement,
      montantPremiereEcheance: json.BAIL_montant_premiere_echeance,
      montantRecuperableAssurance: json.BAIL_montant_recuperable_assurance,
      montantRecuperableAssuranceDouzieme:
        json.BAIL_montant_recuperable_assurance_douzieme,
      majorations: json.BAIL_majorations,
      montantGaranties: json.BAIL_montant_garanties,
      usageLocaux: json.BAIL_usage_locaux,
      signatureProprietaire: !!json.BAIL_signature_proprietaire,
      signatureLocataire: !!json.BAIL_signature_locataire,
      pdf: json.BAIL_pdf,
      locataire: Utilisateur.fromJson({
        USER_id: json.BAIL_id_locataire,
      }),
      bien: Bien.fromJson({ BIEN_ID: json.BAIL_id_bien }),
      edlEntree: json.BAIL_edl_entree,
      edlSortie: json.BAIL_edl_sortie,
      assuranceProprietaire: json.BAIL_assurance_proprietaire,
    });
  }
}

/**
 * interface de majoration
 */
export interface Majoration {
  /**
   * nature de la majoration
   */
  nature: string;
  /**
   * modalité de la majoration
   */
  modalites: string;
  /**
   * délai réalisation travaux
   */
  delaiRealisation: string;
  /**
   * montant réalisation
   */
  montant: string;
}
