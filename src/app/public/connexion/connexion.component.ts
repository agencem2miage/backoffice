import { TokenService } from './../../core/services/token.service';
import { AuthentificationService } from './../../core/services/authentification.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Constants } from '../../constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss'],
})
export class ConnexionComponent implements OnInit {
  /**
   * Formulaire de connexion
   */
  loginForm!: FormGroup;

  /**
   * Le formulaire a été soumis
   */
  submitted!: boolean;

  /**
   * constructeur du guard
   * @param constants
   * @param fb
   * @param authentificationService
   * @param tokenService
   * @param router
   */
  constructor(
    public constants: Constants,
    private fb: FormBuilder,
    private authentificationService: AuthentificationService,
    private tokenService: TokenService,
    private router: Router
  ) {}

  /**
   * Init du composant
   */
  ngOnInit(): void {
    this.initForm();
  }

  /**
   * Initialisation du formulaire
   */
  initForm(): void {
    this.loginForm = this.fb.group({
      mail: ['', [Validators.required]],
      mdp: ['', [Validators.required]],
    });
  }

  /**
   * méthode appelée lors de la soumission du formulaire.
   * Elle appelle l'API de connexion
   */
  onSubmit(): void {
    this.submitted = true;

    this.authentificationService
      .login(
        this.loginForm.controls.mail.value,
        this.loginForm.controls.mdp.value
      )
      .subscribe((result) => {
        this.tokenService.saveToken(result.token);
        this.tokenService.saveUser(result.user);
        this.router.navigate(['/admin/profil']);
      });
  }
}
