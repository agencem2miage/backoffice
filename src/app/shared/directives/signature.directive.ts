import {
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  /**
   * Selecteur de signature
   */
  selector: '[signature]',
})
/**
 * Directive de signature
 */
export class SignatureDirective implements OnInit, OnChanges {
  /**
   * TODO
   */
  @Output() imgSrc = new EventEmitter<string>();
  /**
   * TODO
   */
  @Input('clear') clearMe: any;
  /**
   * TODO
   */
  @Input('options') options: any;

  /**
   * contexte
   */
  private context: any;

  /**
   * boolean si l'utilisateur est en train d'écrire ou non
   */
  private isDrawing = false;

  /**
   * zone de signature
   */
  private sigPadElement: any;

  /**
   * initialisation de la directive
   */
  ngOnInit(): void {
    this.sigPadElement = this.signaturePad.nativeElement;
    this.context = this.sigPadElement.getContext('2d');
    this.context.strokeStyle = this.options.color || '#3742fa';
    this.context.lineWidth = this.options.width || 2;

    if (this.options.blur) {
      this.context.shadowBlur = this.options.blur || 2;
      this.context.shadowColor =
        this.options.blurColor || this.context.strokeStyle;
    }

    this.context.lineCap = 'round';
    this.context.lineJoin = 'round';
  }

  /**
   *
   * @param changes Mise à jour de la directive en fonction des changements
   * @returns void
   */
  ngOnChanges(changes: SimpleChanges): void {
    if (!this.context) {
      return;
    }

    if (changes.clearMe) {
      this.clear();
      this.save();
    }
  }

  /**
   * constructuer de la directive
   * @param signaturePad
   */
  constructor(private signaturePad: ElementRef) {}

  /**
   * Evenement si l'utilisateur commence à écrire
   */
  @HostListener('touchstart', ['$event'])
  handleTouchStart(e: any): void {
    e.preventDefault();
    this.isDrawing = true;
    this.start(e, 'layer');
  }

  /**
   * Evenement si l'utilisateur écrit
   */
  @HostListener('touchmove', ['$event'])
  handleTouchMove(e: any): void {
    if (!this.isDrawing) {
      return;
    }

    e.preventDefault();
    this.draw(e, 'layer');
  }
  /**
   * si l'utilisateur écrit vers le haut
   */
  @HostListener('document:touchend', ['$event'])
  @HostListener('document:mouseup', ['$event'])
  onMouseUp(): void {
    if (this.isDrawing) {
      this.save();
    }

    this.isDrawing = false;
  }
  /**
   * Si l'utilisateur écrit vers le bas
   */
  @HostListener('mousedown', ['$event'])
  onMouseDown(e: any): void {
    this.isDrawing = true;
    this.start(e);
  }
  /**
   * Evenement si la souris bouge
   */
  @HostListener('mousemove', ['$event'])
  onMouseMove(e: any): void {
    if (!this.isDrawing) {
      return;
    }

    this.draw(e);
  }

  /**
   * méthode de démarrage de la signature
   * @param e événement
   * @param type utilisateur
   */
  private start(e: any, type: 'client' | 'layer' = 'client') {
    const coords = this.relativeCoords(e, type);
    this.context.moveTo(coords.x, coords.y);
  }

  /**
   * méthode de dessin
   * @param e événement
   * @param type utilisateur
   */
  private draw(e: any, type: 'client' | 'layer' = 'client') {
    const coords = this.relativeCoords(e, type);
    this.context.lineTo(coords.x, coords.y);
    this.context.stroke();
  }

  /**
   * Coordonnées relatives
   * @param event événement
   * @param type utilisateur
   * @returns coordonnées
   */
  private relativeCoords(event: any, type: 'client' | 'layer') {
    const bounds = this.sigPadElement.getBoundingClientRect();
    let x = event[type + 'X'];
    let y = event[type + 'Y'];

    if ('layer' !== type) {
      x -= bounds.left;
      y -= bounds.top;
    }

    return { x, y };
  }

  /**
   * supprime l'écriture
   */
  clear(): void {
    this.context.clearRect(
      0,
      0,
      this.sigPadElement.width,
      this.sigPadElement.height
    );
    this.context.beginPath();
  }

  /**
   * Sauvegarde de l'écriture
   */
  save(): void {
    this.imgSrc.emit(this.sigPadElement.toDataURL('image/png'));
  }
}
