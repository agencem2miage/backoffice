import { Injectable } from '@angular/core';

import { environment } from '../environments/environment';
@Injectable({
  providedIn: 'root',
})
/**
 * Liste des constantes de l'application
 */
export class Constants {
  /**
   * Points d'entrée de l'API
   */
  public API_ENDPOINTS = Object.freeze({
    // Utilisation GET, POST, PUT, DELETE
    USER: '/users',
    BAIL: '/bail',
    BIEN: '/bien',
    ADRESSE: '/address',
    IMAGES: '/images',
    CONNEXION: '/connexion',
    REGISTER: '/register',
    PIECES: '/pieces',
    FILE: '/file',
  });

  /**
   * Chemin vers les fichiers pdf à télécharger
   */
  public PATH_FILE_LOCATION = Object.freeze({
    BAIL: '/1.bail/',
    ETAT_LIEU: '/2.etat-lieu/',
    ASSURANCE_PROPRIETAIRE: '/3.assurance-proprietaire/',
    ASSURANCE_LOCATAIRE: '/4.assurance-proprietaire/',
    CONTRAT_GESTION: '/5.contrat-gestion/',
    DIAG_ENERGIE: '/6.diagnostique-energetique/',
    AUTRE: '/7.autre/',
  });

  /**
   * DATE
   */
  public CURRENT_DATE = Date.now();

  /**
   * INFO du site
   */
  public FRONT_URL = environment.BACK_URL;
  /**
   * URL actuel
   */
  public CURRENT_URL = window.location.href;
  public IMAGE_URL = this.FRONT_URL + '/assets/images/';
  public SITE_NAME = 'Back office Agencetourix';
  /**
   * date de création
   */
  public ORG_FUNDING_DATE = '2021';
  /**
   * fondateur du site
   */
  public FOUNDER_NAME = 'Alain Guimard';

  /**
   * nom contact
   */
  public CONTACT_NAME = 'John Doe';
  /**
   * mail contact
   */
  public CONTACT_EMAIL = 'john.doe@gmail.com';
  /**
   * téléphone du contact
   */
  public CONTACT_PHONE = '06 05 07 08 09';
  /**
   * adresse du contact
   */
  public CONTACT_ADDRESS = '10 rue de la paix';
  /**
   * ville du contact
   */
  public CONTACT_CITY = 'Bordeaux';
  /**
   * code postal du contact
   */
  public CONTACT_CP = '33000';

  /**
   * titre du site
   */
  public SEO_SITE_TITLE = this.SITE_NAME + ' : ';
  /**
   * description du site
   */
  public SEO_SITE_DESCRIPTION = 'Description SEO';
}

/**
 * Types de role et leur identifiant
 */
export enum Roles {
  ADMIN = 1,
  AGENT = 2,
  PROPRIETAIRE = 3,
  LOCATAIRE = 4,
}

/**
 * Types de bien et id BDD
 */
export enum TypesBien {
  MAISON = 1,
  APPARTEMENT = 2,
  AUTRE = 3,
}

/**
 * Type contrat et id en BDD
 */
export enum TypesContrat {
  LOCATION = 1,
}

/**
 * Type piece et id en bdd
 */
export enum TypesPiece {
  CUISINE = 1,
  SALLE_DE_BAIN = 2,
  SALON = 3,
  SALLE_A_MANGER = 4,
  SALLE_EAU = 5,
  CHAMBRE = 6,
  TOILETTES = 7,
}

/**
 * regime juridique et id en bdd
 */
export enum RegimesJuridiques {
  MONOPROPRIETE = 1,
  COPROPRIETE = 2,
}

/**
 * usagge locaux et id bdd
 */
export enum UsagesLocaux {
  HABITATION = 1,
  MIXTE = 2,
}

/**
 * duree bail et id en bdd
 */
export enum DureesBail {
  REDUITE = 1,
  TROIS_ANS = 2,
  SIX_ANS = 3,
}

/**
 * civilite mossibles
 */
export enum Civilites {
  MADAME = 'Madame',
  MONSIEUR = 'Monsieur',
}

/**
 * modalités réglements charges récupérables
 */
export enum ModalitesReglementChargesRecuperables {
  PROVISIONS_SUR_CHARGES = 'Provisions sur charges',
  PAIEMENT_PERIODIQUE = 'Paiement périodique',
  FORFAIT_CHARGE = 'Forfait charge',
}

/**
 * échéances de paiement possible
 */
export enum EcheancesPaiement {
  ECHOIR = 'A échoir',
  ECHU = 'A terme échu',
}

/**
 * etat equipement
 */
export enum EtatsEquipements {
  TRES_BON,
  BON,
  MOYEN,
  MAUVAIS,
}

/**
 * type meuble
 */
export enum TypeMeuble {
  NON_MEUBLÉ = 0,
  MEUBLÉ = 1,
}
