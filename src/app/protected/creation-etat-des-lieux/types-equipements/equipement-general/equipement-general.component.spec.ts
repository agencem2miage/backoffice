import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipementGeneralComponent } from './equipement-general.component';

describe('EquipementGeneralComponent', () => {
  let component: EquipementGeneralComponent;
  let fixture: ComponentFixture<EquipementGeneralComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipementGeneralComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipementGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
