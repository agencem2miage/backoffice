import {
  HTTP_INTERCEPTORS,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { TokenService } from './../services/token.service';

/**
 * Intercepteur permettant d'inclure le header avec le token de connexion de l'administrateur
 */
@Injectable()
export class AdminAuthInterceptor implements HttpInterceptor {
  /**
   * Constructeur de l'intercepteur
   * @param TokenService Service de gestion des tokens
   */
  constructor(private TokenService: TokenService) {}

  /**
   * Intercepte la requête sortante et ajoute le token de connexion si présent
   * @param request La requête sortante/entrante
   * @param next Passe la main à l'intercepteur suivant
   * @returns La requête avec le header contenant le token de connexion
   */
  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    let authRequest = request;
    const authToken = this.TokenService.getToken();

    if (authToken) {
      authRequest = request.clone({
        headers: request.headers.set('x-auth-token', authToken),
      });
    }

    return next.handle(authRequest);
  }
}

/**
 * Export de l'intercepteur pour les providers du CoreModule
 */
export const adminAuthInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AdminAuthInterceptor, multi: true },
];
