import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Constants } from 'src/app/constants';
import { environment } from 'src/environments/environment';
import { Bail } from 'src/app/shared/models/bail.model';

/**
 * Définit le content-type du header
 */
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
/**
 * Service de bail
 */
export class BailService {
  /**
   * Point d'entrée de l'API pour la gestion des bails
   */
  bailEndpoint = environment.API_URL + this.constants.API_ENDPOINTS.BAIL;

  /**
   * Constructeur du service
   * @param http Service natif d'angular pour la gestion de requêtes HTTP
   * @param constants Constantes de l'application
   */
  constructor(private http: HttpClient, private constants: Constants) {}

  getBailBien(idBien: number): Observable<any> {
    return this.http.get(this.bailEndpoint + '/' + idBien).pipe(
      map((jsonResponse: any) => {
        if (jsonResponse.length > 0) {
          return Bail.fromJson(jsonResponse[0]);
        } else {
          return Bail.fromJson({});
        }
      })
    );
  }

  /**
   * création d'un nouveau bail
   * @param bail
   * @returns
   */
  creerBail(bail: Bail): Observable<unknown> {
    return this.http.post(this.bailEndpoint, { bail }, httpOptions);
  }

  /**
   * sauvegarde d'un bail
   * @param bail
   * @returns
   */
  saveBail(bail: Bail): Observable<unknown> {
    return this.http.post(this.bailEndpoint + '/save', { bail }, httpOptions);
  }

  /**
   * récupération liste des bails
   * @returns
   */
  getBails(): Observable<Bail[]> {
    return this.http
      .get(this.bailEndpoint)
      .pipe(
        map((jsonResponse: any) =>
          jsonResponse.map((jsonBail: any) => Bail.fromJson(jsonBail))
        )
      );
  }
}
