import { CommonModule } from '@angular/common';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgImageSliderModule } from 'ng-image-slider';

import { DetailsBienComponent } from './details-bien/details-bien.component';
import { ListBienComponent } from './list-bien/list-bien.component';
import { ProtectedRoutingModule } from './protected-routing.module';
import { CreationCompteComponent } from './creation-compte/creation-compte.component';
import { SuppressionCompteComponent } from './suppression-compte/suppression-compte.component';
import { Ng2CompleterModule } from 'ng2-completer';
import { ProfileComponent } from './profile/profile.component';
import { CreationBailModule } from './creation-bail/creation-bail.module';
import { CreationEtatDesLieuxModule } from './creation-etat-des-lieux/creation-etat-des-lieux.module';
import { UploadComponent } from './upload/upload.component';
import { FormsModule } from '@angular/forms';
import { CreationBienComponent } from './creation-bien/creation-bien.component';

@NgModule({
  declarations: [
    DetailsBienComponent,
    ListBienComponent,
    CreationCompteComponent,
    SuppressionCompteComponent,
    ProfileComponent,
    UploadComponent,
    CreationBienComponent,
  ],
  imports: [
    CommonModule,
    CreationBailModule,
    ProtectedRoutingModule,
    ReactiveFormsModule,
    Ng2CompleterModule,
    NgImageSliderModule,
    FormsModule,
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class ProtectedModule {}
