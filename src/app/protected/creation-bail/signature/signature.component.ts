import {
  Constants,
  DureesBail,
  ModalitesReglementChargesRecuperables,
  RegimesJuridiques,
  TypesBien,
  UsagesLocaux,
} from './../../../constants';
import { CreationBailProvider } from 'src/app/core/providers/creation-bail.provider';
import { BailService } from './../../../core/services/bail.service';
import { Component, OnInit } from '@angular/core';
import { Bail } from 'src/app/shared/models/bail.model';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import Utils from 'src/app/utils';
import { DatePipe } from '@angular/common';

(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-signature',
  templateUrl: './signature.component.html',
  styleUrls: ['./signature.component.scss'],
})
/**
 * classe de signature
 */
export class SignatureComponent implements OnInit {
  /**
   * source de la signature
   */
  src: { bailleur: string; locataire: string } = {
    bailleur: '',
    locataire: '',
  };
  /**
   * vidage des signatures
   */
  clearMe: { bailleur: boolean; locataire: boolean } = {
    bailleur: false,
    locataire: false,
  };
  /**
   * signature
   */
  signing: { bailleur: boolean; locataire: boolean } = {
    bailleur: true,
    locataire: true,
  };

  /**
   * font du doc
   */
  docDefinition: any = {
    styles: {
      titre: {
        fontSize: 15,
        bold: true,
        alignment: 'center' as const,
      },
      sousTitre: {
        fontSize: 10,
        alignement: 'center' as const,
      },
      partie: {
        fontSize: 18,
        bold: true,
        margin: [0, 20, 0, 10] as [number, number, number, number],
      },
      sousPartie: {
        fontSize: 16,
        bold: true,
        margin: [0, 10, 0, 5] as [number, number, number, number],
      },
      paragraphe: {
        margin: [0, 10, 0, 0] as [number, number, number, number],
      },
    },
  };

  /**
   * formulaire du bail
   */
  bailForms = this.creationBailProvider.formGroups;

  /**
   * premiere page du formulaire bail
   */
  designationParties = this.bailForms.find(
    (form) => form.formGroupLabel === 'designationParties'
  )?.formGroup.controls;
  /**
   * deuxieme page du formulaire bail
   */
  objetContrat = this.bailForms.find(
    (form) => form.formGroupLabel === 'objetContrat'
  )?.formGroup.controls;
  /**
   * troisieme page du formulaire bail
   */
  priseEffetContrat = this.bailForms.find(
    (form) => form.formGroupLabel === 'priseEffetContrat'
  )?.formGroup.controls;
  /**
   * quatrieme page du formulaire bail
   */
  conditionsFinancieres = this.bailForms.find(
    (form) => form.formGroupLabel === 'conditionsFinancieres'
  )?.formGroup.controls;
  /**
   * cinquieme page du formulaire bail
   */
  travauxGaranties = this.bailForms.find(
    (form) => form.formGroupLabel === 'travauxGaranties'
  )?.formGroup.controls;
  /**
   * sixieme page du formulaire bail
   */
  choixPartiesBien = this.bailForms.find(
    (form) => form.formGroupLabel === 'choixPartiesBien'
  )?.formGroup.controls;

  /**
   * constructeur du component de signature
   * @param bailService
   * @param creationBailProvider
   * @param constants
   */
  constructor(
    private bailService: BailService,
    private creationBailProvider: CreationBailProvider,
    private constants: Constants
  ) {}

  /**
   * méthode d'initialisation du component
   */
  async ngOnInit(): Promise<void> {
    this.docDefinition.content = [
      {
        columns: [
          {
            image: '',
            width: 150,
            height: 150,
            margin: 0,
          },
          [
            {
              text: 'Contrat de location ou de colocation de logement vide',
              style: 'titre',
            },
            {
              stack: [
                'Soumis au titre Ier de la loi du 6 juillet 1989 tendant à améliorer',
                'les rapports locatifs et portant modification de la loi n° 86-1290 du',
                '23 décembre 1986',
              ],
              style: 'sousTitre',
            },
          ],
        ],
      },
      this.designationPartiesPdf,
      this.consistanceLogementPdf,
      this.destinationLocauxPdf,
      this.enumerationLocauxPdf,
      this.priseEffetContratPdf,
      this.conditionsFinancieresPdf,
      this.travauxPdf,
      this.garantiesPdf,
      {
        pageBreak: 'before' as const,
        stack: [
          {
            text: 'VII. Clause de solidarité',
            style: 'partie',
          },
          "Modalités particulières des obligations en cas de pluralité de locataires : en cas de colocation, c'est à dire de la location d’un même logement par plusieurs locataires, constituant leur résidence principale et formalisée par la conclusion d’un contrat unique ou de plusieurs contrats entre les locataires et le bailleur, les locataires sont tenus conjointement, solidairement et indivisiblement à l’égard du bailleur au paiement des loyers, charges et accessoires dus en application du présent bail. La solidarité d'un des colocataires et celle de la personne qui s'est portée caution pour lui prennent fin à la date d'effet du congé régulièrement délivré et lorsqu'un nouveau colocataire figure au bail. A défaut, la solidarité du colocataire sortant s'éteint au plus tard à l'expiration d'un délai de six mois après la date d'effet du congé.",
        ],
      },
      {
        stack: [
          {
            text: 'VIII. Clause résolutoire',
            style: 'partie',
          },
          "Modalités de résiliation de plein droit du contrat : Le bail sera résilié de plein droit en cas d'inexécution des obligations du locataire, soit en cas de défaut de paiement des loyers et des charges locatives au terme convenu, de non-versement du dépôt de garantie, de défaut d'assurance du locataire contre les risques locatifs, de troubles de voisinage constatés par une décision de justice passée en force de chose jugée rendue au profit d'un tiers. Le bailleur devra assigner le locataire devant le tribunal pour faire constater l'acquisition de la clause résolutoire et la résiliation de plein droit du bail. Lorsque le bailleur souhaite mettre en œuvre la clause résolutoire pour défaut de paiement des loyers et des charges ou pour nonversement du dépôt de garantie, il doit préalablement faire signifier au locataire, par acte d'huissier, un commandement de payer, qui doit mentionner certaines informations et notamment la faculté pour le locataire de saisir le fonds de solidarité pour le logement. De plus, pour les bailleurs personnes physiques ou les sociétés immobilières familiales, le commandement de payer doit être signalé par l'huissier à la commission de coordination des actions de prévention des expulsions locatives dès lors que l'un des seuils relatifs au montant et à l'ancienneté de la dette, fixé par arrêté préfectoral, est atteint. Le locataire peut, à compter de la réception du commandement, régler sa dette, saisir le juge d'instance pour demander des délais de paiement, voire demander ponctuellement une aide financière à un fonds de solidarité pour le logement. Si le locataire ne s'est pas acquitté des sommes dues dans les deux mois suivant la signification, le bailleur peut alors assigner le locataire en justice pour faire constater la résiliation de plein droit du bail. En cas de défaut d'assurance, le bailleur ne peut assigner en justice le locataire pour faire constater l'acquisition de la clause résolutoire qu'après un délai d'un mois après un commandement demeuré infructueux. Clause applicable selon les modalités décrites au paragraphe 4.3.2.1. de la notice d'information jointe au présent bail.",
        ],
      },
      {
        pageBreak: 'before' as const,
        columns: [],
      },
    ];

    await Utils.imageUrlToBase64(this.constants.IMAGE_URL + 'logo.png').then(
      (base64Image) =>
        (this.docDefinition.content[0].columns[0].image =
          base64Image?.toString() || '')
    );
  }

  /**
   * changement des sources
   * @param src
   * @param partie
   */
  srcChange(src: any, partie: string): void {
    partie === 'bailleur'
      ? (this.src.bailleur = src)
      : (this.src.locataire = src);
  }

  /**
   * vidage du formulaire
   * @param partie
   */
  clear(partie: string): void {
    partie === 'bailleur'
      ? (this.clearMe.bailleur = !this.clearMe.bailleur)
      : (this.clearMe.locataire = !this.clearMe.locataire);
  }

  /**
   * sauvegarde du bail
   * @param partie
   */
  save(partie: string): void {
    const date = new Date();

    partie === 'bailleur'
      ? (this.signing.bailleur = false)
      : (this.signing.locataire = false);

    this.docDefinition.content[
      this.docDefinition.content.length - 1
    ].columns.push([
      {
        stack: [
          {
            text: 'Signature ' + partie,
            style: 'partie',
          },
          {
            text:
              'Le ' +
              date.getDate() +
              '/' +
              (date.getMonth() + 1) +
              '/' +
              date.getFullYear(),
          },
          {
            image:
              partie === 'bailleur' ? this.src.bailleur : this.src.locataire,
            width: 150,
            height: 150,
            margin: 0,
          },
        ],
      },
    ]);
  }

  /**
   * méthode de soumission du formulaire
   */
  async onSubmit(): Promise<void> {
    pdfMake.createPdf(this.docDefinition).getBase64((base64Pdf) => {
      const nouveauBail = new Bail({
        id: -1,
        loyerHorsCharges: parseFloat(
          this.conditionsFinancieres?.loyerHorsCharges.value
        ),
        usageLocaux: this.objetContrat?.usageLocaux.value,
        datePriseEffet: this.priseEffetContrat?.datePriseEffet.value,
        dureeBail: this.priseEffetContrat?.dureeBail.value,
        dureeBailReduite: this.priseEffetContrat?.dureeBailReduite?.value || '',
        justificationDureeBailReduite:
          this.priseEffetContrat?.justificationDureeBailReduite?.value || '',
        chargesRecuperables: parseFloat(
          this.conditionsFinancieres?.chargesRecuperables.value
        ),
        isMontantMaximumAnnuel: this.conditionsFinancieres
          ?.isMontantMaximumAnnuel.value,
        isLoyerReference: this.conditionsFinancieres?.isLoyerReference.value,
        loyerReference: parseFloat(
          this.conditionsFinancieres?.loyerReference.value
        ),
        periodicite: this.conditionsFinancieres?.periodicite.value,
        loyerAncienLocataire: parseFloat(
          this.conditionsFinancieres?.loyerAncienLocataire.value
        ),
        modaliteReglementChargesRecuperables: this.conditionsFinancieres
          ?.modaliteReglementChargesRecuperables.value,
        montantChargesRecuperables: parseFloat(
          this.conditionsFinancieres?.montantChargesRecuperables.value
        ),
        souscriptionAssuranceParBailleur: this.conditionsFinancieres
          ?.souscriptionAssuranceParBailleur.value,
        periodicitePaiementLoyer: this.conditionsFinancieres
          ?.periodicitePaiementLoyer.value,
        echeancePaiement: this.conditionsFinancieres?.echeancePaiement.value,
        datePeriodePaiement: this.conditionsFinancieres?.datePeriodePaiement
          .value,
        montantPremiereEcheance: parseFloat(
          this.conditionsFinancieres?.montantPremiereEcheance.value
        ),
        montantRecuperableAssurance: parseFloat(
          this.conditionsFinancieres?.montantRecuperableAssurance?.value || '0'
        ),
        montantRecuperableAssuranceDouzieme: parseFloat(
          this.conditionsFinancieres?.montantRecuperableAssuranceDouzieme
            ?.value || '0'
        ),
        majorations: this.travauxGaranties?.majorations.value,
        montantGaranties: parseFloat(
          this.travauxGaranties?.montantGaranties.value
        ),
        pdf: base64Pdf,
        signatureLocataire: true,
        signatureProprietaire: true,
        locataire: this.choixPartiesBien?.locataire.value,
        bien: this.choixPartiesBien?.bien.value,
        edlEntree: '',
        edlSortie: '',
        assuranceProprietaire: '',
      });

      this.bailService.creerBail(nouveauBail).subscribe();
    });
  }

  /**
   * génération du bail pdf
   * @param action
   */
  genererBailPDF(action = 'download'): void {
    if (action === 'download') {
      pdfMake
        .createPdf(this.docDefinition)
        .download(
          '[' +
            this.creationBailProvider.bien.id +
            '] [' +
            this.designationParties?.bailleurNom.value +
            '] [' +
            this.designationParties?.locataireNom.value +
            ']'
        );
    } else if (action === 'print') {
      pdfMake.createPdf(this.docDefinition).print();
    } else {
      pdfMake.createPdf(this.docDefinition).open();
    }
  }

  /**
   * désignation des parties
   */
  get designationPartiesPdf(): any {
    return {
      stack: [
        {
          text: 'I. Désignation des parties',
          style: 'partie',
        },
        'Le présent contrat est conclu entre les soussignés :',
        ' ',
        this.designationParties?.bailleurTitre.value +
          ' ' +
          this.designationParties?.bailleurNom.value +
          ' ' +
          this.designationParties?.bailleurPrenom.value +
          ', ',
        'né le ' +
          new DatePipe('fr-FR').transform(
            this.designationParties?.bailleurDateNaissance.value
          ) +
          ' à ' +
          this.designationParties?.bailleurLieuNaissance.value,
        'Demeurant ' +
          this.designationParties?.bailleurAdresse.value +
          ', ' +
          this.designationParties?.bailleurCodePostal.value +
          ' ' +
          this.designationParties?.bailleurVille.value,
        ' ',
        "CI-APRÈS DÉNOMMÉ LE BAILLEUR d'une part,",
        ' ',
        this.designationParties?.locataireTitre.value +
          ' ' +
          this.designationParties?.locataireNom.value +
          ' ' +
          this.designationParties?.locatairePrenom.value +
          ',',
        'né le ' +
          new DatePipe('fr-FR').transform(
            this.designationParties?.locataireDateNaissance.value
          ) +
          ' à ' +
          this.designationParties?.locataireLieuNaissance.value,
        'Demeurant ' +
          this.designationParties?.locataireAdresse.value +
          ', ' +
          this.designationParties?.locataireCodePostal.value +
          ' ' +
          this.designationParties?.locataireVille.value,
        ' ',
        "CI-APRÈS DÉNOMMÉ LE LOCATAIRE d'autre part,",
        'Il a été convenu ce qui suit,',
      ],
      style: 'paragraphe',
    };
  }

  /**
   * partie consistance logement en pdf
   */
  get consistanceLogementPdf(): any {
    return {
      stack: [
        {
          text: 'II. Objet du contrat',
          style: 'partie',
        },
        "Le présent contrat a pour objet la location d'un logement ainsi déterminé :",
        {
          text: 'A. Consistance du logement',
          style: 'sousPartie',
        },
        'Adresse :',
        this.objetContrat?.rue.value +
          ', ' +
          this.objetContrat?.codePostal.value +
          ' ' +
          this.objetContrat?.ville.value,
        this.objetContrat?.complementAdresse.value || '',
        ' ',
        "Type d'habitat : " + this.typeBien,
        'Surface : ' + this.objetContrat?.surface.value + 'm²',
        'Meublé : ' + (this.objetContrat?.meuble.value ? 'Oui' : 'Non'),
        "Régime juridique de l'immeuble : " +
          (this.objetContrat?.regimeImmeuble.value ===
          RegimesJuridiques.MONOPROPRIETE
            ? 'Monopropriété'
            : 'Copropriété'),
        'Période de construction : ' + this.objetContrat?.construction.value,
        'Autres parties du logement : ' +
          this.objetContrat?.autresParties.value,
        "Éléments d'équipement du logement : " +
          this.objetContrat?.equipements.value,
      ],
      style: 'paragraphe',
    };
  }

  /**
   * récupération du type de bien
   */
  get typeBien(): string {
    switch (this.objetContrat?.typeHabitat.value) {
      case TypesBien.APPARTEMENT:
        return 'Appartement';
      case TypesBien.MAISON:
        return 'Maison';
      default:
        return 'Autre';
    }
  }

  /**
   * récupération de la destination des locaux en pdf
   */
  get destinationLocauxPdf(): any {
    return {
      stack: [
        {
          text: 'B. Destination des locaux',
          style: 'sousPartie',
        },
        'Usage : ' +
          (this.objetContrat?.usageLocaux.value === UsagesLocaux.HABITATION
            ? 'Habitation'
            : 'Mixte'),
      ],
      style: 'paragraphe',
    };
  }

  /**
   * énumération des locaux en pdf
   */
  get enumerationLocauxPdf(): any {
    return {
      stack: [
        {
          text:
            "C. Énumération des locaux, parties, équipements et accessoires de l'immeuble à usage commun",
          style: 'sousPartie',
        },
        'Garage à vélo : ' +
          (this.objetContrat?.garageVelo.value ? 'Oui' : 'Non'),
        'Espaces verts : ' +
          (this.objetContrat?.espacesVerts.value ? 'Oui' : 'Non'),
        'Local poubelles : ' +
          (this.objetContrat?.localPoubelles.value ? 'Oui' : 'Non'),
        'Ascenseur : ' + (this.objetContrat?.ascenseur.value ? 'Oui' : 'Non'),
        'Aire et équipements de jeux : ' +
          (this.objetContrat?.jeux.value ? 'Oui' : 'Non'),
        'Gardiennage : ' +
          (this.objetContrat?.gardiennage.value ? 'Oui' : 'Non'),
        'Laverie : ' + (this.objetContrat?.laverie.value ? 'Oui' : 'Non'),
      ],
      style: 'paragraphe',
    };
  }

  /**
   * prise d'effet contrat pdf
   */
  get priseEffetContratPdf(): any {
    const priseEffetContrat = {
      stack: [
        {
          text: "III. Date de prise d'effet et durée du contrat",
          style: 'partie',
        },
        'La durée du contrat et sa date de prise d’effet sont ainsi définies :',
        {
          text: 'A. Date de prise d’effet du contrat',
          style: 'sousPartie',
        },
        "Date de prise d'effet du bail : " +
          new DatePipe('fr-FR').transform(
            this.priseEffetContrat?.datePriseEffet.value
          ),
        {
          text: 'B. Durée du contrat',
          style: 'sousPartie',
        },
        'Durée du bail : ' + this.dureeBail,
      ],
    };

    if (this.priseEffetContrat?.dureeBail.value === DureesBail.REDUITE) {
      priseEffetContrat.stack.push(
        "Durée réduite (durée minimale d'un an lorsqu’un événement précis le justifie) : " +
          this.priseEffetContrat?.dureeBailReduite.value,
        {
          text:
            'C. Événement et raison justifiant la durée réduite du contrat de location',
          style: 'sousPartie',
        },
        "Justification de l'événement : " +
          this.priseEffetContrat?.justificationDureeBailReduite.value
      );
    }

    priseEffetContrat.stack.push(
      ' ',
      'En l’absence de proposition de renouvellement du contrat, celui-ci est, à son terme, reconduit tacitement pour 3 ou 6 ans et dans les mêmes conditions. Le locataire peut mettre fin au bail à tout moment, après avoir donné congé. Le bailleur, quant à lui, peut mettre fin au bail à son échéance et après avoir donné congé, soit pour reprendre le logement en vue de l’occuper lui-même ou une personne de sa famille, soit pour le vendre, soit pour un motif sérieux et légitime.'
    );

    return priseEffetContrat;
  }

  /**
   * Récupération durée bail
   */
  get dureeBail(): string {
    switch (this.priseEffetContrat?.dureeBail.value) {
      case DureesBail.REDUITE:
        return 'Durée réduite';
      case DureesBail.TROIS_ANS:
        return '3 ans';
      case DureesBail.SIX_ANS:
        return '6 ans';
      default:
        return 'N/C';
    }
  }

  /**
   * récupération conditions financieres pdf
   */
  get conditionsFinancieresPdf(): any {
    const conditionsFinancieres = {
      stack: [
        {
          text: 'IV. Conditions financières',
          style: 'partie',
        },
        'Les parties conviennent des conditions financières suivantes :',
        {
          text: 'A. Loyer',
          style: 'sousPartie',
        },
        'a) Loyer initial :',
        'Loyer mensuel : ' +
          this.conditionsFinancieres?.loyerMensuel.value +
          '€',
        'Loyer (hors charges) : ' +
          this.conditionsFinancieres?.loyerHorsCharges.value +
          '€',
        'Charges récupérables : ' +
          this.conditionsFinancieres?.chargesRecuperables.value +
          '€',
        ' ',
        'b) Modalités particulières de fixation initiale du loyer applicables dans les zones tendues :',
      ],
    };

    if (
      this.conditionsFinancieres?.isMontantMaximumAnnuel.value ||
      this.conditionsFinancieres?.isLoyerReference.value
    ) {
      this.conditionsFinancieres?.isMontantMaximumAnnuel.value
        ? conditionsFinancieres.stack.push(
            "Le loyer du logement objet du présent contrat est soumis au décret fixant annuellement le montant maximum d'évolution des loyers à la relocation"
          )
        : null;
      this.conditionsFinancieres?.isLoyerReference.value
        ? conditionsFinancieres.stack.push(
            'Le loyer du logement du présent contrat est soumis au loyer de référence majoré fixé par arrêté préfectoral'
          )
        : null;
    } else {
      conditionsFinancieres.stack.push('Sans objet');
    }

    conditionsFinancieres.stack.push(
      'Loyer de référence en €/m² : ' +
        this.conditionsFinancieres?.loyerReference.value +
        '€',
      'Date ou périodicité de révision : ' +
        this.conditionsFinancieres?.periodicite.value,
      ' ',
      'c) Informations relatives au loyer du dernier locataire',
      'Montant du dernier loyer acquitté par le précédent locataire : ' +
        this.conditionsFinancieres?.loyerAncienLocataire.value +
        '€',
      {
        text: 'B. Charges récupérables',
        style: 'sousPartie',
      },
      'Modalité de règlement des charges récupérables : ' +
        this.modalitesReglementChargesRecuperables
    );

    this.conditionsFinancieres?.montantChargesRecuperables.value
      ? conditionsFinancieres.stack.push(
          'Montant des provisions sur charges ou, en cas de colocation, du forfait de charge : ' +
            this.conditionsFinancieres?.montantChargesRecuperables.value +
            '€'
        )
      : null;

    conditionsFinancieres.stack.push({
      text:
        "C. Souscription par le bailleur d'une assurance pour le compte des colocataires",
      style: 'sousPartie',
    });

    if (this.conditionsFinancieres?.souscriptionAssuranceParBailleur.value) {
      conditionsFinancieres.stack.push(
        "Montant total annuel récupérable au titre de l'assurance pour compte des colocataires : " +
          this.conditionsFinancieres?.montantRecuperableAssurance.value,
        'Montant récupérable par douzième : ' +
          this.conditionsFinancieres?.montantRecuperableAssuranceDouzieme
            .value +
          '€'
      );
    } else {
      conditionsFinancieres.stack.push('Sans objet');
    }

    conditionsFinancieres.stack.push(
      {
        text: 'D. Modalités de paiement',
        style: 'sousPartie',
      },
      'Périodicité du paiement : ' +
        this.conditionsFinancieres?.periodicitePaiementLoyer.value,
      'Échéance : ' + this.conditionsFinancieres?.echeancePaiement.value,
      'Date ou période de paiement : ' +
        this.conditionsFinancieres?.datePeriodePaiement.value,
      'Montant total dû à la première échéance de paiement pour une période complète de location : ' +
        this.conditionsFinancieres?.montantPremiereEcheance.value +
        '€'
    );

    return conditionsFinancieres;
  }

  /**
   * récupération modalité reglement charges récupérables
   */
  get modalitesReglementChargesRecuperables(): string {
    switch (
      this.conditionsFinancieres?.modaliteReglementChargesRecuperables.value
    ) {
      case ModalitesReglementChargesRecuperables.PROVISIONS_SUR_CHARGES:
        return 'Provisions sur charges avec régularisation annuelle';
      case ModalitesReglementChargesRecuperables.PAIEMENT_PERIODIQUE:
        return 'Paiement périodique des charges sans provision';
      case ModalitesReglementChargesRecuperables.FORFAIT_CHARGE:
        return '[En cas de colocation seulement] Forfait de charges';
      default:
        return 'N/C';
    }
  }

  /**
   * récupération des travauc en pdf
   */
  get travauxPdf(): any {
    const travauxGaranties: any = {
      stack: [
        {
          text: 'V. Travaux',
          style: 'partie',
        },
        'Montant et nature des travaux d’amélioration ou de mise en conformité avec les caractéristiques de décence effectués depuis la fin du dernier contrat de location ou depuis le dernier renouvellement :',
        ' ',
      ],
    };

    const travaux = {
      layout: 'lightHorizontalLines',
      table: {
        headerRows: 1,
        widths: ['*', '*', '*', '*'],
        body: [['Nature des travaux', 'Montant (en euros)']],
      },
    };

    if (
      this.travauxGaranties?.travaux.value &&
      Object.keys(this.travauxGaranties?.travaux.value).length > 0
    ) {
      for (const key in this.travauxGaranties?.travaux.value) {
        travaux.table.body.push([
          this.travauxGaranties?.travaux.value[key].nature,
          this.travauxGaranties?.travaux.value[key].montant,
        ]);
      }
    } else {
      travaux.table.body.push(['Aucun travaux', '']);
    }

    travauxGaranties.stack.push(
      travaux,
      ' ',
      ' ',
      'Majoration du loyer en cours de bail consécutive à des travaux d’amélioration entrepris par le bailleur :',
      ' '
    );

    const majorations = {
      layout: 'lightHorizontalLines',
      table: {
        headerRows: 1,
        widths: ['*', '*', '*', '*'],
        body: [
          [
            'Nature des travaux',
            "Modalités d'exécution",
            'Délai de réalisation',
            'Montant de la majoration du loyer (en euros)',
          ],
        ],
      },
    };

    if (
      this.travauxGaranties?.majorations.value &&
      Object.keys(this.travauxGaranties?.majorations.value).length > 0
    ) {
      for (const key in this.travauxGaranties?.majorations.value) {
        majorations.table.body.push([
          this.travauxGaranties?.majorations.value[key].nature,
          this.travauxGaranties?.majorations.value[key].modalites,
          this.travauxGaranties?.majorations.value[key].delaiRealisation,
          this.travauxGaranties?.majorations.value[key].montant,
        ]);
      }
    } else {
      majorations.table.body.push(['Aucune majoration', '', '', '']);
    }

    travauxGaranties.stack.push(majorations);

    return travauxGaranties;
  }

  /**
   * récupération des garanties pdf
   */
  get garantiesPdf(): any {
    return {
      stack: [
        {
          text: 'VI. Garanties',
          style: 'partie',
        },
        'Montant du dépôt de garantie de l’exécution des obligations du locataire / Garantie autonome [inférieur ou égal à un mois de loyers hors charges] : ' +
          this.travauxGaranties?.montantGaranties.value +
          '€',
      ],
    };
  }
}
