import { Component, OnInit } from '@angular/core';
import { NgMeta } from 'ngmeta';

import { Constants } from '../../constants';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
})
export class NotFoundComponent implements OnInit {
  /**
   * Constructeur du composant
   */
  constructor(public constants: Constants, private ngMeta: NgMeta) {}

  /**
   * Initialisation du composant
   */
  ngOnInit(): void {
    this.ngMeta.setAll({
      title: this.constants.SEO_SITE_TITLE + 'Accueil',
      description: this.constants.SEO_SITE_DESCRIPTION,
      canonical: this.constants.CURRENT_URL,
      image: this.constants.IMAGE_URL + 'logo.png',
    });
  }
}
