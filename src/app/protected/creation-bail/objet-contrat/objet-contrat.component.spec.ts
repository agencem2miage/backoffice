import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjetContratComponent } from './objet-contrat.component';

describe('ObjetContratComponent', () => {
  let component: ObjetContratComponent;
  let fixture: ComponentFixture<ObjetContratComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObjetContratComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjetContratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
