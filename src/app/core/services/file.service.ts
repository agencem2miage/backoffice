import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Constants } from 'src/app/constants';
import { environment } from 'src/environments/environment';

/**
 * Définit le content-type du header
 */
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class FileService {
  /**
   * Point d'entrée de l'API pour la gestion des bails
   */
  fileEndpoint = environment.API_URL + this.constants.API_ENDPOINTS.FILE;
  //imagesEndpoint = environment.IMAGE_API;

  /**
   * Constructeur du service
   * @param http Service natif d'angular pour la gestion de requêtes HTTP
   * @param constants Constantes de l'application
   */
  constructor(private http: HttpClient, private constants: Constants) {}

  /**
   * récupération localisation fichier
   * @param bailId
   * @returns
   */
  getListeFileLocation(bailId: number): Observable<any> {
    return this.http
      .get(this.fileEndpoint + '/liste-file-location/' + bailId)
      .pipe(map((jsonResponse: any) => jsonResponse));
  }

  /**
   * mise a jour localisation d'un fichier
   * @param listFile
   * @param bailId
   * @returns
   */
  updateFileLocation(listFile: any, bailId: string): Observable<any> {
    return this.http.put(
      this.fileEndpoint + '/file-location',
      { listFile, bailId },
      httpOptions
    );
  }
}
