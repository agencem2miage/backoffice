import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoixBailComponent } from './choix-bail.component';

describe('ChoixBailComponent', () => {
  let component: ChoixBailComponent;
  let fixture: ComponentFixture<ChoixBailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoixBailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoixBailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
