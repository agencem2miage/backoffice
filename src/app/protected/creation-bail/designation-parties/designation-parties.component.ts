import { Civilites } from './../../../constants';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { CreationBailProvider } from './../../../core/providers/creation-bail.provider';

@Component({
  selector: 'app-designation-parties',
  templateUrl: './designation-parties.component.html',
  styleUrls: ['./designation-parties.component.scss'],
})
export class DesignationPartiesComponent implements OnInit {
  /**
   * Formulaire pour la désignation des parties
   */
  form!: FormGroup;

  /**
   * Nom du formulaire
   */
  formLabel = 'designationParties';

  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  civilites = Civilites;

  /**
   * Constructeur du composant
   * @param creationBailProvider Provider permettant d'enregistrer les étapes de création de bail
   * @param fb FormBuilder natif angular
   */
  constructor(
    private creationBailProvider: CreationBailProvider,
    private fb: FormBuilder,
    private router: Router
  ) {}

  /**
   * Initialisation du formulaire
   */
  ngOnInit(): void {
    this.initForm();
  }

  /**
   * Initialisation du formulaire
   */
  initForm(): void {
    const formulairePrerempli = this.creationBailProvider.formGroups.find(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli) {
      this.form = formulairePrerempli.formGroup;
    } else {
      this.form = this.fb.group({
        bailleurTitre: [
          {
            value: this.creationBailProvider.parties.proprietaire.civilite,
            disabled: true,
          },
          [Validators.required, Validators.maxLength(55)],
        ],
        bailleurNom: [
          {
            value: this.creationBailProvider.parties.proprietaire.nom,
            disabled: true,
          },
          [Validators.required, Validators.maxLength(55)],
        ],
        bailleurPrenom: [
          {
            value: this.creationBailProvider.parties.proprietaire.prenom,
            disabled: true,
          },
          [Validators.required, Validators.maxLength(55)],
        ],
        bailleurDateNaissance: [
          {
            value: this.creationBailProvider.parties.proprietaire.dateNaissance,
            disabled: true,
          },
          [Validators.required],
        ],
        bailleurLieuNaissance: [
          {
            value: this.creationBailProvider.parties.proprietaire
              .villeNaissance,
            disabled: true,
          },
          [Validators.required, Validators.maxLength(55)],
        ],
        bailleurAdresse: [
          {
            value:
              this.creationBailProvider.parties.proprietaire.adresse.numero +
              ' ' +
              this.creationBailProvider.parties.proprietaire.adresse.rue,
            disabled: true,
          },
          [Validators.required],
        ],
        bailleurCodePostal: [
          {
            value: this.creationBailProvider.parties.proprietaire.adresse
              .codePostal,
            disabled: true,
          },
          [Validators.required, Validators.pattern('[0-9]{5}')],
        ],
        bailleurVille: [
          {
            value: this.creationBailProvider.parties.proprietaire.adresse.ville,
            disabled: true,
          },
          [Validators.required, Validators.maxLength(55)],
        ],
        locataireTitre: [
          {
            value: this.creationBailProvider.parties.locataire.civilite,
            disabled: true,
          },
          [Validators.required, Validators.maxLength(55)],
        ],
        locataireNom: [
          {
            value: this.creationBailProvider.parties.locataire.nom,
            disabled: true,
          },
          [Validators.required, Validators.maxLength(55)],
        ],
        locatairePrenom: [
          {
            value: this.creationBailProvider.parties.locataire.prenom,
            disabled: true,
          },
          [Validators.required, Validators.maxLength(55)],
        ],
        locataireDateNaissance: [
          {
            value: this.creationBailProvider.parties.locataire.dateNaissance,
            disabled: true,
          },
          [Validators.required],
        ],
        locataireLieuNaissance: [
          {
            value: this.creationBailProvider.parties.locataire.villeNaissance,
            disabled: true,
          },
          [Validators.required, Validators.maxLength(55)],
        ],
        locataireAdresse: [
          {
            value:
              this.creationBailProvider.parties.locataire.adresse.numero +
              ' ' +
              this.creationBailProvider.parties.locataire.adresse.rue,
            disabled: true,
          },
          [Validators.required],
        ],
        locataireCodePostal: [
          {
            value: this.creationBailProvider.parties.locataire.adresse
              .codePostal,
            disabled: true,
          },
          [Validators.required, Validators.pattern('[0-9]{5}')],
        ],
        locataireVille: [
          {
            value: this.creationBailProvider.parties.locataire.adresse.ville,
            disabled: true,
          },
          [Validators.required, Validators.maxLength(55)],
        ],
      });
    }
  }

  /**
   * Soumission du formulaire pour passer à l'étape suivante
   */
  onSubmit(): void {
    this.submitted = true;

    const formulairePrerempli = this.creationBailProvider.formGroups.findIndex(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli !== -1) {
      this.creationBailProvider.formGroups.splice(formulairePrerempli, 1, {
        formGroupLabel: this.formLabel,
        formGroup: this.form,
      });
    } else {
      this.creationBailProvider.formGroups.push({
        formGroupLabel: this.formLabel,
        formGroup: this.form,
      });
    }

    this.router.navigate(['admin', 'creation-bail', 'objet-contrat']);
  }
}
