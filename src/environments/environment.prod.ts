/**
 * Contient toutes les variables d'environement pour se connecter au backoffice et à l'API en production
 */
export const environment = {
  production: true,
  API_URL: 'http://agencetourix.ddns.net/api',
  BACK_URL: 'http://agencetourix.ddns.net/back',
  IMAGE_API: 'http://agencetourix.ddns.net/images/',
  FILE_LOCATION_API:
    'http://agencetourix.ddns.net/document-location/dossier-numero-',
};
