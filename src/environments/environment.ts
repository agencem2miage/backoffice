/**
 * Contient toutes les variables d'environement pour se connecter au backoffice et à l'API en mode développeur
 */
export const environment = {
  production: false,
  API_URL: 'http://localhost:8080/api',
  BACK_URL: 'http://localhost:4201',
  IMAGE_API: 'http://localhost:8080/images/',
  FILE_LOCATION_API: 'http://localhost:8080/document-location/dossier-numero-',
};
