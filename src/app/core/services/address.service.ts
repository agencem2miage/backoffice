import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Constants } from 'src/app/constants';
import { Observable } from 'rxjs';
import { Adresse } from 'src/app/shared/models/adresse.model';
import { map } from 'rxjs/operators';

/**
 * définit le content type d'un header
 */
const httpOption = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})

/**
 * Classe pour les services de l'addresse
 */
export class AddressService {
  /**
   * Adresse API pour accéder aux adresses
   */
  addressEndpoint = environment.BACK_URL + this.constants.API_ENDPOINTS.ADRESSE; // A CHANGER

  /**
   * Import des services nécessaires
   * @param http Service natif d'angular pour la création de requêtes HTTP
   * @param constants Fichier qui contient les constantes du projet
   */
  constructor(private http: HttpClient, private constants: Constants) {}

  /**
   * Récupère l'identifiant de la dernière adresse créée
   * @returns un identifiant
   */
  getLastIdCreated(): Observable<any> {
    return this.http
      .get(this.addressEndpoint + '/lastId/')
      .pipe(
        map(
          (jsonResponse: any) =>
            jsonResponse.map((jsonUser: any) => Adresse.fromJson(jsonUser))[0]
        )
      );
  }

  /**
   * Récupère une adresse auprès de l'API Agence
   * @param addressId L'identifiant de l'utilisateur
   * @returns l'adresse récupéré
   */
  getAddress(addressId: number): Observable<Adresse> {
    return this.http
      .get(this.addressEndpoint + '/' + addressId)
      .pipe(
        map(
          (jsonResponse: any) =>
            jsonResponse.map((jsonUser: any) => Adresse.fromJson(jsonUser))[0]
        )
      );
  }

  /**
   * Envoie les données d'une nouvelle adresse à ajouter en base
   * @param newAddress Objet Adresse à rajouter dans la base
   * @returns Retour Http
   */
  addAddress(newAddress: Adresse): Observable<any> {
    return this.http.post(this.addressEndpoint, { newAddress }, httpOption);
  }
}
