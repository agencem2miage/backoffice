import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Civilites, Roles } from 'src/app/constants';
import { AuthentificationService } from 'src/app/core/services/authentification.service';
import { Adresse } from 'src/app/shared/models/adresse.model';
import { Modal, ModalType } from 'src/app/shared/models/modal.model';
import { Role } from 'src/app/shared/models/role.model';
import { Utilisateur } from 'src/app/shared/models/utilisateur.model';
import { NotificationsService } from 'src/app/core/services/notifications.service';
import {
  NotificationBackground,
  NotificationIcon,
  Notification,
} from 'src/app/shared/models/notification.model';
import { ModalService } from 'src/app/core/services/modal.service';

@Component({
  selector: 'app-creation-compte',
  templateUrl: './creation-compte.component.html',
  styleUrls: ['./creation-compte.component.scss'],
})
export class CreationCompteComponent implements OnInit {
  /**
   * Formulaire pour la création de compte
   */
  form!: FormGroup;

  /**
   * Nom du formulaire, ici 'createAccount'
   */
  formLabel = 'createAccount';

  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  /**
   * Role utilisateur
   */
  roles = Roles;

  /**
   * civilite utilisateur
   */
  civilites = Civilites;

  /**
   * Constructeur du composant
   * @param fb FormBuilder natif angular
   * @param router Router pour la submission du formulaire
   */
  constructor(
    private fb: FormBuilder,
    private authentificationService: AuthentificationService,
    private modalService: ModalService,
    private notificationsService: NotificationsService
  ) {}

  /**
   * Initialisation du formulaire
   */
  ngOnInit(): void {
    this.initForm();
  }

  /**
   * Initialisation du formulaire
   */
  initForm(): void {
    this.form = this.fb.group({
      creationType: ['', [Validators.required, Validators.maxLength(55)]],
      civilite: ['', [Validators.required, Validators.maxLength(55)]],
      firstName: ['', [Validators.required, Validators.maxLength(55)]],
      lastName: ['', [Validators.required, Validators.maxLength(55)]],
      birthDate: ['', [Validators.required]],
      birthPlace: ['', [Validators.required, Validators.maxLength(110)]],
      mail: [
        '',
        [
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],
      phone: ['', [Validators.required]],
      numberAdress: ['', [Validators.required]],
      street: ['', [Validators.required, Validators.maxLength(110)]],
      adressComplement: [''],
      city: ['', [Validators.required, Validators.maxLength(55)]],
      postalCode: ['', [Validators.required, Validators.pattern('[0-9]{5}')]],
      country: ['', [Validators.required, Validators.maxLength(100)]],
    });
  }

  /**
   * méthode de soumission du formulaire
   */
  onSubmit(): void {
    this.submitted = true;

    const notification = new Notification({
      message: 'Compte créé avec succés !',
      background: NotificationBackground.GREEN,
      icon: NotificationIcon.CHECK,
    });

    if (this.form.valid) {
      const confirmationModal = new Modal({
        title: 'Confirmation',
        text: 'Êtes-vous sûr de vouloir créer le compte ?',
        type: ModalType.CONFIRMATION,
      });

      confirmationModal.confirm = () => {
        const user: Utilisateur = new Utilisateur({
          id: -1,
          role: new Role({
            id: this.form.controls.creationType.value,
            label: '',
          }),
          civilite: this.form.controls.civilite.value,
          nom: this.form.controls.firstName.value,
          prenom: this.form.controls.lastName.value,
          adresse: new Adresse({
            id: -1,
            numero: this.form.controls.numberAdress.value,
            rue: this.form.controls.street.value,
            complement: this.form.controls.adressComplement.value,
            codePostal: this.form.controls.postalCode.value,
            ville: this.form.controls.city.value,
            pays: this.form.controls.country.value,
          }),
          mail: this.form.controls.mail.value,
          dateNaissance: this.form.controls.birthDate.value,
          villeNaissance: this.form.controls.birthPlace.value,
          numeroTelephone: this.form.controls.phone.value,
          motDePasse: '',
        });
        if (this.form.valid) {
          this.authentificationService.register(user, user.adresse).subscribe();
          this.notificationsService.genericNotification(notification);
        }
        this.initForm();
      };
      this.modalService.confirmationModal(confirmationModal);
    }
  }
}
