import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CreationBailProvider } from 'src/app/core/providers/creation-bail.provider';

@Component({
  selector: 'app-travaux-garanties',
  templateUrl: './travaux-garanties.component.html',
  styleUrls: ['./travaux-garanties.component.scss'],
})
export class TravauxGarantiesComponent implements OnInit {
  /**
   * Formulaire pour la désignation des parties
   */
  form!: FormGroup;

  /**
   * Nom du formulaire
   */
  formLabel = 'travauxGaranties';

  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  /**
   * Id du formulaire
   */
  formGroupsIds: {
    travaux: number;
    majorations: number;
  } = {
    travaux: 0,
    majorations: 0,
  };

  /**
   * Constructeur du composant
   * @param creationBailProvider Provider permettant d'enregistrer les étapes de création de bail
   * @param fb FormBuilder natif angular
   */
  constructor(
    private creationBailProvider: CreationBailProvider,
    private fb: FormBuilder,
    private router: Router
  ) {}

  /**
   * Initialisation du formulaire
   */
  ngOnInit(): void {
    this.initForm();
  }

  /**
   * Initialisation du formulaire
   */
  initForm(): void {
    const formulairePrerempli = this.creationBailProvider.formGroups.find(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli) {
      this.form = formulairePrerempli.formGroup;
    } else {
      this.form = this.fb.group({
        travaux: this.fb.group({}),
        majorations: this.fb.group({}),
        montantGaranties: [
          '',
          [Validators.required, Validators.pattern('^\\d+(\\.\\d{1,2})?$')],
        ],
      });

      this.creationBailProvider.bien.travaux.forEach((travaux) => {
        this.onAddTravaux(travaux.nature, travaux.montant);
      });
    }
  }

  /**
   * Soumission du formulaire pour passer à l'étape suivante
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.form.valid) {
      const formulairePrerempli = this.creationBailProvider.formGroups.findIndex(
        (form) => form.formGroupLabel === this.formLabel
      );

      if (formulairePrerempli !== -1) {
        this.creationBailProvider.formGroups.splice(formulairePrerempli, 1, {
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      } else {
        this.creationBailProvider.formGroups.push({
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      }

      this.router.navigate(['admin', 'creation-bail', 'signature']);
    }
  }

  /**
   * récupérer la liste des travaux
   */
  get travaux(): FormGroup {
    return this.form.controls.travaux as FormGroup;
  }

  /**
   * récupérer les majorations
   */
  get majorations(): FormGroup {
    return this.form.controls.majorations as FormGroup;
  }

  /**
   * Ajouter les travaux
   * @param nature
   * @param montant
   */
  onAddTravaux(nature?: string, montant?: number): void {
    this.travaux.addControl(
      'travaux' + this.formGroupsIds.travaux,
      this.fb.group({
        nature: [
          { value: nature || '', disabled: true },
          [Validators.required],
        ],
        montant: [
          { value: montant || 0, disabled: true },
          [Validators.required, Validators.pattern('^\\d+(\\.\\d{1,2})?$')],
        ],
      })
    );

    this.formGroupsIds.travaux++;
  }

  /**
   * Ajouter une majoration
   */
  onAddMajoration(): void {
    this.majorations.addControl(
      'majoration' + this.formGroupsIds.majorations,
      this.fb.group({
        nature: ['', [Validators.required]],
        modalites: ['', [Validators.required]],
        delaiRealisation: ['', [Validators.required]],
        montant: [
          '',
          [Validators.required, Validators.pattern('^\\d+(\\.\\d{1,2})?$')],
        ],
      })
    );

    this.formGroupsIds.majorations++;
  }

  /**
   * Supprimer les travaux
   * @param formGroupLabel
   */
  onDeleteTravaux(formGroupLabel: string): void {
    this.travaux.removeControl(formGroupLabel);
  }

  /**
   * Supprimer majoration
   * @param formGroupLabel
   */
  onDeleteMajoration(formGroupLabel: string): void {
    this.majorations.removeControl(formGroupLabel);
  }
}
