import { CreationEdlGuard } from './../core/guards/creation-edl.guard';
import { InventaireMeublesComponent } from './creation-etat-des-lieux/inventaire-meubles/inventaire-meubles.component';
import { PiecesComponent } from './creation-etat-des-lieux/pieces/pieces.component';
import { ChoixBailComponent } from './creation-etat-des-lieux/choix-bail/choix-bail.component';
import { CreationBailGuard } from './../core/guards/creation-bail.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DetailsBienComponent } from './details-bien/details-bien.component';
import { ListBienComponent } from './list-bien/list-bien.component';
import { ProfileComponent } from './profile/profile.component';
import { CreationCompteComponent } from './creation-compte/creation-compte.component';
import { SuppressionCompteComponent } from './suppression-compte/suppression-compte.component';
import { LoggedInAdminGuard } from '../core/guards/logged-in-admin.guard';
import { ChoixPartiesBienComponent } from './creation-bail/choix-parties-bien/choix-parties-bien.component';
import { ConditionsFinancieresComponent } from './creation-bail/conditions-financieres/conditions-financieres.component';
import { DesignationPartiesComponent } from './creation-bail/designation-parties/designation-parties.component';
import { ObjetContratComponent } from './creation-bail/objet-contrat/objet-contrat.component';
import { PriseEffetContratComponent } from './creation-bail/prise-effet-contrat/prise-effet-contrat.component';
import { SignatureComponent } from './creation-bail/signature/signature.component';
import { TravauxGarantiesComponent } from './creation-bail/travaux-garanties/travaux-garanties.component';
import { SignatureEdlComponent } from './creation-etat-des-lieux/signature-edl/signature-edl.component';
import { UploadComponent } from './upload/upload.component';
import { CreationBienComponent } from './creation-bien/creation-bien.component';

const routes: Routes = [
  {
    path: 'admin',
    redirectTo: 'admin/profil',
  },
  {
    path: 'admin',
    canActivate: [LoggedInAdminGuard],
    children: [
      {
        path: 'details-bien/:id',
        component: DetailsBienComponent,
      },
      {
        path: 'liste-biens',
        component: ListBienComponent,
      },
      {
        path: 'profil',
        component: ProfileComponent,
      },
      {
        path: 'upload',
        component: UploadComponent,
      },
      {
        path: 'creation-bien',
        component: CreationBienComponent,
      },
      {
        path: 'creation-compte',
        component: CreationCompteComponent,
      },
      {
        path: 'creation-bail',
        redirectTo: 'creation-bail/choix-parties-bien',
      },
      {
        path: 'creation-bail',
        canActivateChild: [CreationBailGuard],
        canDeactivate: [CreationBailGuard],
        children: [
          {
            path: 'choix-parties-bien',
            component: ChoixPartiesBienComponent,
          },
          {
            path: 'designation-parties',
            component: DesignationPartiesComponent,
          },
          {
            path: 'objet-contrat',
            component: ObjetContratComponent,
          },
          {
            path: 'prise-effet-contrat',
            component: PriseEffetContratComponent,
          },
          {
            path: 'conditions-financieres',
            component: ConditionsFinancieresComponent,
          },
          {
            path: 'travaux-garanties',
            component: TravauxGarantiesComponent,
          },
          {
            path: 'signature',
            component: SignatureComponent,
          },
        ],
      },
      {
        path: 'creation-etat-des-lieux',
        redirectTo: 'creation-etat-des-lieux/choix-bail',
      },
      {
        path: 'creation-etat-des-lieux',
        canActivateChild: [CreationEdlGuard],
        canDeactivate: [CreationEdlGuard],
        children: [
          {
            path: 'choix-bail',
            component: ChoixBailComponent,
          },
          {
            path: 'pieces',
            component: PiecesComponent,
          },
          {
            path: 'inventaire-meubles',
            component: InventaireMeublesComponent,
          },
          {
            path: 'signature',
            component: SignatureEdlComponent,
          },
        ],
      },
    ],
  },
  {
    path: 'suppression-compte',
    component: SuppressionCompteComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProtectedRoutingModule {}
