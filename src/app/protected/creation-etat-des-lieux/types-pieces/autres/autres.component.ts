import { TypesPiece } from './../../../../constants';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EtatsEquipements } from 'src/app/constants';
import { CreationEdlProvider } from 'src/app/core/providers/creation-edl.provider';

@Component({
  selector: 'app-autres',
  templateUrl: './autres.component.html',
  styleUrls: ['./autres.component.scss'],
})
/**
 * Composant d'autre piece
 */
export class AutresComponent implements OnInit {
  /**
   * formulaire autre piece
   */
  form!: FormGroup;
  /**
   * soumission du formulaire
   */
  submitted = false;
  /**
   * etat des equipements
   */
  etats = EtatsEquipements;
  /**
   * titre du formulaire
   */
  @Input() formLabel!: string;
  /**
   * type de piece
   */
  @Input() typePiece!: number;

  /**
   * Constructeur du component
   * @param fb formulaire
   * @param creationEdlProvider provider de création de salle de bain
   */
  constructor(
    private fb: FormBuilder,
    private creationEdlProvider: CreationEdlProvider
  ) {}

  /**
   * Méthode d'initialisation du component
   */
  ngOnInit(): void {
    const formulairePrerempli = this.creationEdlProvider.piecesFormGroups.find(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli) {
      this.form = formulairePrerempli.formGroup;
    } else {
      this.form = this.fb.group({
        rangementsPlacards: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        serruresPortesMenuiseries: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        fenetresVolets: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        murs: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        plafond: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        solPlinthes: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        robinetterie: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        radiateursTuyauterie: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        prisesInterrupteurs: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        eclairage: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
      });
    }
  }

  /**
   * Méthode de soumission du component
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.form.valid) {
      const formulairePrerempli = this.creationEdlProvider.piecesFormGroups.findIndex(
        (form) => form.formGroupLabel === this.formLabel
      );

      if (formulairePrerempli !== -1) {
        this.creationEdlProvider.piecesFormGroups.splice(
          formulairePrerempli,
          1,
          {
            formGroupLabel: this.formLabel,
            formGroup: this.form,
          }
        );
      } else {
        this.creationEdlProvider.piecesFormGroups.push({
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      }

      let elementStyle = document.querySelector(
        '.autrePieceItem' + this.formLabel
      ) as HTMLElement;
      elementStyle.style.display = 'none';

      elementStyle = document.querySelector(
        '.autrePiece' + this.formLabel
      ) as HTMLElement;

      elementStyle.style.backgroundColor = '#60b664';
    }
  }

  get typePieceString(): string {
    switch (this.typePiece) {
      case TypesPiece.CHAMBRE:
        return 'Chambre';
      case TypesPiece.SALLE_A_MANGER:
        return 'Salle à manger';
      case TypesPiece.SALON:
        return 'Salon';
      default:
        return 'Autre pièce';
    }
  }

  /**
   * méthode d'édition du formulaire
   */
  editForm(): void {
    let elementStyle = document.querySelector(
      '.autrePieceItem' + this.formLabel
    ) as HTMLElement;
    elementStyle.style.display = 'block';

    elementStyle = document.querySelector(
      '.autrePiece' + this.formLabel
    ) as HTMLElement;

    elementStyle.style.backgroundColor = '#fff';
  }
}
