import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InventaireMeublesComponent } from './inventaire-meubles.component';

describe('InventaireMeublesComponent', () => {
  let component: InventaireMeublesComponent;
  let fixture: ComponentFixture<InventaireMeublesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InventaireMeublesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InventaireMeublesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
