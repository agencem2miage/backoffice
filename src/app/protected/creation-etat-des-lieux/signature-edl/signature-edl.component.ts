import { Router } from '@angular/router';
import { CreationEdlProvider } from 'src/app/core/providers/creation-edl.provider';
import { Component, OnInit } from '@angular/core';
import { Constants } from 'src/app/constants';
import Utils from 'src/app/utils';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-signature',
  templateUrl: './signature-edl.component.html',
  styleUrls: ['./signature-edl.component.scss'],
})
/**
 * Classe de signature d'un état des lieux
 */
export class SignatureEdlComponent implements OnInit {
  /**
   * nom du locataire et propriétaire signataire
   */
  src: { bailleur: string; locataire: string } = {
    bailleur: '',
    locataire: '',
  };
  /**
   * boolean de signature du bail
   */
  clearMe: { bailleur: boolean; locataire: boolean } = {
    bailleur: false,
    locataire: false,
  };
  /**
   * boolean de signature du bail
   */
  signing: { bailleur: boolean; locataire: boolean } = {
    bailleur: true,
    locataire: true,
  };

  /**
   * paramètre de front du bail
   */
  docDefinition: any = {
    styles: {
      titre: {
        fontSize: 15,
        bold: true,
        alignment: 'center' as const,
      },
      sousTitre: {
        fontSize: 10,
        alignement: 'center' as const,
      },
      partie: {
        fontSize: 18,
        bold: true,
        margin: [0, 20, 0, 10] as [number, number, number, number],
      },
      sousPartie: {
        fontSize: 16,
        bold: true,
        margin: [0, 10, 0, 5] as [number, number, number, number],
      },
      paragraphe: {
        margin: [0, 10, 0, 0] as [number, number, number, number],
      },
    },
  };

  /**
   * constructeur de la page
   * @param constants
   * @param creationEdlProvider
   * @param router
   */
  constructor(
    private constants: Constants,
    private creationEdlProvider: CreationEdlProvider,
    private router: Router
  ) {}

  /**
   * méthode d'initialisation du component
   */
  async ngOnInit(): Promise<void> {
    this.docDefinition.content = [
      {
        columns: [
          {
            image: '',
            width: 150,
            height: 150,
            margin: 0,
          },
          [
            {
              text: 'ETAT DES LIEUX',
              style: 'titre',
            },
            {
              stack: ['(Conforme LOI ALUR)'],
              style: 'sousTitre',
            },
          ],
        ],
      },
      {
        pageBreak: 'before' as const,
        stack: [
          {
            text: 'Signature des parties',
            style: 'partie',
          },
          'L’état des lieux doit être effectué lors de la remise des clés. Le locataire peut cependant compléter l’état des lieux ultérieurement, à deux occasions :',
          '    - dans les 10 jours qui suivent l’établissement de l’état des lieux pour tout équipement concernant le logement',
          '    - pendant le premier mois de la période de chauffe concernant l’état des équipements de chauffage',
          'Il appartient au locataire de veiller à maintenir en l’état le logement qu’il occupe. A ce titre, il doit assurer l’entretien courant du logement et les menues réparations nécessaires.',
          'A défaut, et sauf s’il est prouvé que la dégradation est dûe à la vétusté du logement, à une malfaçon ou à un cas de force majeure, le bailleur peut retenir sur le dépôt de garantie',
          'les sommes correspondant aux réparations locatives qui n’ont pas été effectuées par le locataire, justificatifs à l’appui.',
        ],
      },
      {
        columns: [],
      },
    ];

    await Utils.imageUrlToBase64(this.constants.IMAGE_URL + 'logo.png').then(
      (base64Image) =>
        (this.docDefinition.content[0].columns[0].image =
          base64Image?.toString() || '')
    );
  }

  /**
   * Changement de la source
   * @param src
   * @param partie
   */
  srcChange(src: any, partie: string): void {
    partie === 'bailleur'
      ? (this.src.bailleur = src)
      : (this.src.locataire = src);
  }

  /**
   * suppression de la signature
   * @param partie
   */
  clear(partie: string): void {
    partie === 'bailleur'
      ? (this.clearMe.bailleur = !this.clearMe.bailleur)
      : (this.clearMe.locataire = !this.clearMe.locataire);
  }

  /**
   * sauvegarde du bail
   * @param partie
   */
  save(partie: string): void {
    const date = new Date();

    partie === 'bailleur'
      ? (this.signing.bailleur = false)
      : (this.signing.locataire = false);

    this.docDefinition.content[
      this.docDefinition.content.length - 1
    ].columns.push([
      {
        stack: [
          {
            text: 'Signature ' + partie,
            style: 'partie',
          },
          {
            text:
              'Le ' +
              date.getDate() +
              '/' +
              (date.getMonth() + 1) +
              '/' +
              date.getFullYear(),
          },
          {
            image:
              partie === 'bailleur' ? this.src.bailleur : this.src.locataire,
            width: 150,
            height: 150,
            margin: 0,
          },
        ],
      },
    ]);
  }

  /**
   * soumission du bail
   */
  async onSubmit(): Promise<void> {
    pdfMake.createPdf(this.docDefinition).getBase64((base64Pdf) => {});
  }

  /**
   * génération pdf du bail
   * @param action
   */
  genererBailPDF(action = 'download'): void {
    if (action === 'download') {
      pdfMake
        .createPdf(this.docDefinition)
        .download(
          '[' +
            this.creationEdlProvider.bail.id +
            '] [' +
            this.creationEdlProvider.bail.bien.proprietaire.nom +
            '] [' +
            this.creationEdlProvider.bail.locataire.nom +
            ']'
        );
    } else if (action === 'print') {
      pdfMake.createPdf(this.docDefinition).print();
    } else {
      pdfMake.createPdf(this.docDefinition).open();
    }
  }

  /**
   * précédent
   */
  previous(): void {
    if (this.creationEdlProvider.bail.bien.meuble) {
      this.router.navigate([
        'admin',
        'creation-etat-des-lieux',
        'inventaire-meubles',
      ]);
    } else {
      this.router.navigate(['admin', 'creation-etat-des-lieux', 'pieces']);
    }
  }
}
