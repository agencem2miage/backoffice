import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EtatsEquipements } from 'src/app/constants';
import { CreationEdlProvider } from 'src/app/core/providers/creation-edl.provider';

@Component({
  selector: 'app-toilettes',
  templateUrl: './toilettes.component.html',
  styleUrls: ['./toilettes.component.scss'],
})
/**
 * Classe de toilette
 */
export class ToilettesComponent implements OnInit {
  /**
   * formulaire du component
   */
  form!: FormGroup;
  /**
   * soumission du formulaire
   */
  submitted = false;
  /**
   * etat des équipements
   */
  etats = EtatsEquipements;
  /**
   * titre du formulaire
   */
  @Input() formLabel!: string;

  /**
   * Constructeur des toilettes
   * @param fb formulaire
   * @param creationEdlProvider provider
   */
  constructor(
    private fb: FormBuilder,
    private creationEdlProvider: CreationEdlProvider
  ) {}

  /**
   * Méthode d'initialisation du component
   */
  ngOnInit(): void {
    const formulairePrerempli = this.creationEdlProvider.piecesFormGroups.find(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli) {
      this.form = formulairePrerempli.formGroup;
    } else {
      this.form = this.fb.group({
        rangementsPlacards: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        serruresPortesMenuiseries: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        fenetresVolets: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        murs: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        plafond: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        solPlinthes: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        robinetterieEvierMiroir: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        radiateursTuyauterie: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        prisesInterrupteurs: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
        eclairage: this.fb.group({
          etat: ['1', Validators.required],
          observations: [''],
        }),
      });
    }
  }

  /**
   * Méthode de soumission du formulaire
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.form.valid) {
      const formulairePrerempli = this.creationEdlProvider.piecesFormGroups.findIndex(
        (form) => form.formGroupLabel === this.formLabel
      );

      if (formulairePrerempli !== -1) {
        this.creationEdlProvider.piecesFormGroups.splice(
          formulairePrerempli,
          1,
          {
            formGroupLabel: this.formLabel,
            formGroup: this.form,
          }
        );
      } else {
        this.creationEdlProvider.piecesFormGroups.push({
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      }

      let elementStyle = document.querySelector(
        '.toilettesItem' + this.formLabel
      ) as HTMLElement;
      elementStyle.style.display = 'none';

      elementStyle = document.querySelector(
        '.toilettes' + this.formLabel
      ) as HTMLElement;

      elementStyle.style.backgroundColor = '#60b664';
    }
  }

  /**
   * méthode d'édition du formulaire
   */
  editForm(): void {
    let elementStyle = document.querySelector(
      '.toilettesItem' + this.formLabel
    ) as HTMLElement;
    elementStyle.style.display = 'block';

    elementStyle = document.querySelector(
      '.toilettes' + this.formLabel
    ) as HTMLElement;

    elementStyle.style.backgroundColor = '#fff';
  }
}
