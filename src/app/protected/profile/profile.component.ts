import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';

import { Constants } from '../../constants';
import { UserService } from '../../core/services/user.service';
import { Utilisateur } from 'src/app/shared/models/utilisateur.model';
import { Modal, ModalType } from 'src/app/shared/models/modal.model';
import { ModalService } from '../../core/services/modal.service';
import { NotificationsService } from 'src/app/core/services/notifications.service';
import {
  NotificationBackground,
  NotificationIcon,
  Notification,
} from 'src/app/shared/models/notification.model';
import { Role } from 'src/app/shared/models/role.model';
import { Adresse } from 'src/app/shared/models/adresse.model';
import { TokenService } from 'src/app/core/services/token.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  constructor(
    private datePipe: DatePipe,
    public constants: Constants,
    private fb: FormBuilder,
    private userService: UserService,
    private cd: ChangeDetectorRef,
    private modalService: ModalService,
    private notificationsService: NotificationsService,
    private TokenService: TokenService
  ) {}

  /**
   * variable temp qui permet de récupérer le user
   */
  user!: Utilisateur;

  /**
   * variable contenant la subscribe de getUser
   */
  getUserSub!: Subscription;

  /**
   * variable contenant la subscribe de getUser
   */
  putUserSub!: Subscription;
  /**
   * Formulaire de connexion
   */
  profileForm!: FormGroup;

  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  /**
   * liste des noms des fichiers de bail
   */
  listFileNameBail: Array<string> = [];

  /**
   * liste des noms des fichiers d'assurance
   */
  listFileNameAssurance: Array<string> = [];

  /**
   * Init du composant
   */
  ngOnInit(): void {
    this.user = Utilisateur.fromJson(this.TokenService.getUser());
    this.initForm();
  }

  /**
   * Unsubscribe
   */
  ngOnDestroy(): void {
    this.putUserSub?.unsubscribe();
  }
  /**
   * Initialisation du formulaire
   */
  initForm(): void {
    this.profileForm = this.fb.group({
      role: [this.user.role.label],
      civilite: [this.user.civilite, Validators.required],
      prenom: [
        this.user.prenom,
        [Validators.required, Validators.maxLength(50)],
      ],
      nom: [this.user.nom, [Validators.required, Validators.maxLength(50)]],
      mail: [this.user.mail, [Validators.required, Validators.maxLength(50)]],
      numeroTelephone: [this.user.numeroTelephone, Validators.required],
      dateNaissance: [
        this.datePipe.transform(this.user.dateNaissance, 'yyyy-MM-dd'),
      ],
      villeNaissance: [this.user.villeNaissance, Validators.maxLength(50)],
      numero: [this.user.adresse.numero, Validators.maxLength(50)],
      rue: [this.user.adresse.rue, Validators.maxLength(50)],
      complement: [this.user.adresse.complement, Validators.maxLength(50)],
      codePostal: [
        this.user.adresse.codePostal,
        Validators.pattern('[0-9]{5}'),
      ],
      ville: [this.user.adresse.ville, Validators.maxLength(50)],
      pays: [this.user.adresse.pays, Validators.maxLength(50)],
      fileBail: [''],
      fileAssurance: [''],
    });
  }

  /**
   * Permet de retourner les controls du formulaire facilement
   */
  get f() {
    return this.profileForm.controls;
  }
  /**
   * méthode appelée lors de la soumission du formulaire.
   * Elle appelle l'API de connexion
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.profileForm.valid) {
      const confirmationModal = new Modal({
        title: 'Confirmation',
        text: 'Êtes-vous sûr de vouloir modifier votre profil ?',
        type: ModalType.CONFIRMATION,
      });

      confirmationModal.confirm = () => {
        const newUser = new Utilisateur({
          id: this.user.id,
          role: Role.fromJson({
            id: this.user.role.id,
            label: this.user.role.label,
          }),
          civilite: this.f.civilite.value,
          nom: this.f.nom.value,
          prenom: this.f.prenom.value,
          adresse: Adresse.fromJson({
            id: this.user.adresse.id,
            numero: this.f.numero.value,
            rue: this.f.rue.value,
            complement: this.f.complement.value,
            codePostal: this.f.codePostal.value,
            ville: this.f.ville.value,
            pays: this.f.pays.value,
          }),
          mail: this.f.mail.value,
          dateNaissance: this.f.dateNaissance.value,
          villeNaissance: this.f.villeNaissance.value,
          numeroTelephone: this.f.numeroTelephone.value,
          motDePasse: '',
        });

        this.putUserSub = this.userService
          .updateUser(newUser)
          .subscribe((jsonResponse) => {
            const notification = new Notification({
              message: 'Votre profil a bien été modifié.',
              background: NotificationBackground.GREEN,
              icon: NotificationIcon.CHECK,
            });
            this.notificationsService.genericNotification(notification);
            this.TokenService.saveUser(jsonResponse);
          });
      };
      this.modalService.confirmationModal(confirmationModal);
    }
  }
}
