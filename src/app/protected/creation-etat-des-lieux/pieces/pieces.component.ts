import { PiecesService } from './../../../core/services/pieces.service';
import { Subscription } from 'rxjs';
import { Piece } from './../../../shared/models/bien.model';
import { CreationEdlProvider } from './../../../core/providers/creation-edl.provider';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { TypesPiece } from 'src/app/constants';

@Component({
  selector: 'app-pieces',
  templateUrl: './pieces.component.html',
  styleUrls: ['./pieces.component.scss'],
})
/**
 * Component des pieces d'un bien
 */
export class PiecesComponent implements OnInit {
  /**
   * subscription d'une piece
   */
  piecesSub!: Subscription;

  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  /**
   * boolean d'erreur
   */
  error = false;

  /**
   * type de piece
   */
  typesPieces = TypesPiece;

  /**
   * liste de pieces
   */
  pieces: Piece[] = [];

  /**
   * constructeur de piece
   * @param router
   * @param creationEdlProvider
   * @param piecesService
   */
  constructor(
    private router: Router,
    private creationEdlProvider: CreationEdlProvider,
    private piecesService: PiecesService
  ) {}

  /**
   * initialisation du component
   */
  ngOnInit(): void {
    this.piecesSub = this.piecesService
      .getPieces(this.creationEdlProvider.bail.bien.id)
      .subscribe((pieces) => {
        this.creationEdlProvider.bail.bien.pieces = this.pieces = pieces.sort(
          (a, b) => a.typePiece - b.typePiece
        );
      });
  }

  /**
   * méthode de soumission du formulaire
   */
  onSubmit(): void {
    this.submitted = true;

    if (
      this.creationEdlProvider.piecesFormGroups.length === this.pieces.length
    ) {
      this.creationEdlProvider.bail.bien.meuble
        ? this.router.navigate([
            'admin',
            'creation-etat-des-lieux',
            'inventaire-meubles',
          ])
        : this.router.navigate([
            'admin',
            'creation-etat-des-lieux',
            'signature',
          ]);
    } else {
      this.error = true;
    }
  }
}
