import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EtatsEquipements } from 'src/app/constants';
import { CreationEdlProvider } from 'src/app/core/providers/creation-edl.provider';

@Component({
  selector: 'app-electromenager',
  templateUrl: './electromenager.component.html',
  styleUrls: ['./electromenager.component.scss'],
})

/**
 * Classe d'électroménager
 */
export class ElectromenagerComponent implements OnInit {
  /**
   * formulaire
   */
  form!: FormGroup;

  /**
   * soumission du formulaire
   */
  submitted = false;

  /**
   * état de l'équipement
   */
  etats = EtatsEquipements;

  /**
   * titre du formulaire
   */
  formLabel = 'electromenager';

  /**
   * constructeur du component
   * @param fb formulaire
   * @param creationEdlProvider provider de creation
   */
  constructor(
    private fb: FormBuilder,
    private creationEdlProvider: CreationEdlProvider
  ) {}

  /**
   * méthode d'initialisation du component
   */
  ngOnInit(): void {
    const formulairePrerempli = this.creationEdlProvider.inventaireFormGroups.find(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli) {
      this.form = formulairePrerempli.formGroup;
    } else {
      this.form = this.fb.group({
        refrigerateur: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        congelateur: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        four: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        cuisiniere: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        plaqueCuisson: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        fourMicroOndes: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        grillePain: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        bouilloire: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        cafetiere: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        laveVaisselle: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        laveLinge: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        secheLinge: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        videoProjecteur: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        television: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        lecteurDvd: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        chaineHifi: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
      });
    }
  }

  /**
   * méthode de soumission du formulaire
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.form.valid) {
      const formulairePrerempli = this.creationEdlProvider.inventaireFormGroups.findIndex(
        (form) => form.formGroupLabel === this.formLabel
      );

      if (formulairePrerempli !== -1) {
        this.creationEdlProvider.inventaireFormGroups.splice(
          formulairePrerempli,
          1,
          {
            formGroupLabel: this.formLabel,
            formGroup: this.form,
          }
        );
      } else {
        this.creationEdlProvider.inventaireFormGroups.push({
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      }

      let elementStyle = document.querySelector(
        '.electromenagerItem'
      ) as HTMLElement;
      elementStyle.style.display = 'none';

      elementStyle = document.querySelector('.electromenager') as HTMLElement;

      elementStyle.style.backgroundColor = '#60b664';
    }
  }

  /**
   * méthode d'édition du formulaire
   */
  editForm(): void {
    let elementStyle = document.querySelector(
      '.electromenagerItem'
    ) as HTMLElement;
    elementStyle.style.display = 'block';

    elementStyle = document.querySelector('.electromenager') as HTMLElement;

    elementStyle.style.backgroundColor = '#fff';
  }
}
