import { Utilisateur } from './../../shared/models/utilisateur.model';
import { UserService } from './../../core/services/user.service';
import {
  RegimesJuridiques,
  TypeMeuble,
  TypesBien,
  TypesContrat,
} from './../../constants';
import { ModalService } from './../../core/services/modal.service';
import { TypeBien } from './../../shared/models/typeBien.model';
import { AdminFileService } from './../../core/services/adminFileService';
import { BailService } from './../../core/services/bail.service';
import { ImagesService } from './../../core/services/images.service';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NgMeta } from 'ngmeta';
import { Bien } from 'src/app/shared/models/bien.model';
import { Constants } from '../../constants';
import { BienService } from 'src/app/core/services/bien.service';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Bail } from 'src/app/shared/models/bail.model';
import { saveAs } from 'file-saver';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Modal, ModalType } from 'src/app/shared/models/modal.model';
import { NotificationsService } from 'src/app/core/services/notifications.service';
import {
  Notification,
  NotificationBackground,
  NotificationIcon,
} from 'src/app/shared/models/notification.model';
import { TypeContrat } from 'src/app/shared/models/typeContrat.model';
import { legalRegime } from 'src/app/shared/models/legalRegime.model';
import { PiecesService } from 'src/app/core/services/pieces.service';

@Component({
  selector: 'app-details-bien',
  templateUrl: './details-bien.component.html',
  styleUrls: ['./details-bien.component.scss'],
})
export class DetailsBienComponent implements OnInit {
  /**
   * Tableau d'images qui seront affichées dans le slider
   */
  imageObject: Array<any> = [];

  /**
   * Image de l'accueil a afficher
   */
  imageAccueil = '';

  /**
   * Formulaire
   */
  formEditBien!: FormGroup;

  /**
   * Souscription au service de récupération du bien
   */
  getBienSub: Subscription = new Subscription();

  /**
   * Souscription au service de récupération de la liste des propriétaires
   */
  getListeProprietaireSub: Subscription = new Subscription();

  /**
   * Souscription au service de récupération des images
   */
  getImagesSub: Subscription = new Subscription();

  /**
   * Permet d'attendre que les images soit chargée pour l'afficher
   */
  imagesLoaded!: Promise<boolean>;

  /**
   * Permet d'attendre que le bail soit chargé pour l'afficher
   */
  bailLoaded!: Promise<boolean>;
  bienLoaded!: Promise<boolean>;
  propLoaded!: Promise<boolean>;
  pieceLoaded!: Promise<boolean>;
  filterLoaded!: Promise<boolean>;

  /**
   * Permet d'attendre que la liste des proprietaires soit chargé pour l'afficher
   */
  listeProprietaireLoaded!: Promise<boolean>;

  /**
   * Le Bien dont le détail est affichée
   */
  bien!: Bien;

  /**
   * Le bail dont les infos sont affichés
   */
  bail!: Bail;

  /**
   * Souscription au service de récupération de l'actualité
   */
  putEditSub!: Subscription;

  /**
   * liste des types bien
   */
  listeTypeBien!: TypeBien[];

  /**
   * liste des type contrat
   */
  listeTypeContrat!: TypeContrat[];

  /**
   * liste meuble
   */
  listeMeuble!: any[];

  /**
   * liste Regime Juridique
   */
  listeRegimeJuridique!: legalRegime[];

  /**
   * liste proprietaire
   */
  listeProprietaire!: Utilisateur[];

  listTypePiece!: any;
  listImgBailName: Array<string> = [];
  listPiece: Array<any> = [];

  listTypeContrat: TypeContrat[] = [];
  listTypeBien: TypeBien[] = [];
  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  /**
   * Constructeur du composant
   */
  constructor(
    public constants: Constants,
    private route: ActivatedRoute,
    private ngMeta: NgMeta,
    private bienService: BienService,
    private imageService: ImagesService,
    private bailService: BailService,
    private UserService: UserService,
    private adminFileService: AdminFileService,
    private fb: FormBuilder,
    private modalService: ModalService,
    private notificationsService: NotificationsService,
    private router: Router,
    private cd: ChangeDetectorRef,
    private piecesService: PiecesService
  ) { }

  /**
   * Initialisation du composant
   */
  ngOnInit(): void {
    this.ngMeta.setAll({
      title: this.constants.SEO_SITE_TITLE + 'Détails du bien',
      description: this.constants.SEO_SITE_DESCRIPTION,
      canonical: this.constants.CURRENT_URL,
      image: this.constants.IMAGE_URL + 'logo.png',
    });
    forkJoin([
      this.bienService.getListeFilterBienAdmin(),
      this.UserService.getProprietaires(),
      this.piecesService.getTypePiece(),
      this.bienService.getBienAgent(
        Number(this.route.snapshot.paramMap.get('id'))
      ),
      this.imageService.getImages(
        Number(this.route.snapshot.paramMap.get('id'))
      ),
      this.bailService.getBailBien(
        Number(this.route.snapshot.paramMap.get('id'))
      ),
    ]).subscribe(([filter, prop, piece, bien, img, bail]) => {
      this.listTypeContrat = filter[0].listTypeContrat;
      this.listTypeBien = filter[0].listTypeBien;
      this.listeProprietaire = prop;
      this.listTypePiece = piece;
      this.bien = bien;
      this.bien.image = img;
      this.chargerImageSlider();
      this.bail = bail;

      if (this.bail.id !== -1) {
        this.UserService.getUser(this.bail.locataire.id).subscribe((user) => {
          this.bail.locataire = user;
          this.initForm();
          this.bailLoaded = Promise.resolve(true);
        });
      } else {
        this.initForm();
        this.bailLoaded = Promise.resolve(true);
      }
    });
  }

  removePiece(idSelect: any) {
    const RemoveElementFromObjectArray = (key: number) => {
      this.listPiece.forEach((value, index) => {
        if (value.id == key) this.listPiece.splice(index, 1);
      });
    };
    RemoveElementFromObjectArray(idSelect);
    const notification = new Notification({
      message: 'La pièce a été supprimée.',
      background: NotificationBackground.GREEN,
      icon: NotificationIcon.CHECK,
    });
    this.notificationsService.genericNotification(notification);
  }

  addPiece(idSelect: any): void {
    const typePiece = this.listTypePiece.filter(
      (obj: any) => obj.id === idSelect
    );
    this.listPiece.push({
      id: typePiece[0].id,
      label: typePiece[0].label,
    });
    const notification = new Notification({
      message: 'Nouvelle pièce ajoutée.',
      background: NotificationBackground.GREEN,
      icon: NotificationIcon.CHECK,
    });
    this.notificationsService.genericNotification(notification);
  }

  onFileChange(event: any): void {
    if (event.target.files && event.target.files.length) {
      const finalResult: Array<string | ArrayBuffer | null> = [];
      const file = event.target.files;
      const countFile = file.length;

      for (let i = 0; i < countFile; i++) {
        this.listImgBailName.push(file.item(i).name);
        if (file.item(i).size <= 5000000) {
          const reader = new FileReader();
          reader.readAsDataURL(file.item(i));
          reader.onload = () => {
            finalResult.push(reader.result);
          };
        } else {
          const notification = new Notification({
            message: "Votre image n'est pas valide.",
            background: NotificationBackground.RED,
            icon: NotificationIcon.CROSS,
          });
          this.notificationsService.genericNotification(notification);
        }
      }
      this.formEditBien.patchValue({
        photoBien: finalResult,
      });
      this.cd.markForCheck();
    }
  }

  onDeleteFile(): void {
    this.listImgBailName = [];
    this.formEditBien.patchValue({
      photoBien: '',
    });
  }

  /**
   * Initialisation du formulaire
   */
  initForm(): void {
    this.formEditBien = this.fb.group({
      proprietaire: [
        this.bien.proprietaire.id,
        [Validators.required, Validators.maxLength(55)], // TODO A changer : la gestion du propriétaire n'est pas au point
      ],
      typeBien: [this.bien.typeBien.id, [Validators.required]],
      ville: [this.bien.adresse.ville, [Validators.required]],
      numeroRue: [this.bien.adresse.numero, [Validators.required]],
      typeContrat: [this.bien.typeContrat.id, [Validators.required]],
      nomRue: [
        this.bien.adresse.rue,
        [Validators.required, Validators.maxLength(55)],
      ],
      complementAdresse: [this.bien.adresse.complement, [Validators.required]],
      codePostal: [
        this.bien.adresse.codePostal,
        [Validators.required, Validators.pattern('[0-9]{5}')],
      ],
      surfaceHabitable: [this.bien.surfaceHabitable, [Validators.required]],
      surfaceTerrain: [this.bien.surfaceTerrain, [Validators.required]],
      surfaceBalcon: [this.bien.surfaceBalcon, [Validators.required]],
      prixVente: [this.bien.prix, [Validators.required]],
      anneeConstruction: [this.bien.anneeConstruction, [Validators.required]],
      equipementsDivers: [this.bien.equipementMaison, [Validators.required]],
      descriptionBien: [this.bien.description, [Validators.required]],
      detailsPieces: [this.bien.detailPieces, [Validators.required]],
      meuble: [this.bien.meuble, [Validators.required]], // TODO
      classeEnergie: [this.bien.classeEnergie, [Validators.required]],
      regimeJuridique: [this.bien.regimeJuridique.id, [Validators.required]],
      ges: [this.bien.ges, [Validators.required]],
      garageVelo: [this.bien.garageVelo, [Validators.required]],
      garageVoiture: [this.bien.garageVoiture, [Validators.required]],
      espacesVert: [this.bien.espacesVerts, [Validators.required]],
      localPoubelle: [this.bien.localPoubelles, [Validators.required]],
      ascenseur: [this.bien.ascenseur, [Validators.required]],
      aireJeux: [this.bien.aireJeux, [Validators.required]],
      gardiennage: [this.bien.gardiennage, [Validators.required]],
      laverie: [this.bien.laverie, [Validators.required]],
      listePieces: [''],
      photoBien: [''], //TODO
    });
  }
  /**
   * méthode permettant de savoir si le bien est un appartement ou non, pour afficher des caractéristiques propres aux appartements
   * @returns boolean
   */
  isAppartement(): boolean {
    if (this.bien.typeBien.label == 'Appartement') {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Méthode permettant de savoir si le bien a un terrain ou non, pour afficher la présence d'un terrain
   * @returns boolean
   */
  isTerrain(): boolean {
    if (this.bien.surfaceTerrain != 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Méthode permettant de savoir si le bien à un balcon, pour afficher sa présence ou non
   * @returns boolean
   */
  isBalcon(): boolean {
    if (this.bien.surfaceBalcon != 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * méthode permettant de récupérer les images du bien, et de les mettre dans le tableau d'image pour le slider
   */
  chargerImageSlider(): void {
    const imageSlider = {
      image: '',
      thumbImage: '',
      order: 0,
    };
    let compteur = 0;

    //récupération des images de l'objet bien
    this.bien.image.forEach((image_bdd: any) => {
      const newImageSlider = Object.create(imageSlider);
      newImageSlider.image = environment.IMAGE_API + '/' + image_bdd.label;
      newImageSlider.thumbImage = environment.IMAGE_API + '/' + image_bdd.label;
      newImageSlider.order = compteur;
      //on met l'objet dans le tableau d'image du slider
      this.imageObject.push(newImageSlider);
      compteur += 1;
    });
    if (compteur == 0) {
      this.imageAccueil = this.constants.IMAGE_URL + 'erreur_image.PNG';
    }
  }

  downloadBail(): void {
    this.adminFileService.getFile(this.bail.pdf).subscribe((file) => {
      const blob = new Blob([file], { type: 'application/pdf' });
      saveAs(blob, 'fichier.pdf');
    });
  }

  /**
   * existe il une assurance locataire
   * @returns
   */
  isAssuranceLocataire(): boolean {
    if (this.bien.assuranceLocataire == '') {
      return false;
    }
    return true;
  }

  /**
   * téléchargement assurance locataire
   */
  downloadAssuranceLocataire(): void {
    this.adminFileService
      .getFile(this.bien.assuranceLocataire)
      .subscribe((file) => {
        const blob = new Blob([file], { type: 'application/pdf' });
        saveAs(blob, 'fichier.pdf');
      });
  }

  /**
   * existe il une assurance proprietaire
   * @returns
   */
  isAssuranceProprietaire(): boolean {
    if (this.bail.assuranceProprietaire == '') {
      return false;
    }
    return true;
  }

  /**
   * téléchargement de l'assurance proprietaire
   */
  downloadAssuranceProprietaire(): void {
    this.adminFileService
      .getFile(this.bail.assuranceProprietaire)
      .subscribe((file) => {
        const blob = new Blob([file], { type: 'application/pdf' });
        saveAs(blob, 'fichier.pdf');
      });
  }

  onSubmit(): void {
    const newBien = this.bien;
    for (let j = 0; j < this.listeProprietaire.length; j++) {
      if (this.listeProprietaire[j].id == this.f.proprietaire.value) {
        newBien.proprietaire = this.listeProprietaire[j];
      }
    }
    newBien.typeBien = new TypeBien({
      id: this.f.typeBien.value,
      label: '',
    });
    newBien.adresse.ville = this.f.ville.value;
    newBien.adresse.numero = this.f.numeroRue.value;
    newBien.adresse.rue = this.f.nomRue.value;
    newBien.adresse.complement = this.f.complementAdresse.value;
    newBien.adresse.codePostal = this.f.codePostal.value;
    newBien.typeContrat = new TypeContrat({
      id: this.f.typeContrat.value,
      label: '',
    });
    newBien.surfaceHabitable = this.f.surfaceHabitable.value;
    newBien.prix = this.f.prixVente.value;
    newBien.anneeConstruction = this.f.anneeConstruction.value;
    //newBien.photoBien TODO : voir comment gérer les photos des biens avec COCO
    newBien.equipementMaison = this.f.equipementsDivers.value;
    newBien.description = this.f.descriptionBien.value;
    newBien.detailPieces = this.f.detailsPieces.value;
    newBien.meuble = this.f.meuble.value;
    newBien.classeEnergie = this.f.classeEnergie.value;
    newBien.regimeJuridique = new legalRegime({
      id: this.f.regimeJuridique.value,
      label: '',
    }); //TODO : a tester
    newBien.ges = this.f.ges.value;
    //newBien.pieces = this.f.salon.value TODO : voir comment gérer ça avec les pièces
    newBien.garageVelo = this.f.garageVelo.value;
    newBien.garageVoiture = this.f.garageVoiture.value;
    newBien.espacesVerts = this.f.espacesVert.value;
    newBien.localPoubelles = this.f.localPoubelle.value;
    newBien.ascenseur = this.f.ascenseur.value;

    const confirmationModal = new Modal({
      title: 'Confirmation',
      text: "Êtes-vous sûr de vouloir modifier les informations de ce bien ?",
      type: ModalType.CONFIRMATION,
    });

    confirmationModal.confirm = () => {
      this.putEditSub = this.bienService.editBien(newBien).subscribe(() => {
        const notification = new Notification({
          message: 'Le bien a bien été modifiée.',
          background: NotificationBackground.GREEN,
          icon: NotificationIcon.CHECK,
        });

        this.notificationsService.genericNotification(notification);
      });
    };
    this.modalService.confirmationModal(confirmationModal);
  }

  /**
   * Permet de retourner les controls du formulaire facilement
   */
  get f() {
    return this.formEditBien.controls;
  }
}
