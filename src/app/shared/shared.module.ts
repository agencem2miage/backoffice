import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SignatureDirective } from './directives/signature.directive';
import { TextareaHeightDirective } from './directives/textarea-height.directive';
import { EnumToObjectPipe } from './pipes/enum-to-object.pipe';

@NgModule({
  declarations: [TextareaHeightDirective, EnumToObjectPipe, SignatureDirective],
  imports: [CommonModule, RouterModule, FormsModule, ReactiveFormsModule],
  exports: [TextareaHeightDirective, EnumToObjectPipe, SignatureDirective],
})
export class SharedModule { }
