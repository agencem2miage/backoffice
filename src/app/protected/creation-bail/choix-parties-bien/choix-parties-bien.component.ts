import { BienService } from 'src/app/core/services/bien.service';
import { UserService } from './../../../core/services/user.service';
import { forkJoin, Subscription } from 'rxjs';
import { Roles } from './../../../constants';
import { Constants } from 'src/app/constants';
import { Utilisateur } from './../../../shared/models/utilisateur.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CreationBailProvider } from 'src/app/core/providers/creation-bail.provider';
import { Bien } from 'src/app/shared/models/bien.model';

@Component({
  selector: 'app-choix-parties-bien',
  templateUrl: './choix-parties-bien.component.html',
  styleUrls: ['./choix-parties-bien.component.scss'],
})
export class ChoixPartiesBienComponent implements OnInit {
  /**
   * Formulaire pour la désignation des parties
   */
  form!: FormGroup;

  /**
   * souscription
   */
  subscription!: Subscription;

  /**
   * Nom du formulaire
   */
  formLabel = 'choixPartiesBien';

  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  /**
   * liste des locataires
   */
  locatairesList: Utilisateur[] = [];

  /**
   * liste des biens
   */
  biens: Bien[] = [];

  /**
   * Constructeur du composant
   * @param creationBailProvider Provider permettant d'enregistrer les étapes de création de bail
   * @param fb FormBuilder natif angular
   */
  constructor(
    private creationBailProvider: CreationBailProvider,
    private fb: FormBuilder,
    private router: Router,
    public constants: Constants,
    private userService: UserService,
    private bienService: BienService
  ) { }

  /**
   * Initialisation du formulaire
   */
  ngOnInit(): void {
    this.subscription = forkJoin({
      users: this.userService.getListUsersWithIdRole(Roles.LOCATAIRE),
      biens: this.bienService.getBiens(),
    }).subscribe(({ users, biens }) => {
      this.locatairesList = users as Utilisateur[];
      this.biens = biens as Bien[];
    });
    this.initForm();
  }

  /**
   * Initialisation du formulaire
   */
  initForm(): void {
    const formulairePrerempli = this.creationBailProvider.formGroups.find(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli) {
      this.form = formulairePrerempli.formGroup;
    } else {
      this.form = this.fb.group({
        bailleur: ['', [Validators.required]],
        locataire: ['', [Validators.required]],
        bien: ['', [Validators.required]],
      });
    }
  }

  /**
   * récupération des bailleurs
   */
  get selectBailleurs(): Utilisateur[] {
    const listIds: number[] = [];

    return this.biens
      .map((bien) => bien.proprietaire)
      .filter((proprietaire, index, self) => {
        const proprioId = listIds.indexOf(proprietaire.id) === -1;
        if (proprioId) {
          listIds.push(proprietaire.id);
        }
        return proprioId;
      });
  }

  /**
   * récupération des biens sélectionné
   */
  get selectBiens(): Bien[] {
    return this.biens.filter(
      (bien) => bien.proprietaire.id === this.form.controls.bailleur.value
    );
  }

  /**
   * changement de bailleur
   */
  changeBailleur(): void {
    this.form.controls.bien.setValue('');
  }

  /**
   * soumission du formulaire
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.form.valid) {
      const formulairePrerempli = this.creationBailProvider.formGroups.findIndex(
        (form) => form.formGroupLabel === this.formLabel
      );

      const selectedBien = this.selectBiens.find(
        (bien) => bien.id === this.form.controls.bien.value
      );

      const selectedLocataire = this.locatairesList.find(
        (locataire) => locataire.id === this.form.controls.locataire.value
      );

      const selectedBailleur = this.selectBailleurs.find(
        (bailleur) => bailleur.id === this.form.controls.bailleur.value
      );

      if (formulairePrerempli !== -1) {
        this.creationBailProvider.formGroups.splice(formulairePrerempli, 1, {
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      } else {
        this.creationBailProvider.formGroups.push({
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      }

      if (selectedBien && selectedLocataire && selectedBailleur) {
        this.creationBailProvider.bien = selectedBien;
        this.creationBailProvider.parties = {
          locataire: selectedLocataire,
          proprietaire: selectedBailleur,
        };
      }

      this.router.navigate(['admin', 'creation-bail', 'designation-parties']);
    }
  }
}
