import {
  ModalitesReglementChargesRecuperables,
  EcheancesPaiement,
} from './../../../constants';
import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Router } from '@angular/router';
import { CreationBailProvider } from 'src/app/core/providers/creation-bail.provider';

/**
 * component de conditions financieres creation bail
 */
@Component({
  selector: 'app-conditions-financieres',
  templateUrl: './conditions-financieres.component.html',
  styleUrls: ['./conditions-financieres.component.scss'],
})
export class ConditionsFinancieresComponent implements OnInit {
  /**
   * Formulaire pour la désignation des parties
   */
  form!: FormGroup;

  /**
   * Nom du formulaire
   */
  formLabel = 'conditionsFinancieres';

  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  /**
   * modalité de reglement des charges recuperables
   */
  modalitesReglementChargesRecuperables = ModalitesReglementChargesRecuperables;

  /**
   * écheances de paiement
   */
  echeancesPaiement = EcheancesPaiement;

  /**
   * Constructeur du composant
   * @param creationBailProvider Provider permettant d'enregistrer les étapes de création de bail
   * @param fb FormBuilder natif angular
   */
  constructor(
    private creationBailProvider: CreationBailProvider,
    private fb: FormBuilder,
    private router: Router
  ) {}

  /**
   * Initialisation du formulaire
   */
  ngOnInit(): void {
    this.initForm();
  }

  /**
   * Initialisation du formulaire
   */
  initForm(): void {
    const formulairePrerempli = this.creationBailProvider.formGroups.find(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli) {
      this.form = formulairePrerempli.formGroup;
    } else {
      this.form = this.fb.group({
        loyerMensuel: [
          {
            value: this.creationBailProvider.bien.prix,
            disabled: true,
          },
          [Validators.required, Validators.pattern('^\\d+(\\.\\d{1,2})?$')],
        ],
        loyerHorsCharges: [
          '',
          [Validators.required, Validators.pattern('^\\d+(\\.\\d{1,2})?$')],
        ],
        chargesRecuperables: [
          '',
          [Validators.required, Validators.pattern('^\\d+(\\.\\d{1,2})?$')],
        ],
        isMontantMaximumAnnuel: [false],
        isLoyerReference: [false],
        loyerReference: [
          '',
          [Validators.required, Validators.pattern('^\\d+(\\.\\d{1,2})?$')],
        ],
        periodicite: ['', [Validators.required]],
        loyerAncienLocataire: [
          '',
          [Validators.required, Validators.pattern('^\\d+(\\.\\d{1,2})?$')],
        ],
        modaliteReglementChargesRecuperables: ['', [Validators.required]],
        montantChargesRecuperables: [
          '',
          [Validators.pattern('^\\d+(\\.\\d{1,2})?$')],
        ],
        souscriptionAssuranceParBailleur: [false],
        periodicitePaiementLoyer: ['', [Validators.required]],
        echeancePaiement: ['', [Validators.required]],
        datePeriodePaiement: ['', [Validators.required]],
        montantPremiereEcheance: [
          '',
          [Validators.required, Validators.pattern('^\\d+(\\.\\d{1,2})?$')],
        ],
      });
    }
  }

  /**
   * Soumission du formulaire pour passer à l'étape suivante
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.form.valid) {
      const formulairePrerempli = this.creationBailProvider.formGroups.findIndex(
        (form) => form.formGroupLabel === this.formLabel
      );

      if (formulairePrerempli !== -1) {
        this.creationBailProvider.formGroups.splice(formulairePrerempli, 1, {
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      } else {
        this.creationBailProvider.formGroups.push({
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      }

      this.router.navigate(['admin', 'creation-bail', 'travaux-garanties']);
    }
  }

  /**
   * Ajoute/Supprime les champs du formulaire selon le choix de l'utilisateur
   */
  changeAssuranceParBailleur(): void {
    if (this.form.controls.souscriptionAssuranceParBailleur.value) {
      this.form.addControl(
        'montantRecuperableAssurance',
        new FormControl('', [Validators.required])
      );
      this.form.addControl(
        'montantRecuperableAssuranceDouzieme',
        new FormControl('', [Validators.required])
      );
    } else {
      if (
        this.form.contains('montantRecuperableAssurance') &&
        this.form.contains('montantRecuperableAssuranceDouzieme')
      ) {
        this.form.removeControl('montantRecuperableAssurance');
        this.form.removeControl('montantRecuperableAssuranceDouzieme');
      }
    }
  }
}
