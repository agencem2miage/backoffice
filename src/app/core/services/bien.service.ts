import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from 'src/app/constants';
import { map } from 'rxjs/operators';
import { Bien } from 'src/app/shared/models/bien.model';
import { environment } from 'src/environments/environment';
import { TypeContrat } from 'src/app/shared/models/typeContrat.model';
import { TypeBien } from 'src/app/shared/models/typeBien.model';
import { Adresse } from 'src/app/shared/models/adresse.model';
import { Utilisateur } from 'src/app/shared/models/utilisateur.model';

/**
 * Définit le content-type du header
 */
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
/**
 * services des biens
 */
export class BienService {
  /**
   * Point d'entrée de l'API pour la gestion des bails
   */
  bienEndpoint = environment.API_URL + this.constants.API_ENDPOINTS.BIEN;

  /**
   * Taille maximale du texte du bien affichée dans la mosaïque
   */
  descriptionLimit = 130;
  /**
   * Constructeur du service
   * @param http Service natif d'angular pour la gestion de requêtes HTTP
   * @param constants Constantes de l'application
   */
  constructor(private http: HttpClient, private constants: Constants) {}

  /**
   * récupération d'un bien a partir de son ID
   * @param id
   * @returns
   */
  getBien(id: number): Observable<Bien> {
    return this.http
      .get(this.bienEndpoint + '/' + id)
      .pipe(map((jsonResponse: any) => Bien.fromJson(jsonResponse[0])));
  }

  /**
   * récupération du détail d'un bien a partir de son id
   * @param id
   * @returns
   */
  getBienDetail(id: number): Observable<Bien> {
    return this.http
      .get(this.bienEndpoint + '/detail/' + id)
      .pipe(map((jsonResponse: any) => Bien.fromJson(jsonResponse[0])));
  }

  /**
   * récupération de la liste des biens détaillées
   * @returns
   */
  getBiens(): Observable<Bien[]> {
    return this.http
      .get(this.bienEndpoint + '/liste-bien-detail')
      .pipe(
        map((jsonResponse: any) =>
          jsonResponse.map((jsonBien: any) => Bien.fromJson(jsonBien))
        )
      );
  }

  /**
   * création d'un nouveau bien
   * @param newBien
   * @returns
   */
  createBien(newBien: Bien): Observable<any> {
    return this.http.post(
      this.bienEndpoint + '/creation-bien',
      { newBien },
      httpOptions
    );
  }

  /**
   * récupération d'un bien pour un agent immobilier (plus détaillé)
   * @param id
   * @returns
   */
  getBienAgent(id: number): Observable<Bien> {
    return this.http
      .get(this.bienEndpoint + '/admin/' + id)
      .pipe(map((jsonResponse: any) => Bien.fromJson(jsonResponse[0])));
  }

  /**
   * modification d'un bien
   * @param newBien
   * @returns
   */
  editBien(newBien: Bien): Observable<any> {
    return this.http.put(
      this.bienEndpoint + '/admin/' + newBien.id,
      { newBien },
      httpOptions
    );
  }

  /**
   * récupération de la liste des biens
   * @returns
   */
  getListeBien(): Observable<any[]> {
    return this.http.get(this.bienEndpoint + '/liste-bien-admin').pipe(
      map((jsonResponse: any) => {
        const listBien: Bien[] = [];
        jsonResponse.forEach((element: any) => {
          const bien: Bien = Bien.fromJson(element);
          if (bien.description.length >= this.descriptionLimit) {
            bien.description =
              bien.description.slice(0, this.descriptionLimit) + '... ';
          }
          listBien.push(bien);
        });
        return listBien;
      })
    );
  }

  /**
   * récupération de la liste des biens filtrés
   * @returns
   */
  getListeFilterBienAdmin(): Observable<any[]> {
    return this.http.get(this.bienEndpoint + '/filter-bien-admin').pipe(
      map((jsonResponse: any) => {
        const listTypeContrat: TypeContrat[] = [];
        const listTypeBien: TypeBien[] = [];
        const listLocalisation: Adresse[] = [];
        const listProprietaire: Utilisateur[] = [];
        const listLocation: number[] = [];
        const listNumBail: number[] = [];

        jsonResponse[0].listTypeContrat.forEach((element: any) => {
          const typeContrat: TypeContrat = TypeContrat.fromJson(element);
          listTypeContrat.push(typeContrat);
        });
        jsonResponse[0].listTypeBien.forEach((element: any) => {
          const typeBien: TypeBien = TypeBien.fromJson(element);
          listTypeBien.push(typeBien);
        });
        jsonResponse[0].listLocalisation.forEach((element: any) => {
          const localisation: Adresse = Adresse.fromJson(element);
          listLocalisation.push(localisation);
        });
        jsonResponse[0].listProprietaire.forEach((element: any) => {
          const proprietaire: Utilisateur = Utilisateur.fromJson(element);
          listProprietaire.push(proprietaire);
        });
        jsonResponse[0].listBail.forEach((element: any) => {
          listLocation.push(element.idBien);
          listNumBail.push(element.id);
        });
        return [
          {
            listTypeContrat: listTypeContrat,
            listTypeBien: listTypeBien,
            listLocalisation: listLocalisation,
            listProprietaire: listProprietaire,
            listLocation: listLocation,
            listNumBail: listNumBail,
          },
        ];
      })
    );
  }
}
