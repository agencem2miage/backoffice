import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CreationEdlProvider } from 'src/app/core/providers/creation-edl.provider';

@Component({
  selector: 'app-inventaire-meubles',
  templateUrl: './inventaire-meubles.component.html',
  styleUrls: ['./inventaire-meubles.component.scss'],
})
/**
 * classe d'inventaire des meubles
 */
export class InventaireMeublesComponent {
  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  /**
   * erreur
   */
  error = false;

  /**
   * constructeur du component
   * @param router
   * @param creationEdlProvider
   */
  constructor(
    private router: Router,
    private creationEdlProvider: CreationEdlProvider
  ) {}

  /**
   * méthode de soumission du formulaire
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.creationEdlProvider.inventaireFormGroups.length === 3) {
      this.router.navigate(['admin', 'creation-etat-des-lieux', 'signature']);
    } else {
      this.error = true;
    }
  }
}
