import { TestBed } from '@angular/core/testing';

import { CreationBailGuard } from './creation-bail.guard';

describe('CreationBailGuard', () => {
  let guard: CreationBailGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CreationBailGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
