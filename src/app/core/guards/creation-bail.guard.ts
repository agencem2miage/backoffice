import { CreationBailProvider } from './../providers/creation-bail.provider';
import { Injectable } from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  CanActivateChild,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
/**
 * guard de création de bail
 */
export class CreationBailGuard implements CanActivateChild {
  /**
   * nom du formulaire
   */
  formsNames: any = {
    designationParties: 'designation-parties',
    objetContrat: 'objet-contrat',
    priseEffetContrat: 'prise-effet-contrat',
    conditionsFinancieres: 'conditions-financieres',
    travauxGaranties: 'travaux-garanties',
    signature: 'signature',
  };

  /**
   * constructeur de création de bail
   * @param creationBailProvider
   * @param router
   * @param route
   */
  constructor(
    private creationBailProvider: CreationBailProvider,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  /**
   * Activation des enfants
   * @param childRoute
   * @param state
   * @returns
   */
  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    let isFormOk = true;

    if (
      Object.values(this.formsNames).includes(
        childRoute.routeConfig?.path || ''
      )
    ) {
      isFormOk = this.creationBailProvider.isFormFilled(
        (Object.keys(this.formsNames)[
          Object.values(this.formsNames).findIndex(
            (formName) => formName === childRoute.routeConfig?.path || ''
          ) - 1
        ] as string) || 'choixPartiesBien'
      );
    }

    const parentUrl = state.url.slice(
      0,
      state.url.indexOf(childRoute.url[childRoute.url.length - 1].path)
    );
    return isFormOk
      ? true
      : this.router.navigate([parentUrl, 'choix-parties-bien'], {
          relativeTo: this.route,
        });
  }

  /**
   * Désactivation du guard
   * @returns
   */
  canDeactivate():
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    this.creationBailProvider.cleanProvider();
    return true;
  }
}
