export class legalRegime {
  /**
   * Identifiant d'un régime juridique
   */
  readonly id: number;

  /**
   * Label d'un régime juridique
   */
  label: string;

  /**
   * Constructeur de l'objet, tout les paramètres sont obligatoires
   * @param option
   */
  constructor(option: { id: number; label: string }) {
    this.id = option.id || 0;
    this.label = option.label || '';
  }

  /**
   * Crée un régime juridique à partir d'un flux JSON
   * @param json Les régimes juridique
   * @returns  {legalRegime} l'objet
   */
  public static fromJson(json: any): legalRegime {
    return new legalRegime({
      id: json.id,
      label: json.label,
    });
  }
}
