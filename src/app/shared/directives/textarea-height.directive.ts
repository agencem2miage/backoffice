import {
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
} from '@angular/core';

@Directive({
  /**
   * Selecteur
   */
  selector: '[appTextareaHeight]',
})
/**
 * Classe de directive de hauteur de zone de texte
 */
export class TextareaHeightDirective {
  /**
   * Constructeur de la directive
   * @param el
   */
  constructor(private el: ElementRef) {}

  @HostBinding('style.height')
  /**
   * taille
   */
  height!: string;

  /**
   * monte la hauteur
   */
  @HostListener('window:keyup') onKeyUp(): void {
    this.height = 1 + this.el.nativeElement.scrollHeight + 'px';
  }
}
