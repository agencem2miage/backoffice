import { TestBed } from '@angular/core/testing';

import { CreationEdlGuard } from './creation-edl.guard';

describe('CreationEdlGuard', () => {
  let guard: CreationEdlGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CreationEdlGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
