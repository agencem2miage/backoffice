import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'enumToObject',
})
/**
 * transforme un enum en objet
 */
export class EnumToObjectPipe implements PipeTransform {
  // eslint-disable-next-line @typescript-eslint/ban-types
  /**
   * Transforme un objet
   * @param data
   * @returns
   */
  transform(data: Object): any {
    const finalObject: any = {};
    const keys = Object.keys(data);
    const array = keys.slice(keys.length / 2);

    array.forEach((item, index) => (finalObject[index + 1] = item));

    return finalObject;
  }
}
