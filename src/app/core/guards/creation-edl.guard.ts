import { CreationEdlProvider } from 'src/app/core/providers/creation-edl.provider';
import { Injectable } from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  CanActivateChild,
  CanDeactivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
/**
 * Guard de création d'un état des lieux
 */
export class CreationEdlGuard
  implements CanActivateChild, CanDeactivate<unknown> {
  /**
   * cosntructeur du guard
   * @param creationEdlProvider
   * @param router
   * @param route
   */
  constructor(
    private creationEdlProvider: CreationEdlProvider,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  /**
   * méthode d'activation des enfants
   * @param childRoute
   * @param state
   * @returns
   */
  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    let isFormOk = true;

    switch (childRoute.routeConfig?.path) {
      case 'pieces':
        isFormOk =
          this.creationEdlProvider.formGroups.findIndex(
            (form) => form.formGroupLabel === 'choixBail'
          ) !== -1;
        break;
      case 'inventaire-meubles':
        if (this.creationEdlProvider.bail) {
          isFormOk =
            this.creationEdlProvider.piecesFormGroups.length ===
            this.creationEdlProvider.bail.bien.pieces.length;
        } else {
          isFormOk = false;
        }
        break;
      case 'signature':
        if (this.creationEdlProvider.bail) {
          isFormOk =
            this.creationEdlProvider.inventaireFormGroups.length > 0 ||
            !this.creationEdlProvider.bail.bien.meuble;
        } else {
          isFormOk = false;
        }
        break;
      default:
        break;
    }

    const parentUrl = state.url.slice(
      0,
      state.url.indexOf(childRoute.url[childRoute.url.length - 1].path)
    );

    return isFormOk
      ? true
      : this.router.navigate([parentUrl, 'choix-bail'], {
          relativeTo: this.route,
        });
  }

  /**
   * désactivation du guard
   * @param component
   * @param currentRoute
   * @param currentState
   * @param nextState
   * @returns
   */
  canDeactivate(
    component: unknown,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return true;
  }
}
