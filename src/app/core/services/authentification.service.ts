import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Constants } from 'src/app/constants';
import { environment } from 'src/environments/environment';
import { Md5 } from 'ts-md5';
import { Utilisateur } from 'src/app/shared/models/utilisateur.model';
import { Adresse } from 'src/app/shared/models/adresse.model';

/**
 * Définit le content-type du header
 */
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class AuthentificationService {
  /**
   * Point d'entrée de l'API pour la gestion de la connexion
   */
  connexionEndpoint =
    environment.API_URL + this.constants.API_ENDPOINTS.CONNEXION;

  /**
   * url d'inscription
   */
  registerEndpoint =
    environment.API_URL + this.constants.API_ENDPOINTS.REGISTER;

  /**
   * Constructeur du service
   * @param http Service natif d'angular pour la gestion de requêtes HTTP
   * @param constants Constantes de l'application
   */
  constructor(private http: HttpClient, private constants: Constants) {}

  /**
   * Méthode de connexion
   * @param login
   * @param password
   * @returns
   */
  login(
    login: string,
    password: string
  ): Observable<{ user: any; token: string }> {
    const passwordCrypt = Md5.hashStr(password).toString();
    return this.http
      .post(
        this.connexionEndpoint + '/admin',
        { login, passwordCrypt },
        httpOptions
      )
      .pipe(
        map((jsonResponse: any) => {
          /**
           * TODO : récupération du token + type d'utilisateur connecté
           */
          return {
            user: jsonResponse.user,
            token: jsonResponse.authenticationToken,
          };
        })
      );
  }

  /**
   * méthode d'inscription
   * @param user
   * @param address
   * @returns
   */
  register(user: Utilisateur, address: Adresse): Observable<any> {
    return this.http.post(
      this.registerEndpoint,
      { address, user },
      httpOptions
    );
  }
}
