import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Constants } from 'src/app/constants';
import { Observable } from 'rxjs';
import { Utilisateur } from 'src/app/shared/models/utilisateur.model';
import { map } from 'rxjs/operators';

/**
 * définit le content-type du header
 */
const httpOption = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

/**
 * Définit le content-type du header
 */
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
/**
 * Services des utilisateurs
 */
export class UserService {
  userEndpoint = environment.API_URL + this.constants.API_ENDPOINTS.USER;

  /**
   * Import des services nécessaires
   * @param http Service natif d'angular pour la création de requêtes HTTP
   * @param constants Fichier qui contient les constantes du projet
   */
  constructor(private http: HttpClient, private constants: Constants) {}

  /**
   * Fonction de suppression de l'utilsateur
   * @param userId identifiant de l'utilisateur
   * @returns requête http delete pour l'utilisateur
   */
  deleteUser(email: string): Observable<any> {
    return this.http.delete(this.userEndpoint + '/' + email);
  }

  /**
   * Récupère un utilisateur auprès de l'API Agence
   * @param id Identifiant de l'utilisateur à récupérer
   * @returns L'utilisateur récupéré
   */
  updateUser(user: Utilisateur): Observable<any> {
    return this.http
      .put(this.userEndpoint + '/' + 'editProfile', { user }, httpOptions)
      .pipe(
        map(
          (jsonResponse: any) =>
            jsonResponse.map((jsonUser: any) => jsonUser)[0]
        )
      );
  }

  /**
   * Récupère la liste des utilisateurs
   * @returns La liste des utilisateurs
   */
  getListUsers(): Observable<Utilisateur[]> {
    return this.http
      .get(this.userEndpoint + '/')
      .pipe(
        map((jsonResponse: any) =>
          jsonResponse.map((jsonUser: any) => Utilisateur.fromJson(jsonUser))
        )
      );
  }

  /**
   * Récupère la liste des utilisateurs suivant leur rôle
   * @param idRole L'identifiant du rôle
   * @returns une liste d'utilisateurs
   */
  getListUsersWithIdRole(idRole: number): Observable<Utilisateur[]> {
    return this.http
      .get(this.userEndpoint + '/role/' + idRole)
      .pipe(
        map((jsonResponse: any) =>
          jsonResponse.map((jsonUser: any) => Utilisateur.fromJson(jsonUser))
        )
      );
  }

  /**
   * Récupère la liste des utilisateurs suivant leur rôle
   * @param idRole L'identifiant du rôle
   * @returns une liste d'utilisateurs
   */
  getEmailUsersWithIdRole(idRole: number): Observable<string[]> {
    return this.http
      .get(this.userEndpoint + '/emailRole/' + idRole)
      .pipe(
        map((jsonResponse: any) =>
          jsonResponse.map((jsonUser: any) => jsonUser['USER_mail'])
        )
      );
  }

  /**
   *
   * récupère un utilisateur en fonction de son ID
   * @param id id utilisateur
   * @returns utilisateur
   */
  getUser(id: number): Observable<Utilisateur> {
    return this.http
      .get(this.userEndpoint + '/' + id)
      .pipe(map((jsonResponse: any) => Utilisateur.fromJson(jsonResponse[0])));
  }

  /**
   * ajoute un nouvel utilisateur
   * @param newUser
   * @returns
   */
  addUser(newUser: Utilisateur): Observable<any> {
    return this.http.post(this.userEndpoint, { newUser }, httpOption);
  }

  /**
   * Récupère la liste des propriétaires
   * @returns liste proprietaire
   */
  getProprietaires(): Observable<any> {
    return this.http
      .get(this.userEndpoint + '/listeProprietaire')
      .pipe(
        map((jsonResponse: any) =>
          jsonResponse.map((jsonUser: any) => Utilisateur.fromJson(jsonUser))
        )
      );
  }
}
