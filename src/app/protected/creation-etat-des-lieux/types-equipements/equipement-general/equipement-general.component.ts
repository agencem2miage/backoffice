import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EtatsEquipements } from 'src/app/constants';
import { CreationEdlProvider } from 'src/app/core/providers/creation-edl.provider';

@Component({
  selector: 'app-equipement-general',
  templateUrl: './equipement-general.component.html',
  styleUrls: ['./equipement-general.component.scss'],
})

/**
 * Component d'équipement
 */
export class EquipementGeneralComponent implements OnInit {
  /**
   * formulaire
   */
  form!: FormGroup;
  /**
   * soumission
   */
  submitted = false;
  /**
   * Etat de l'équipement
   */
  etats = EtatsEquipements;
  /**
   * titre du formulaire
   */
  formLabel = 'equipementGeneral';

  /**
   * constructeur de l'équipement
   * @param fb formulaire
   * @param creationEdlProvider provider de création d'équipement
   */
  constructor(
    private fb: FormBuilder,
    private creationEdlProvider: CreationEdlProvider
  ) {}

  /**
   * méthode d'initialisation du component
   */
  ngOnInit(): void {
    const formulairePrerempli = this.creationEdlProvider.inventaireFormGroups.find(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli) {
      this.form = formulairePrerempli.formGroup;
    } else {
      this.form = this.fb.group({
        grandesAssiettes: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        assiettesDessert: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        assiettesCreuses: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        autresAssiettes: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        fourchettes: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        petitesCuilleres: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        grandesCuilleres: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        couteauxTable: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        couteauxCuisine: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        couteauxPain: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        verresPied: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        autresVerres: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        bols: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        tasses: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        soucoupes: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        couvertsService: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        tireBouchon: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        decapsuleur: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        carafes: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        planchesDecouper: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        plats: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        saladiers: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        passoires: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        poeles: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        casseroles: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        egouttoir: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        ouvreBoites: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        balaisBalayettes: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        pelles: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        seaux: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        torchons: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        matelas: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        traversins: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        taiesTraversin: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        oreillers: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        taiesOreillers: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        drapsDessous: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        draps: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        couettes: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        houssesCouettes: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        couvertures: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        alaises: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        couvreLits: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        peignoirsBain: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        serviettesBain: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        serviettesToilette: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        gantsToilette: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        nappes: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        serviettesTable: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        coussins: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
      });
    }
  }

  /**
   * méthode de soumission du formulaire
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.form.valid) {
      const formulairePrerempli = this.creationEdlProvider.inventaireFormGroups.findIndex(
        (form) => form.formGroupLabel === this.formLabel
      );

      if (formulairePrerempli !== -1) {
        this.creationEdlProvider.inventaireFormGroups.splice(
          formulairePrerempli,
          1,
          {
            formGroupLabel: this.formLabel,
            formGroup: this.form,
          }
        );
      } else {
        this.creationEdlProvider.inventaireFormGroups.push({
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      }

      let elementStyle = document.querySelector(
        '.equipementGeneralItem'
      ) as HTMLElement;
      elementStyle.style.display = 'none';

      elementStyle = document.querySelector(
        '.equipementGeneral'
      ) as HTMLElement;

      elementStyle.style.backgroundColor = '#60b664';
    }
  }

  /**
   * méthode d'édition du formulaire
   */
  editForm(): void {
    let elementStyle = document.querySelector(
      '.equipementGeneralItem'
    ) as HTMLElement;
    elementStyle.style.display = 'block';

    elementStyle = document.querySelector('.equipementGeneral') as HTMLElement;

    elementStyle.style.backgroundColor = '#fff';
  }
}
