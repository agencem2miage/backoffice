import { Component, OnInit } from '@angular/core';
import { CompleterData, CompleterService } from 'ng2-completer';
import { Constants, Roles } from 'src/app/constants';
import { ModalService } from 'src/app/core/services/modal.service';
import { NotificationsService } from 'src/app/core/services/notifications.service';
import { UserService } from 'src/app/core/services/user.service';
import { Modal, ModalType } from 'src/app/shared/models/modal.model';
import {
  NotificationBackground,
  NotificationIcon,
  Notification,
} from 'src/app/shared/models/notification.model';

@Component({
  selector: 'app-suppression-compte',
  templateUrl: './suppression-compte.component.html',
  styleUrls: ['./suppression-compte.component.scss'],
})
export class SuppressionCompteComponent implements OnInit {
  submitted = false;
  emailFound!: any;
  searchUser!: string[];
  dataService!: CompleterData;
  roles = Roles;
  displayedUser!: number;
  /**
   * Constructeur du composant
   * @param modalService Service pour la création du modal
   * @param notificationService Service pour la création de notification
   */
  constructor(
    public constants: Constants,
    private userService: UserService,
    private modalService: ModalService,
    private notificationService: NotificationsService,
    private completerService: CompleterService
  ) {
    this.dataService = completerService.local(
      this.searchUser,
      'email',
      'email'
    );
  }

  ngOnInit(): void {
    this.displayedUser = 4;
    this.fillData(4);
  }

  onSubmit(): void {
    this.submitted = true;

    const notification = new Notification({
      message: 'Compte supprimé avec succés !',
      background: NotificationBackground.GREEN,
      icon: NotificationIcon.CHECK,
    });

    const notificationErr = new Notification({
      message:
        'Le site a rencontré une erreur interne. Veuillez nous excuser pour la gêne occasionnée.',
      background: NotificationBackground.RED,
      icon: NotificationIcon.CHECK,
    });

    const confirmationModal = new Modal({
      title: 'Confirmation',
      text: 'Êtes-vous sûr de vouloir supprimer le compte ?',
      type: ModalType.CONFIRMATION,
    });

    confirmationModal.confirm = () => {
      try {
        this.userService.deleteUser(this.emailFound).subscribe();
        this.notificationService.genericNotification(notification);
        this.emailFound = '';
      } catch (error) {
        this.notificationService.genericNotification(notificationErr);
      }
    };

    this.modalService.confirmationModal(confirmationModal);
  }

  switchDataUser(switchType: number): void {
    this.displayedUser = switchType;
    console.log(switchType);
    switch (switchType) {
      case 2:
        this.fillData(2);
        break;
      case 3:
        this.fillData(3);
        break;
      case 4:
        this.fillData(4);
        break;
    }
  }

  fillData(idRole: number): void {
    this.searchUser = [];
    this.userService.getEmailUsersWithIdRole(idRole).subscribe((res) => {
      this.searchUser = res;
    });
  }
}
