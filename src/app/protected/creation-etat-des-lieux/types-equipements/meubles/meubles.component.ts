import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EtatsEquipements } from 'src/app/constants';
import { CreationEdlProvider } from 'src/app/core/providers/creation-edl.provider';

@Component({
  selector: 'app-meubles',
  templateUrl: './meubles.component.html',
  styleUrls: ['./meubles.component.scss'],
})
/**
 * classe de meuble
 */
export class MeublesComponent implements OnInit {
  /**
   * formulaire de meuble
   */
  form!: FormGroup;
  /**
   * boolean de soumission du formulaire
   */
  submitted = false;
  /**
   * Formulaire d'état de l'équipement
   */
  etats = EtatsEquipements;

  /**
   * titre du formulaire
   */
  formLabel = 'meubles';

  /**
   * Constructeur du component
   * @param fb formulaire
   * @param creationEdlProvider création du provider
   */
  constructor(
    private fb: FormBuilder,
    private creationEdlProvider: CreationEdlProvider
  ) {}

  /**
   * Méthode d'initialisation du formulaire
   */
  ngOnInit(): void {
    const formulairePrerempli = this.creationEdlProvider.inventaireFormGroups.find(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli) {
      this.form = formulairePrerempli.formGroup;
    } else {
      this.form = this.fb.group({
        chaisesSejour: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        chaisesChambres: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        chaisesCuisine: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        chaisesAutres: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        tabourets: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        fauteuils: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        canapes: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        tablesSejour: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        tablesChambres: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        tablesCuisine: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        tablesNuit: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        tablesAutres: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        bureaux: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        commodes: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        armoires: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        buffets: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        litsSimples: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        litsDoubles: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        placards: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        lustresPlafonniers: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
        lampesAppliques: this.fb.group({
          etat: ['1'],
          observations: [''],
        }),
      });
    }
  }

  /**
   * méthode de soumission du formulaire
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.form.valid) {
      const formulairePrerempli = this.creationEdlProvider.inventaireFormGroups.findIndex(
        (form) => form.formGroupLabel === this.formLabel
      );

      if (formulairePrerempli !== -1) {
        this.creationEdlProvider.inventaireFormGroups.splice(
          formulairePrerempli,
          1,
          {
            formGroupLabel: this.formLabel,
            formGroup: this.form,
          }
        );
      } else {
        this.creationEdlProvider.inventaireFormGroups.push({
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      }

      let elementStyle = document.querySelector('.meublesItem') as HTMLElement;
      elementStyle.style.display = 'none';

      elementStyle = document.querySelector('.meubles') as HTMLElement;

      elementStyle.style.backgroundColor = '#60b664';
    }
  }

  /**
   * méthode d'édition du formulaire
   */
  editForm(): void {
    let elementStyle = document.querySelector('.meublesItem') as HTMLElement;
    elementStyle.style.display = 'block';

    elementStyle = document.querySelector('.meubles') as HTMLElement;

    elementStyle.style.backgroundColor = '#fff';
  }
}
