import { Modal, ModalType } from 'src/app/shared/models/modal.model';
import { ModalService } from './../services/modal.service';
import { TokenService } from './../services/token.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/**
 * Composant Header
 */
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  constructor(
    private tokenService: TokenService,
    private router: Router,
    private modalService: ModalService
  ) {}

  /**
   * boolean si on est connecté
   */
  isConnected = false;

  /**
   * méthode d'init du component
   */
  ngOnInit(): void {
    this.isConnected = this.tokenService.getToken() !== '';
  }

  /**
   * Vérifie l'état de connexion de l'administrateur
   */
  ngDoCheck(): void {
    this.isConnected = this.tokenService.getToken() !== '';
  }

  /**
   * méthode de déconnexion
   */
  logOut(): void {
    const confirmation = new Modal({
      type: ModalType.CONFIRMATION,
      title: 'Déconnexion',
      text: 'Souhaitez-vous vraiment vous déconnecter ?',
    });

    confirmation.confirm = () => {
      this.tokenService.logOut();
      this.router.navigate(['/']);
      this.isConnected = false;
    };

    this.modalService.confirmationModal(confirmation);
  }
}
