import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { Constants } from '../../constants';
import { Modal, ModalType } from 'src/app/shared/models/modal.model';
import { ModalService } from '../../core/services/modal.service';
import { NotificationsService } from 'src/app/core/services/notifications.service';
import {
  NotificationBackground,
  NotificationIcon,
  Notification,
} from 'src/app/shared/models/notification.model';
import { FileService } from 'src/app/core/services/file.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss'],
})
export class UploadComponent implements OnInit {
  constructor(
    public constants: Constants,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private fileService: FileService,
    private modalService: ModalService,
    private notificationsService: NotificationsService
  ) {}

  url_basic: string = environment.FILE_LOCATION_API;

  updateFileLocationSub!: Subscription;

  getListeFileLocation!: Subscription;

  /**
   * Formulaire de connexion
   */
  uploadForm!: FormGroup;

  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  //BOUCHONNERRRRRR
  idBail = 1;

  listFileLocation = {
    listFileBail: [''],
    listFileEtatLieu: [''],
    listFileAssuranceProprietaire: [''],
    listFileAssuranceLocataire: [''],
    listFileContratGestion: [''],
    listFileDiagEnergie: [''],
    listFileAutre: [''],
  };

  listFileNameBail: Array<string> = [];
  listFileNameEtatLieu: Array<string> = [];
  listFileNameAssuranceProprietaire: Array<string> = [];
  listFileNameAssuranceLocataire: Array<string> = [];
  listFileNameContratGestion: Array<string> = [];
  listFileNameDiagEnergie: Array<string> = [];
  listFileNameAutre: Array<string> = [];

  ngOnInit(): void {
    this.getListeFileLocation = this.fileService
      .getListeFileLocation(this.idBail)
      .subscribe((result) => {
        this.listFileLocation = result;
      });
    this.initForm();
  }

  initForm(): void {
    this.uploadForm = this.fb.group({
      fileBail: [''],
      fileEtatLieu: [''],
      fileAssuranceProprietaire: [''],
      fileAssuranceLocataire: [''],
      fileContratGestion: [''],
      fileDiagEnergie: [''],
      fileAutre: [''],
    });
  }

  /**
   * Permet de retourner les controls du formulaire facilement
   */
  get f() {
    return this.uploadForm.controls;
  }

  /**
   * Enregistre l'image passée par l'utilisateur en base64
   * @param event Evenement contenant l'image
   */
  onFileChange(event: any, type: string): void {
    if (event.target.files && event.target.files.length) {
      const finalResult: Array<string | ArrayBuffer | null> = [];
      const file = event.target.files;
      const countFile = file.length;
      switch (type) {
        case 'bail': {
          this.listFileNameBail = [];
          break;
        }
        case 'etatLieu': {
          this.listFileNameEtatLieu = [];
          break;
        }
        case 'assuranceProprietaire': {
          this.listFileNameAssuranceProprietaire = [];
          break;
        }
        case 'assuranceLocation': {
          this.listFileNameAssuranceLocataire = [];
          break;
        }
        case 'contratGestion': {
          this.listFileNameContratGestion = [];
          break;
        }
        case 'diagEnergie': {
          this.listFileNameDiagEnergie = [];
          break;
        }
        case 'autre': {
          this.listFileNameAutre = [];
          break;
        }
      }
      for (let i = 0; i < countFile; i++) {
        if (
          file.item(i).type === 'application/pdf' &&
          file.item(i).size <= 5000000
        ) {
          switch (type) {
            case 'bail': {
              this.listFileNameBail.push(file.item(i).name);
              break;
            }
            case 'etatLieu': {
              this.listFileNameEtatLieu.push(file.item(i).name);
              break;
            }
            case 'assuranceProprietaire': {
              this.listFileNameAssuranceProprietaire.push(file.item(i).name);
              break;
            }
            case 'assuranceLocation': {
              this.listFileNameAssuranceLocataire.push(file.item(i).name);
              break;
            }
            case 'contratGestion': {
              this.listFileNameContratGestion.push(file.item(i).name);
              break;
            }
            case 'diagEnergie': {
              this.listFileNameDiagEnergie.push(file.item(i).name);
              break;
            }
            case 'autre': {
              this.listFileNameAutre.push(file.item(i).name);
              break;
            }
          }

          const reader = new FileReader();
          reader.readAsDataURL(file.item(i));
          reader.onload = () => {
            finalResult.push(reader.result);
          };
        } else {
          const notification = new Notification({
            message: "Votre fichier n'est pas valide.",
            background: NotificationBackground.RED,
            icon: NotificationIcon.CROSS,
          });
          this.notificationsService.genericNotification(notification);
        }
      }

      switch (type) {
        case 'bail': {
          this.uploadForm.patchValue({
            fileBail: finalResult,
          });
          break;
        }
        case 'etatLieu': {
          this.uploadForm.patchValue({
            fileEtatLieu: finalResult,
          });
          break;
        }
        case 'assuranceProprietaire': {
          this.uploadForm.patchValue({
            fileAssuranceProprietaire: finalResult,
          });
          break;
        }
        case 'assuranceLocation': {
          this.uploadForm.patchValue({
            fileAssuranceLocataire: finalResult,
          });
          break;
        }
        case 'contratGestion': {
          this.uploadForm.patchValue({
            fileContratGestion: finalResult,
          });
          break;
        }
        case 'diagEnergie': {
          this.uploadForm.patchValue({
            fileDiagEnergie: finalResult,
          });
          break;
        }
        case 'autre': {
          this.uploadForm.patchValue({
            fileAutre: finalResult,
          });
          break;
        }
      }
      this.cd.markForCheck();
    }
  }

  /**
   * Supprime l'image sélectionnée
   */
  onDeleteFile(type: string): void {
    switch (type) {
      case 'bail': {
        this.listFileNameBail = [];
        this.uploadForm.patchValue({
          fileBail: '',
        });
        break;
      }
      case 'etatLieu': {
        this.listFileNameEtatLieu = [];
        this.uploadForm.patchValue({
          fileEtatLieu: '',
        });
        break;
      }
      case 'assuranceProprietaire': {
        this.listFileNameAssuranceProprietaire = [];
        this.uploadForm.patchValue({
          fileAssuranceProprietaire: '',
        });
        break;
      }
      case 'assuranceLocation': {
        this.listFileNameAssuranceLocataire = [];
        this.uploadForm.patchValue({
          fileAssuranceLocataire: '',
        });
        break;
      }
      case 'contratGestion': {
        this.listFileNameContratGestion = [];
        this.uploadForm.patchValue({
          fileContratGestion: '',
        });
        break;
      }
      case 'diagEnergie': {
        this.listFileNameDiagEnergie = [];
        this.uploadForm.patchValue({
          fileDiagEnergie: '',
        });
        break;
      }
      case 'autre': {
        this.listFileNameAutre = [];
        this.uploadForm.patchValue({
          fileAutre: '',
        });
        break;
      }
    }
  }

  /**
   * méthode appelée lors de la soumission du formulaire.
   * Elle appelle l'API de connexion
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.uploadForm.valid) {
      const confirmationModal = new Modal({
        title: 'Confirmation',
        text: 'Êtes-vous sûr de vouloir envoyer ces pièces justificatives ?',
        type: ModalType.CONFIRMATION,
      });

      confirmationModal.confirm = () => {
        const listFile = {
          listFileBail: this.f.fileBail.value,
          listFileEtatLieu: this.f.fileEtatLieu.value,
          listFileAssuranceProprietaire: this.f.fileAssuranceProprietaire.value,
          listFileAssuranceLocataire: this.f.fileAssuranceLocataire.value,
          listFileContratGestion: this.f.fileContratGestion.value,
          listFileDiagEnergie: this.f.fileDiagEnergie.value,
          listFileAutre: this.f.fileAutre.value,
        };
        this.updateFileLocationSub = this.fileService
          .updateFileLocation(listFile, '' + this.idBail)
          .subscribe(() => {
            const notification = new Notification({
              message: 'Vos fichiers ont bien été ajoutés sur le serveur.',
              background: NotificationBackground.GREEN,
              icon: NotificationIcon.CHECK,
            });
            this.notificationsService.genericNotification(notification);
            this.getListeFileLocation = this.fileService
              .getListeFileLocation(this.idBail)
              .subscribe((result) => {
                this.listFileLocation = result;
              });
          });

        this.uploadForm.patchValue({
          fileBail: '',
          fileEtatLieu: '',
          fileAssuranceProprietaire: '',
          fileAssuranceLocataire: '',
          fileContratGestion: '',
          fileDiagEnergie: '',
          fileAutre: '',
        });
      };
      this.modalService.confirmationModal(confirmationModal);
    }
  }
}
