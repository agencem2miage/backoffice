import { environment } from './../../../../environments/environment';
import { Router } from '@angular/router';
import { CreationEdlProvider } from './../../../core/providers/creation-edl.provider';
import { UserService } from './../../../core/services/user.service';
import { BienService } from 'src/app/core/services/bien.service';
import { BailService } from './../../../core/services/bail.service';
import { Bail } from './../../../shared/models/bail.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { forkJoin, Subscription } from 'rxjs';

@Component({
  selector: 'app-choix-bail',
  templateUrl: './choix-bail.component.html',
  styleUrls: ['./choix-bail.component.scss'],
})
/**
 * Component de choix du bail
 */
export class ChoixBailComponent implements OnInit {
  /**
   * Formulaire pour la désignation des parties
   */
  form!: FormGroup;

  /**
   * subsription de liste de bail
   */
  subscriptionListeBails!: Subscription;

  /**
   * subscription de détail
   */
  subscriptionDetail!: Subscription;

  /**
   * Nom du formulaire
   */
  formLabel = 'choixBail';

  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  /**
   * liste de bails
   */
  listeBails: Bail[] = [];

  lienPdf: string = environment.FILE_LOCATION_API;

  /**
   * constructeur du component
   * @param bailService
   * @param fb
   * @param bienService
   * @param userService
   * @param creationEdlProvider
   * @param router
   */
  constructor(
    private bailService: BailService,
    private fb: FormBuilder,
    private bienService: BienService,
    private userService: UserService,
    private creationEdlProvider: CreationEdlProvider,
    private router: Router
  ) {}

  /**
   * méthode d'init du component
   */
  ngOnInit(): void {
    this.subscriptionListeBails = this.bailService
      .getBails()
      .subscribe((bails) => (this.listeBails = bails));
    this.initForm();
  }

  /**
   * initialisaiton du formulaire
   */
  initForm(): void {
    const formulairePrerempli = this.creationEdlProvider.formGroups.find(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli) {
      this.form = formulairePrerempli.formGroup;
    } else {
      this.form = this.fb.group({
        bail: ['', [Validators.required]],
        edlEntree: [false],
        edlSortie: [false],
        dateEntree: [''],
        dateSortie: [''],
      });
    }
  }

  /**
   * retourne l'id du bien sélectionné
   */
  get selectedBienId(): number {
    return (
      this.listeBails.find((bail) => bail.id === this.form.controls.bail.value)
        ?.bien.id || -1
    );
  }

  /**
   * retourne l'id du locataire sélectionné
   */
  get selectedLocataireId(): number {
    return (
      this.listeBails.find((bail) => bail.id === this.form.controls.bail.value)
        ?.locataire.id || -1
    );
  }

  get selectedBail(): Bail | undefined {
    return this.listeBails.find(
      (bail) => bail.id === this.form.controls.bail.value
    );
  }

  /**
   * return le détail du bail sélectionné
   */
  detailSelectedBail(): void {
    this.subscriptionDetail = forkJoin([
      this.bienService.getBienDetail(this.selectedBienId),
      this.userService.getUser(this.selectedLocataireId),
    ]).subscribe(([bien, locataire]) => {
      const bailIndex = this.listeBails.findIndex(
        (bail) => bail.bien.id === this.selectedBienId
      );
      this.listeBails[bailIndex].bien = bien;
      this.listeBails[bailIndex].locataire = locataire;
    });
  }

  /**
   * Méthode de soumission du formulaire
   */
  onSubmit(): void {
    this.submitted = true;

    if (
      this.form.valid &&
      ((this.form.controls.edlEntree.value &&
        this.form.controls.dateEntree.value.length > 0) ||
        (this.form.controls.edlSortie.value &&
          this.form.controls.dateSortie.value.length > 0))
    ) {
      const formulairePrerempli = this.creationEdlProvider.formGroups.findIndex(
        (form) => form.formGroupLabel === this.formLabel
      );

      if (formulairePrerempli !== -1) {
        this.creationEdlProvider.formGroups.splice(formulairePrerempli, 1, {
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      } else {
        this.creationEdlProvider.formGroups.push({
          formGroupLabel: this.formLabel,
          formGroup: this.form,
        });
      }

      this.creationEdlProvider.bail = this.listeBails[
        this.listeBails.findIndex(
          (bail) => bail.id === this.form.controls.bail.value
        )
      ];

      this.router.navigate(['admin', 'creation-etat-des-lieux', 'pieces']);
    }
  }
}
