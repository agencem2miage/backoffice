import { Adresse } from './adresse.model';
import { Role } from './role.model';

/**
 * Modèle représentant un utilisateur
 */
export class Utilisateur {
  /**
   * Identifiant de l'utilisateur
   */
  readonly id: number;

  /**
   * Type de l'utilisateur : admin, proprietaire, agent, locataire
   */
  role: Role;

  /**
   * Civilité de l'utilisateur : Monsieur, Madame ou Autre
   */
  civilite: string;

  /**
   * Nom d'utilisateur
   */
  nom: string;

  /**
   * Prénom de l'utilisateur
   */
  prenom: string;

  /**
   * Adresse postale de l'utilisateur
   */
  adresse: Adresse;

  /**
   * Mail de l'utilisateur
   */
  mail: string;

  /**
   * Date de naissance de l'utilisateur
   */
  dateNaissance: string;

  /**
   * Ville de naissance de l'utilisateur
   */
  villeNaissance: string;

  /**
   * Numero de téléphone de l'utilisateur
   */
  numeroTelephone: string;

  /**
   * Mot de passe
   */
  motDePasse: string;

  /**
   * Constructeur de l'objet, tous les paramètres sont obligatoires
   * @param options
   */
  constructor(option: {
    id: number;
    role: Role;
    civilite: string;
    nom: string;
    prenom: string;
    adresse: Adresse;
    mail: string;
    dateNaissance: string;
    villeNaissance: string;
    numeroTelephone: string;
    motDePasse: string;
  }) {
    this.id = option.id || 0;
    this.role = option.role || '';
    this.civilite = option.civilite || '';
    this.nom = option.nom || '';
    this.prenom = option.prenom || '';
    this.adresse = option.adresse || '';
    this.mail = option.mail || '';
    this.dateNaissance = option.dateNaissance || '';
    this.villeNaissance = option.villeNaissance || '';
    this.numeroTelephone = option.numeroTelephone || '';
    this.motDePasse = option.motDePasse || '';
  }

  /**
   * Crée un utilisateur à partir d'un flux  JSON
   * @param json Les propriétés de l'uilisateur contenues dans le flux JSON
   * @returns {Utilisateur} L'objet utilisateur créé
   */
  public static fromJson(json: any): Utilisateur {
    return new Utilisateur({
      id: json.USER_id,
      role: Role.fromJson({ id: json.ROLE_id, label: json.ROLE_label }),
      civilite: json.USER_civilite,
      nom: json.USER_nom,
      prenom: json.USER_prenom,
      adresse: Adresse.fromJson({
        id: json.ADRESSE_id,
        numero: json.ADRESSE_numero,
        rue: json.ADRESSE_rue,
        complement: json.ADRESSE_complement,
        codePostal: json.ADRESSE_codePostal,
        ville: json.ADRESSE_ville,
        pays: json.ADRESSE_pays,
      }),
      mail: json.USER_mail,
      dateNaissance: json.USER_dateNaissance,
      villeNaissance: json.USER_lieuNaissance,
      numeroTelephone: json.USER_tel,
      motDePasse: json.USER_mdp,
    });
  }
}
