import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from './../../shared/shared.module';
import { DesignationPartiesComponent } from './designation-parties/designation-parties.component';
import { ObjetContratComponent } from './objet-contrat/objet-contrat.component';
import { PriseEffetContratComponent } from './prise-effet-contrat/prise-effet-contrat.component';
import { ConditionsFinancieresComponent } from './conditions-financieres/conditions-financieres.component';
import { TravauxGarantiesComponent } from './travaux-garanties/travaux-garanties.component';
import { SignatureComponent } from './signature/signature.component';
import { ChoixPartiesBienComponent } from './choix-parties-bien/choix-parties-bien.component';

@NgModule({
  declarations: [
    DesignationPartiesComponent,
    ObjetContratComponent,
    PriseEffetContratComponent,
    ConditionsFinancieresComponent,
    TravauxGarantiesComponent,
    SignatureComponent,
    ChoixPartiesBienComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    SharedModule,
  ],
})
export class CreationBailModule {}
