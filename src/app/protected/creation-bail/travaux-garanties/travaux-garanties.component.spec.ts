import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TravauxGarantiesComponent } from './travaux-garanties.component';

describe('TravauxGarantiesComponent', () => {
  let component: TravauxGarantiesComponent;
  let fixture: ComponentFixture<TravauxGarantiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TravauxGarantiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TravauxGarantiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
