import {
  TypesBien,
  RegimesJuridiques,
  UsagesLocaux,
} from './../../../constants';
import { Constants } from 'src/app/constants';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CreationBailProvider } from 'src/app/core/providers/creation-bail.provider';

@Component({
  selector: 'app-objet-contrat',
  templateUrl: './objet-contrat.component.html',
  styleUrls: ['./objet-contrat.component.scss'],
})
export class ObjetContratComponent implements OnInit {
  /**
   * Formulaire pour la désignation des parties
   */
  form!: FormGroup;

  /**
   * Nom du formulaire
   */
  formLabel = 'objetContrat';

  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  /**
   * type de bien
   */
  typesBien = TypesBien;

  /**
   * régime juridique
   */
  regimesJuridiques = RegimesJuridiques;

  /**
   * usage des locaux
   */
  usagesLocaux = UsagesLocaux;

  /**
   * Constructeur du composant
   * @param creationBailProvider Provider permettant d'enregistrer les étapes de création de bail
   * @param fb FormBuilder natif angular
   */
  constructor(
    private creationBailProvider: CreationBailProvider,
    private fb: FormBuilder,
    private router: Router,
    public constants: Constants
  ) {}

  /**
   * Initialisation du formulaire
   */
  ngOnInit(): void {
    this.initForm();
  }

  /**
   * Initialisation du formulaire
   */
  initForm(): void {
    const formulairePrerempli = this.creationBailProvider.formGroups.find(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli) {
      this.form = formulairePrerempli.formGroup;
    } else {
      this.form = this.fb.group({
        rue: [
          {
            value:
              this.creationBailProvider.bien.adresse.numero +
              ' ' +
              this.creationBailProvider.bien.adresse.rue,
            disabled: true,
          },
          [Validators.required],
        ],
        codePostal: [
          {
            value: this.creationBailProvider.bien.adresse.codePostal,
            disabled: true,
          },
          [Validators.required, Validators.pattern('[0-9]{5}')],
        ],
        ville: [
          {
            value: this.creationBailProvider.bien.adresse.ville,
            disabled: true,
          },
          [Validators.required, Validators.maxLength(55)],
        ],
        complementAdresse: [
          {
            value: this.creationBailProvider.bien.adresse.complement,
            disabled: true,
          },
        ],
        typeHabitat: [
          {
            value: this.creationBailProvider.bien.typeBien.id.toString(),
            disabled: true,
          },
          [Validators.required],
        ],
        surface: [
          {
            value: this.creationBailProvider.bien.surfaceHabitable,
            disabled: true,
          },
          [Validators.required, Validators.pattern('^\\d+(\\.\\d{1,2})?$')],
        ],
        meuble: [{ value: false, disabled: true }],
        regimeImmeuble: [
          {
            value: this.creationBailProvider.bien.regimeJuridique.id.toString(),
            disabled: true,
          },
          [Validators.required],
        ],
        construction: [
          {
            value: this.creationBailProvider.bien.anneeConstruction,
            disabled: true,
          },
          [Validators.required],
        ],
        autresParties: [
          {
            value: this.creationBailProvider.bien.detailPieces,
            disabled: true,
          },
          [Validators.required],
        ],
        equipements: [
          {
            value: this.creationBailProvider.bien.equipementMaison,
            disabled: true,
          },
          [Validators.required],
        ],
        usageLocaux: ['', [Validators.required]],
        garageVelo: [
          { value: this.creationBailProvider.bien.garageVelo, disabled: true },
        ],
        espacesVerts: [
          {
            value: this.creationBailProvider.bien.espacesVerts,
            disabled: true,
          },
        ],
        localPoubelles: [
          {
            value: this.creationBailProvider.bien.localPoubelles,
            disabled: true,
          },
        ],
        ascenseur: [
          { value: this.creationBailProvider.bien.ascenseur, disabled: true },
        ],
        jeux: [
          { value: this.creationBailProvider.bien.aireJeux, disabled: true },
        ],
        gardiennage: [
          { value: this.creationBailProvider.bien.gardiennage, disabled: true },
        ],
        laverie: [
          { value: this.creationBailProvider.bien.laverie, disabled: true },
        ],
      });
    }
  }

  /**
   * Soumission du formulaire pour passer à l'étape suivante
   */
  onSubmit(): void {
    this.submitted = true;

    const formulairePrerempli = this.creationBailProvider.formGroups.findIndex(
      (form) => form.formGroupLabel === this.formLabel
    );

    if (formulairePrerempli !== -1) {
      this.creationBailProvider.formGroups.splice(formulairePrerempli, 1, {
        formGroupLabel: this.formLabel,
        formGroup: this.form,
      });
    } else {
      this.creationBailProvider.formGroups.push({
        formGroupLabel: this.formLabel,
        formGroup: this.form,
      });
    }

    this.router.navigate(['admin', 'creation-bail', 'prise-effet-contrat']);
  }
}
