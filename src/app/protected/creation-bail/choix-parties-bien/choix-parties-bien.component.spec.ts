import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoixPartiesBienComponent } from './choix-parties-bien.component';

describe('ChoixPartiesBienComponent', () => {
  let component: ChoixPartiesBienComponent;
  let fixture: ComponentFixture<ChoixPartiesBienComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoixPartiesBienComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoixPartiesBienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
